/**
 * Compress CSS files.
 *
 * ---------------------------------------------------------------
 *
 * Minifies css files and places them into .tmp/public/min directory.
 *
 * For usage docs see:
 * 		https://github.com/gruntjs/grunt-contrib-cssmin
 */
module.exports = function(grunt) {
    var date = require('../pipeline').getTimeStamp();

    grunt.config.set('cssmin', {
        target: {
            files: [{
                src: ['.tmp/public/concat/production_app.css'],
                dest: '.tmp/public/min/production' + date +'.app.min.css'
            },{
                src: ['.tmp/public/concat/production.css'],
                dest: '.tmp/public/min/production' + date +'.min.css'
            }]
        }
	});
	grunt.loadNpmTasks('grunt-contrib-cssmin');
};
