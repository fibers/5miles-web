/**
 * grunt/pipeline.js
 *
 * The order in which your css, javascript, and template files should be
 * compiled and linked from your views and static HTML files.
 *
 * (Note that you can take advantage of Grunt-style wildcard/glob/splat expressions
 * for matching multiple files.)
 */



// CSS files to inject in order
//
// (if you're using LESS with the built-in default config, you'll want
//  to change `assets/styles/importer.less` instead.)
var cssFilesToInject = [
  'vendor_copy/**/*.css',
  'styles/web/*.css'
];
var cssAppFilesToInject = [
    'styles/app/pub/reset.css',
    'styles/app/pub/unit.css'
];


// Client-side javascript files to inject in order
// (uses Grunt-style wildcard/glob/splat expressions)
var jsFilesToInject = [

  'vendor_copy/jquery/*.js',
  'vendor_copy/**/*.js',

  // Load sails.io before everything else
//  'js/dependencies/sails.io.js',

  // Dependencies like jQuery, or Angular are brought in here
  'js/dependencies/**/*.js',

  // All of the rest of your client-side js files
  // will be injected here in no particular order.
  'js/**/*.js',

  'js/*.js'
];


// Client-side HTML templates are injected using the sources below
// The ordering of these templates shouldn't matter.
// (uses Grunt-style wildcard/glob/splat expressions)
//
// By default, Sails uses JST templates and precompiles them into
// functions for you.  If you want to use jade, handlebars, dust, etc.,
// with the linker, no problem-- you'll just want to make sure the precompiled
// templates get spit out to the same file.  Be sure and check out `tasks/README.md`
// for information on customizing and installing new tasks.
var templateFilesToInject = [
  'templates/**/*.html'
];



// Prefix relative paths to source files so they point to the proper locations
// (i.e. where the other Grunt tasks spit them out, or in some cases, where
// they reside in the first place)
module.exports.cssFilesToInject = cssFilesToInject.map(function(path) {
  return '.tmp/public/' + path;
});
module.exports.cssAppFilesToInject = cssAppFilesToInject.map(function(path) {
    return '.tmp/public/' + path;
});
module.exports.jsFilesToInject = jsFilesToInject.map(function(path) {
  return '.tmp/public/' + path;
});
module.exports.templateFilesToInject = templateFilesToInject.map(function(path) {
  return 'assets/' + path;
});



// the base js for loading
var baseJS = function(extJS) {
    var base = ['vendor_copy/jquery/jquery.js',
        'vendor_copy/bootstrap/bootstrap.js',
        'js/dependencies/jquery.cookie.js',
        'js/dependencies/angular.min.js',
        'js/dependencies/jquery.smartbanner.js',
        'js/dependencies/Autocomplete.js',
        'js/app.js'
    ];
    base = base.concat(extJS);
    return base;
}

var unitJSFiles = [
    {
        name:"home",
        src: baseJS([
            'js/dependencies/masonry.pkgd.min.js',
            'js/dependencies/imagesloaded.pkgd.min.js',
            'js/home/home.js'
        ])
    },{
        name:"sell",
        src: baseJS(['js/dependencies/masonry.pkgd.min.js',
            'js/dependencies/imagesloaded.pkgd.min.js',
            'js/home/home.js'
        ])
    },{
        name:"search",
        src: baseJS([
            'js/dependencies/masonry.pkgd.min.js',
            'js/dependencies/imagesloaded.pkgd.min.js',
            'js/home/home.js'
        ])
    },{
        name:"login",
        src: baseJS([
            'js/login/login.js',
            'js/dependencies/jstz-1.0.4.min.js',
            'js/dependencies/facebook.js'
        ])
    },{
        name:"item",
        src: baseJS([
            'js/dependencies/jquery.jplayer.min.js',
            'js/dependencies/swipe.js',
            'js/item/item.js'
        ])
    },{
        name:"itemUpload",
        directReplace:"item/itemUploadLayout.ejs",
        src: baseJS([
            'js/dependencies/jquery.d.ui.widget.js',
            'js/dependencies/jquery.e.iframe-transport.js',
            'js/dependencies/jquery.fileupload.js',
            'js/dependencies/jquery.g.cloudinary.js',
            'js/dependencies/cloudinary/load-image.min.js',
            'js/dependencies/cloudinary/canvas-to-blob.min.js',
            'js/dependencies/cloudinary/jquery.fileupload-process.js',
            'js/dependencies/cloudinary/jquery.fileupload-image.js',
            'js/dependencies/cloudinary/jquery.fileupload-validate.js',
            'js/item/itemUpload.js'
        ])
    },{
        name:"person",
        src: baseJS([
            'js/dependencies/masonry.pkgd.min.js',
            'js/dependencies/imagesloaded.pkgd.min.js',
            'js/person/person.js'
        ])
    },{
        name:"promotion",
        src: baseJS([
            'js/home/home.js',
            'js/promotion/promotion.js',
            'js/dependencies/facebook.js'
        ])
    },{
        name:"reset",
        src:[
            'vendor_copy/jquery/jquery.js',
            'js/reset/reset.js'
        ]
    },{
        name:"mail",
        src: baseJS([
            'js/dependencies/swipe.js',
            'js/mail/mail.js'
        ])
    },{
        name:"category",
        src: baseJS([])
    },{
        name:"errpage",
        src: baseJS([
            'js/dependencies/masonry.pkgd.min.js',
            'js/dependencies/imagesloaded.pkgd.min.js',
            'js/home/home.js'
        ])
    },{
        name:"registration",
        src: baseJS([])
    },{
        name:"message",
        src: baseJS([
            'js/message/msg.js'
        ])
    },{
        name:"app_notification",
        src: [
            'js/dependencies/zepto/zepto.min.js',
            'js/app/notification.js'
        ]
    },{
        name:"account",
        src: baseJS(['js/account/account.js'])
    },
    {
        name:"app/service",
        src: [
            'js/dependencies/zepto/zepto.min.js',
            'js/app/app.service.js'
        ]
    }
];
var unitCssFiles = [
    {
        name:"default",
        src: module.exports.cssFilesToInject
    }
];
var unitCssAppFiles = [
    {
        name:"app",
        src: module.exports.cssAppFilesToInject
    }
];


module.exports.jsCssConcat = function() {
    var res = {};
    unitJSFiles.forEach(function (f) {
        res[f.name] = {
            "src": f.src.map(function(path) {
                return '.tmp/public/' + path;
            }),
            "dest": '.tmp/public/concat/production_' + f.name + ".js"
        }
    });
    unitCssFiles.forEach(function (f) {
        res[f.name] = {
            "src": f.src,
            "dest": '.tmp/public/concat/production.css'
        }
    });
    unitCssAppFiles.forEach(function (f) {
        res[f.name] = {
            "src": f.src,
            "dest": '.tmp/public/concat/production_app.css'
        }
    });

//    unitCssAppFiles.forEach(function (f) {
//        res[f.name] = {
//            "src": f.src.map(function(path) {
//                return '.tmp/public/' + path;
//            }),
//            "dest": '.tmp/public/concat/production_' + f.name + ".css"
//        }
//    });
    return res;
};

var date = Date.parse(new Date());
module.exports.jsUglify = function() {
    var res = {};
    unitJSFiles.forEach(function (f) {
        res[f.name] = {
            "src": '.tmp/public/concat/production_' + f.name + ".js",
            "dest": '.tmp/public/min/production_' + f.name + date + '.min.js'
        }
    });
    return res;
};
module.exports.jsDevReplace = function() {
    var res = {};
    unitJSFiles.forEach(function (f) {
        var src = "views/" + f.name + "/*.ejs";
        if (f.directReplace) {
            src = "views/" + f.directReplace;
        }
        res[src] = f.src.map(function(path) {
            return '.tmp/public/' + path;
        });
    });
    return res;
};
module.exports.cssDevReplace = function() {
    var res = {
        "views/*.ejs":cssFilesToInject.map(function(path) {
            return '.tmp/public/' + path;
        })
    };
    return res;
};
module.exports.cssAppDevReplace = function() {
//    var res = {};
//    cssAppFilesToInject.forEach(function (f) {
//        var src = "views/app/" + f.name + "/*.ejs";
//        if (f.directReplace) {
//            src = "views/" + f.directReplace;
//        }
//        res[src] = f.src.map(function(path) {
//            return '.tmp/public/' + path;
//        });
//    });
    var res = {
        "views/app/*/*.ejs":cssAppFilesToInject.map(function(path) {
            return '.tmp/public/' + path;
        })
    };
    return res;
};
module.exports.jsReplace = function() {
    var res = {};
    unitJSFiles.forEach(function (f) {
        var src = "views/" + f.name + "/*.ejs";
        if (f.directReplace) {
            src = "views/" + f.directReplace;
        }
        res[src] = ['.tmp/public/min/production_' + f.name + date + '.min.js'];
    });
    return res;
};

module.exports.cssReplace = function() {
    var res = {
        "views/*.ejs":['.tmp/public/min/production' + date + '.min.css']
    };
    return res;
};
module.exports.cssAppReplace = function() {
    var res = {
        "views/app/*/*.ejs":['.tmp/public/min/production' + date + '.app.min.css']
    };
    return res;
};
module.exports.getTimeStamp = function() {
    return date;
};


