/**
 * ResetPasswordController
 *
 * @description :: Reset the user's password
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Q = require('q');
var ejs = require('ejs');
var picCloudPrefix = sails.config.settings.picCloudPrefix;
var tools = require('../services/Tools.js');
var siteMap =sails.config.settings.siteMap;
var Intl = require("intl");
var formidable = require('formidable'),
    util = require('util'),fs=require('fs');
var ua   = require('mobile-agent');

var log = require('../services/Log.js');
var categoryDataJson = tools.getCategoryJson();


function _isEmptyObj(obj){
    return Object.keys(obj).length?false:true;
}

var _thumbScale = function(input,size,face) {
    if(input == null)return input;
    if (!size) {
        size = 0.5;
    }
    if(face){
        return input.replace(/upload/, 'upload/w_' + size + ',f_auto,h_' + size + ',c_thumb,g_face').replace(/http:/, 'https:');
    }else{
        return input.replace(/upload/, 'upload/w_' + size + ",f_auto").replace(/http:/, 'https:');
    }
    
};

var _titleCut = function(str) {
    var temp = [];
    if (str) {
        temp = str.split(" ");
        if (temp.length > 4) {
            temp = temp.slice(0,4);
        }
        str = temp.join(" ");

    }
    return str;
};
module.exports = {
    /**
     * get the item data
     * @param req
     * @param res
     */
    showItem: function (req, res) {
        try {
            var key = req.param("code");
            tools.appendClientCookie(req,res,"recent_item",key,10);
            //thumbnail:50:'g_face'
            ejs.filters.thumb = function(obj) {
                return Tools.thumbFilter(obj,"100","g_face");
            };
            ejs.filters.thumbScale = function(obj,size,face) {
                return _thumbScale(obj,size,face);
            };
            ejs.filters.clipSize = function(obj,width) {
                if (width && width < 500) {
                    return obj.replace(/w_800/, 'w_' + width);
                } else {
                    return obj;
                }
            };
            ejs.filters.numberFixed = function(number) {
                return Tools.milesFilter(number);
            };
            ejs.filters.titleParse = function(url, catId) {
                var catId = catId + "";
                if (!(catId == 'undefined') && !(catId == 'null')) {
                    if (categoryDataJson[catId]) {
                        url =  categoryDataJson[catId].title + " " + url;
                    }
                }
                return Tools.titleParse(url);
            }
            ItemService.getProductDetailWithDistance(key,req,res).then(function(data) {

                var currencySymbol = new Intl.NumberFormat(req.getLocale(),{style:"currency",currency:data.currency}).format(0);
                data.currencySymbol = currencySymbol.replace(/[\d\.-]/g,"");
                if(data.currencySymbol.length>1){
                    //if the currency length is more than one charactor, the style need modify.
                    data.longSymbol = "longSymbol";
                }
                if (_isEmptyObj(data)) {
                    res.notFound();
                } else  {
                    res.locals.layout = "item/itemLayout";
                    var agent = Tools.judgeClient(req);

                    var os = tools.judgeMbClientOS(req);
                    //seo display
                    if (agent.Facebook || agent.Fragment) {
                        res.render('item/itemPlain', {item: data,"completeUrl":""});
                        return;
                    }

                    //get recommend items
                    var itemId = data.id;
                    var lat = data.location.lat;
                    var lon = data.location.lon;
                    var cat = categoryDataJson[data.cat_id] ? categoryDataJson[data.cat_id].title : "";
                    var head = {
                        "title":data.title ,
                        "keyTitle":_titleCut(data.title),
                        "owner":data.owner.nickname ,
                        "category": cat,
                        "description":data.desc
                    };
                    //build 5miles://.... type url in order to recall app when installed.
                    var cat_value = categoryDataJson[data.cat_id];
                    var completeUrl = req.baseUrl + "/item/" + data.id +"/" + tools.parseTitle((cat_value!=undefined?cat_value.title:"")+" "+ data.title);

                    ItemService.getProductRecommend(req,itemId, lat, lon).then(function(recommend) {
                        if (recommend) {
                            data["recommends"] = recommend;
                        }
                        var downUrl = Tools.getDownloadLinks(req,res);
                        var reportArray = tools.getReportArray();
                        var agent = ua(req.headers['user-agent']);
                        res.view('item/item', {item: data,"head":head,"origUrl":req.baseUrl+req.originalUrl,"down":downUrl,
                            "os":os,"completeUrl":completeUrl,"categoryDataJson":categoryDataJson,"mobile":agent.Mobile,"reportArray":JSON.stringify(reportArray)});
                    });
                }
            },function() {
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];

                log.error("getProductDetail error 404",key,":refer:" + refer,":agent:" + agent);
                res.notFound();
            });
        } catch (err) {
            if (err.status === 404) {
                res.notFound();
            } else {
                log.error('getItem failed', err);
            }
        }
    },
    /**
     * for 301 redirect
     * @param req
     * @param res
     */
    showItemRedirect: function (req, res) {
        try {
            var key = req.param("code");
            ItemService.getProductDetailWithDistance(key,req,res).then(function(data) {
                if (_isEmptyObj(data)) {
                    res.notFound();
                } else  {
                    var agent = Tools.judgeClient(req);
                    //seo display
                    if (agent.Facebook) {
                        res.render('item/itemPlain', {item: data,"completeUrl":""});
                        return;
                    }
                    //redirect the contain the title
                    var catId = data.cat_id;
                    var title = data.title;

                    if (catId != null) {
                        var cat = categoryDataJson[catId] ? categoryDataJson[catId].title : "";
                        title =  cat + " " + title;
                    }
                    res.redirect(301,"/item/" + key + "/" + Tools.titleParse(title));
                }
            },function() {
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];

                log.error("showItemRedirect error 404",key,":refer:" + refer,":agent:" + agent);
                res.notFound();
            });
        } catch (err) {
            if (err.status === 404) {
                res.notFound();
            } else {
                log.error('showItemRedirect failed', err);
            }
        }
    },
    showItemMap: function (req, res) {
        var lat = req.param("lat");
        var lon = req.param("lon");
        var encodeLat = new Buffer(lat, 'base64').toString();
        var encodeLon = new Buffer(lon, 'base64').toString();
        res.locals.layout = "item/itemLayout";
        var head = {
            "title":"Map - Your Mobile Marketplace",
            "keywords":"5miles,map,5milesapp",
            "description":"map of product"
        };
        res.view('item/itemMap', {item: {"latitude":encodeLat,"longitude":encodeLon},"head":head,"completeUrl":""});
    },
    showManageItem: function (req, res) {
        ItemService.signForPicture(req,res).then(function(data) {
            var categoryDataArray = tools.getCategoryArray();
            data["categoryDataArray"] = categoryDataArray;
            res.render('item/itemUploadLayout', data);
        },function(error) {
            res.redirect('/loginShow/?param=sign');
        });
    },
    /**
     * the home page for view items
     * @param req
     * @param res
     */
    getHomeItems: function(req, res) {
        var keyword = req.param("keyword");
        var category = req.param("category");
        var distance = req.param("distance");
        var orderby = req.param("orderby");
        if (keyword || category || distance || orderby) {
            //keyword search
            ItemService.getSearchItemsDistance(req,res).then(function(data) {
                res.json(data);
            },function(error) {
                res.json(error);
            });
        } else {
            //home search
            ItemService.getHomeItemsDistance(req,res).then(function(data) {
                res.json(data);
            },function(error) {
                res.json(error);
            });
        }
    },
    /**
     * get the search items
     * @param req
     * @param res
     */
    getSearchItems: function(req, res) {
        ItemService.getSearchItemsDistance(req,res).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    

    like : function(req,res){
        var item = req.params.item_id;
        ItemService.like(req,res,item).then(function(data){
            res.ok(data);
        },function(err){
            res.ok(err)
        })
    },
    unlike : function(req,res){
        var item = req.params.item_id;
        ItemService.unlike(req,res,item).then(function(data){
            res.ok(data);
        },function(err){
            res.ok(err);
        })
    },
    follow : function(req,res){
        var userId = req.params.user_id;
        ItemService.follow(req,res,userId).then(function(data){
            res.ok(data);
        },function(err){
            res.ok(err);
        })
    },
    unfollow : function(req,res){
        var userId = req.params.user_id;
        ItemService.unfollow(req,res,userId).then(function(data){
            res.ok(data);
        },function(err){
            res.ok(err);
        })

    },
    upload: function(req, res) {
        ItemService.postItem(req,res).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    }
};
