/**
 * MobileController
 *
 * @description :: Server-side logic for managing Mobiles
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var loginCtr = require('../controllers/LoginController.js');

module.exports = {

    about: function (req, res) {
        res.view('info/about', {layout: 'mobile_layout'});
    },

    support: function (req, res) {
        loginCtr.zendesk(req,res,{"mobile":true,"return":"http://help.5milesapp.com/hc/en-us/articles/205372817/?fromapp=1"});
//        res.view('info/support', {layout: 'mobile_layout'});
    },

    privacy: function (req, res) {
        res.view('info/privacy', {layout: 'mobile_layout'});
    },

    terms: function (req, res) {
        res.view('info/terms', {layout: 'mobile_layout'});
    },

    helpCenter: function (req, res) {
        loginCtr.zendesk(req,res,{"mobile":true,"return":"http://help.5milesapp.com/hc/en-us#fromapp=1"});
//        res.view('info/helpCenter', {layout: 'mobile_layout'});
    },

    prohibited: function (req, res) {
        res.redirect("http://help.5milesapp.com/hc/en-us/articles/205079167?from=prohibited");
//        res.view('info/prohibited', {layout: 'mobile_layout'});
    }
};

