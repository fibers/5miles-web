/**
 * SendController
 *
 * @description :: Server-side logic for managing infoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var NexmoService = require('../services/NexmoService.js');
var AWSService = require('../services/AWSService.js');
var MailService = require("../services/MailService.js");
var tools = require("../services/Tools.js");
var log = require('../services/Log.js');

var settings = sails.config.settings;

module.exports = {
    message: function (req, res) {
        var dest = req.param('dest');
        var type = req.param('type');

        if (dest == null || type == null) {
            return res.json(404, {success: false, data: 'Parameters error'});
        }

        if (type.toLowerCase() == 'email') {
            MailService.sendEmail(dest,req);
            return res.ok({success: true, data: ''});
        } else if (type.toLowerCase() == 'phone') {
            NexmoService.sendMessage(dest, settings.message.content).then(function(data){
                return res.ok({success: true, data: ''});
            },function(err){
                return res.ok({"ERROR":error});
            });
        }

    },
    sendInnerMsg: function (req, res) {
        res.render('serv/msgLayout',{});
    },
    messageServ: function (req, res) {
        var dest = req.param('dest');
        var text = req.param('msg');

        if (dest == null || text == null) {
            return res.json(404, {success: false, data: 'Parameters error'});
        }
        NexmoService.sendMessage(dest, text).then(function(data){
            return res.ok({success: true, data: ''});
        },function(err){
            return res.ok({"ERROR":error});
        });

    },

    download: function (req, res) {
        var userAgent = req.get('User-agent');
        if (_.contains(userAgent, 'iPhone') || _.contains(userAgent, 'iPad')) {//_.contains(userAgent, 'iPhone') || _.contains(userAgent, 'iPad')
            var url = tools.getDownloadLinks(req,res)[1];
            res.redirect(url);
        } else if (_.contains(userAgent, 'Android') || _.contains(userAgent, 'X11')) {
            var url = tools.getDownloadLinks(req,res)[0];
            res.redirect(url);
        } else {
            res.redirect('/');
        }
    }
};

