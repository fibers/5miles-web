/**
 * HomeController
 *
 */
var tools = require('../services/Tools.js');
var log = require('../services/Log.js');
var newmsgService = require('../services/NewMessagesService.js');
var ejs = require('ejs');
var recommend = sails.config.settings.recommend;

var siteMap =sails.config.settings.siteMap;
var categoryDataJson = tools.getCategoryJson();


module.exports = {
    home: function (req, res) {
        ejs.filters.replaceCate = function(obj) {
            return obj.replace(/ & /g,'-').replace(/ /g,'-');
        };
        var os = tools.judgeMbClientOS(req);
        var categoryDataArray = tools.getCategoryArray();
        res.render('home/homeLayout',{"down":Tools.getDownloadLinks(req,res),"os":os,
            "categoryDataJson":categoryDataJson,"categoryDataArray":categoryDataArray});
    },
    redirectHome: function (req, res) {
        res.redirect("/");
    },
    video: function (req, res) {
        res.render('home/video');
    },
    win: function (req, res) {
        res.render('home/gleamPage');
    },
    search: function (req, res) {
        var op = "recent_cat";
        var value = req.param("category");
        var kw = '';
        if(value == undefined){
            op = "recent_search"
            value = kw = req.param("keyword");
        } else if(categoryDataJson[value] != undefined){
            kw = categoryDataJson[value].title;
        }
        ejs.filters.replaceCate = function(obj) {
            return obj.replace(/ & /g,'-').replace(/ /g,'-');

        };
        tools.appendClientCookie(req,res,op,value,20);
        var categoryDataArray = tools.getCategoryArray();
        res.render('search/searchLayout',{"down":Tools.getDownloadLinks(req,res),"keyword":kw,
            "categoryDataJson":categoryDataJson,"categoryDataArray":categoryDataArray});
    },
    siteMapXml: function(req,res){
        res.locals.layout = "";
        var categoryDataArray = tools.getCategoryArray();
        res.render("siteMapXml",{siteData:siteMap,category:categoryDataArray});
    },
    siteMapHTML: function(req,res){
        res.locals.layout = "";
        var categoryDataArray = tools.getCategoryArray();
        res.render("siteMapHTML",{siteData:siteMap,"down":Tools.getDownloadLinks(req,res),category:categoryDataArray});
    },
    recommend: function(req,res){
        //set the ip to Dallas for recommend items.
        req.headers['x-forwarded-for'] = recommend.mockIP;
        var offset = req.param("offset");

        ejs.filters.titleParse = function(url, catId) {
            if (catId != null) {
                url =  categoryDataJson[catId].title + " " + url;
            }
            return tools.titleParse(url);
        }
        //home search
        ItemService.getHomeItemsDistance(req,res).then(function(data) {
            var arr = data.objects;
            for(var i in arr){
                var image = arr[i].images[0];
                image.imageLink = image.imageLink.replace(/upload/, 'upload/w_' + 300 + ",f_auto").replace(/http:/, 'https:');
            }
            var offset = data.meta.next.match(/offset=(\d+)/)[1];

            res.render("home/recommend",{"result":data,"offset":offset})
        },function(error) {
            res.json(error);
        });
    },

    newMessages :  function(req,res){
        newmsgService.getNewMsg(req).then(function(data){
            res.json(data);
        },function(err){
            res.json({});
        })
    },
    myMessage : function(req,res){
        var os = tools.judgeMbClientOS(req);
        res.locals.layout = "message/messageLayout";
        res.view("message/message",{"down":Tools.getDownloadLinks(req,res),"os":os});
    },
    promo : function(req,res){
        var code = req.param("code");

        var address = {
            "android":"http://app.appsflyer.com/com.thirdrock.fivemiles?pid=offline&c=" + code,
            "ios":"http://app.appsflyer.com/id917554930?pid=offline&c=" + code,
            "ext":"https://5milesapp.com/?utm_source=offline&utm_medium=URL&utm_campaign=" + code
        };
        var os = tools.judgeMbClientOS(req);
        var url = address["ext"];
        if (os.IOS) {
            url = address["ios"];
        } else if (os.Android) {
            url = address["android"];
        }
        res.redirect(url);
    },
    _test : function(req,res){
        MailService._test(req,res);
    }
};

