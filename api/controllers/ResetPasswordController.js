/**
 * ResetPasswordController
 *
 * @description :: Reset the user's password
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var log = require('../services/Log.js');

module.exports = {
    resetPasswordView:function(req, res) {
        res.locals.layout = "reset/resetLayout";
        res.view('reset/resetpwd');
        return;
    },
    resetPassword: function (req, res) {
       return ResetService.resetPassword(req,res);
    },
    resetEmailView:function(req, res) {
        res.locals.layout = "reset/resetLayout";
        res.view('reset/reset');
        return;
    },
    resetEmail: function (req, res) {
        ResetService.resetEmail(req,res).then(function(data) {
            res.json(data);
        },function(error) {
            log.error("resetEmail:",JSON.stringify(error));
            res.json(error);
        });
    }
};

