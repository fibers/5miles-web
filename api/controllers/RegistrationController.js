/**
 * HomeController
 *
 */
var RegistrationService = require('../services/RegistrationService.js');
var log = require('../services/Log.js');
var Tools = require('../services/Tools.js');
var ejs = require('ejs');


module.exports = {
    verifyEmail: function (req, res) {
        var verifyCode = encodeURIComponent(req.param("emailCode"));

        var os = Tools.judgeMbClientOS(req);
        res.locals.layout = "registration/registLayout";
        RegistrationService.verifyEmail(req,verifyCode).then(function(data){
            res.view('registration/verifyEmail',{type:"success","down":Tools.getDownloadLinks(req,res),os:os});
        },function(error){
            res.view('registration/verifyEmail',{type:"fail","down":Tools.getDownloadLinks(req,res),os:os});
        });

    }
};

