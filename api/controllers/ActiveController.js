/**
 * ActiveController
 */

module.exports = {
    /**
     * These rules will need to remain up until 8/31/15
     * @param req
     * @param res
     */
    cashcard: function (req, res) {
        res.locals.layout = "active/activeLayout";
        res.view('active/cashCard');
    },
    bus: function (req, res) {
        res.locals.layout = "active/activeLayout";
        res.view('active/bus');
    },
    metro: function (req, res) {
        res.locals.layout = "active/activeLayout";
        res.view('active/metro');
    }
};

