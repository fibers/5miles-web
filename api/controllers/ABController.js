/**
 * ABController
 *
 * @description :: The first init data
 */

var categoryServ = require('../services/CategoryService.js');
var InitDataServ = require('../services/InitDataService.js');

var log = require('../services/Log.js');
var http = require('http');

var category = [];
var categoryJson = {};
var reportJson = [];

module.exports = {
    _initCategoryInfo: function() {
        if (category && (category.length == 0)) {
            categoryServ.getCategoryInfo().then(function(data) {
                category = data.objects;
                for (var i = 0; i < category.length;i++) {
                    var item = category[i];
                    categoryJson[item.id] = item;
                }
            },function(error) {
                log.error("initCategoryInfo",error);
            });
        }
    },
    _initReportList: function() {
        if (reportJson && (reportJson.length == 0)) {
            InitDataServ.getReportList().then(function(data) {
                reportJson = data;
            },function(error) {
                log.error("_initReportList",error);
            });
        }
    }
};
exports.getCategoryArray = function () {
    return category;
};
exports.getCategoryJson = function () {

    return categoryJson;
};
exports.getReportArray = function () {
    return reportJson;
};


module.exports._initCategoryInfo();
module.exports._initReportList();