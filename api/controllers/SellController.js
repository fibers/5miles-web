/**
 * HomeController
 *
 */
var tools = require('../services/Tools.js');

var ejs = require('ejs');
module.exports = {
    sell: function(req,res){
    	res.locals.layout = "sell/sellLayout";
    	res.view('sell/sell',{"down":Tools.getDownloadLinks(req,res)});
    },
    sellMb: function(req,res){
        res.locals.layout = "sell/sellMbLayout";
        var loc = tools.getTheReqLocation(req);
        res.view('sell/sellMb',{"down":Tools.getDownloadLinks(req,res),"loc":loc});
    }
};

