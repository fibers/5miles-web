/**
 * CategoryController
 */
var tools = require('../services/Tools.js');
var log = require('../services/Log.js');

var category = [];

module.exports = {
    categoryMb: function(req,res){
        category = tools.getCategoryArray();
        res.render("category/categoryMbLayout",{"categoryDataArray":category});
    }
};


