/**
 * LoginController
 *
 * @description :: manage login and sign up
 */

var settings = sails.config.settings;
var service = require('../services/LoginService.js');
var itemService = require('../services/ItemService.js');
var tools = require('../services/Tools.js');
var log = require('../services/Log.js');
var http = require('http');
var jwt = require('jwt-simple');
var uuid = require('uuid');
var url = require('url');


module.exports = {
    /**
     * show the login page
     * @param req
     * @param res
     */
    loginShow: function (req, res) {
        res.locals.layout = "login/loginLayout";
        var clear = req.param("clear");
        if (clear && clear == "true") {
            tools.clearClientCookie(req, res);
        }
        res.view('login/login', {});
    },
    /**
     * do login
     * @param req
     * @param res
     */
    login: function (req, res) {
        LoginService.login(req,res);
    },
    /**
     * do login
     * @param req
     * @param res
     */
    loginFacebook: function (req, res) {
        LoginService.loginFacebook(req,res);
    },
    /**
     * do loginOut
     * @param req
     * @param res
     */
    loginOut: function (req, res) {
        LoginService.loginOut(req,res);
    },
    /**
     * process the sign up
     * @param req
     * @param res
     */
    signUp: function (req, res) {
        service.signUp(req,res).then(function(data) {
            res.ok(data);
        },function(error) {
            res.ok(error);
        });
    },
    /**
     * mail chat: send the offer to seller
     */
    signUpAndUpload: function(req,res){
        service.signUp(req,res).then(function(data) {
            // set the data to the req
            var header = {
                "userToken":data.token,
                "userId":data.id
            }
            itemService.postItem(req,res,header).then(function(result) {
                header["email"] = data.email;
                header["nickname"] = data.nickname;
                header["token"] = data.token;
                header["id"] = data.id;
                header["portrait"] = data.portrait;
                header["fb_user_id"] = data.fb_user_id;
                header["new_id"] = result.new_id;
                res.ok(header);
            },function(error) {
                res.ok(error);
            });
        },function(error) {
            res.ok(error);
        });
    },
    /**
     * just sign ok
     * @param req
     * @param res
     */
    signOK: function(req, res) {
        res.render('login/signok', {});
    },
    /**
     * just offer ok
     * @param req
     * @param res
     */
    offerOK: function(req, res) {
        res.render('login/offerok', {});
    },
    /**
     * show the validate the phone number page
     * @param req
     * @param res
     */
    validatePhoneShow: function (req, res) {
        res.locals.layout = "login/loginLayout";
        res.view('login/validatePhone', {});
    },
    sendPassCode: function (req, res) {
        service.sendPassCode(req,res).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    verifyPassCode: function (req, res) {
        service.verifyPassCode(req,res).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    /**
     * The zendesk auto login
     */
    zendesk: function(req, res, ext) {
        var subdomain = '5miles';
        var shared_key = 'Q6Hl2S9IHb3LbTVr9Qn3Bl8TSUiwRyfQUzE49QMIiE8OZ3w4';
//        var subdomain = 'coollist';
//        var shared_key = 'PHGufOSPtAGjnVcmQhJk1s2LAHVfcPDzTGQVmsGlMGSDaQ5Z';
        var cookie = Tools.getClientCookie(req);
        var redirect = "";
        var mobile = ext && ext.mobile;
        if (mobile) {
            if (req.param("email") && req.param("name")) {
                var payload = {
                    iat: (new Date().getTime() / 1000),
                    jti: uuid.v4()
                };
                payload["name"] = req.param("name");
                payload["email"] = req.param("email");
                // encode
                var token = jwt.encode(payload, shared_key);
                var redirect = 'https://' + subdomain + '.zendesk.com/access/jwt?jwt=' + token;
                if (ext.return) {
                    redirect += '&return_to=' + encodeURIComponent(ext.return);
                }
            } else if (ext.return) {
                    redirect = ext.return;
            }
        } else if (cookie && cookie.mail) {
            var payload = {
                iat: (new Date().getTime() / 1000),
                jti: uuid.v4()
            };
            payload["name"] = cookie.nick;
            payload["email"] = cookie.mail;
            // encode
            var token = jwt.encode(payload, shared_key);
            var redirect = 'https://' + subdomain + '.zendesk.com/access/jwt?jwt=' + token;

            var query = url.parse(req.url, true).query;

            if(query['return_to']) {
                redirect += '&return_to=' + encodeURIComponent(query['return_to']);
            }
        } else {
            redirect = 'https://' + subdomain + '.zendesk.com';
        }

        res.writeHead(302, {
            'Location': redirect
        });
        res.end();
    },
    zendesk_login: function(req, res) {
        var subdomain = '5miles';
        // encode
        var redirect = 'https://' + subdomain + '.zendesk.com';
        res.writeHead(302, {
            'Location': redirect
        });
        res.end();
    }
};

