/**
 * PromotionController
 * @author ChangQi changqi@wespoke.com
 * @description :: promotion router handler.
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

//var PromotionService = require("../services/PromotionService.js");
var sails = require("sails");
var promService = require("../services/PromotionService.js");
var moment = require("moment-timezone");
var settings = sails.config.settings.promotion;
var tools = require("../services/Tools.js");
var log = require('../services/Log.js');

module.exports = {
    switchPromotion: function (req, res) {
        
        //1.convert the current machine time to timezone time.
        //2.target time in settings is the target timezone's time,not machine time.
        var currentTime = moment().tz(settings.timeZone);
        var prettyCurrentTime = currentTime.format("YYYY/MM/DD HH:mm:ss");
        
        var targetTime = moment.tz(settings.startTime,settings.timeZone);
        
        var prettyTargetTime = targetTime.format("YYYY/MM/DD HH:mm:ss");
        var endTime = moment.tz(settings.endTime,settings.timeZone);
        var prettyEndTime = endTime.format("YYYY/MM/DD HH:mm:ss");
        var span = targetTime.unix() - currentTime.unix();
        log.info("PromotionController: get current and target time each: ",prettyCurrentTime,prettyTargetTime," and span :",span);

        var linkMap = tools.getDownloadLinks(req,res);
        res.locals.layout = "promotion/promotionLayout";
        if(span > 0){
            //still in warm up procedure.
            res.view(settings.warmupView, {"currentTime": prettyCurrentTime,"targetTime": prettyTargetTime,"gPlayLink":linkMap[0],"appLink":linkMap[1]});
        }else{
            res.view(settings.officialView,{"currentTime": prettyCurrentTime,"targetTime": prettyEndTime,"gPlayLink":linkMap[0],"appLink":linkMap[1]});
        }
        
    },
    getPromotionInfo : function(req,res){
        var op = req.param("op");

        promService.getPromotionItems(req,op).then(function(data){
            res.end(JSON.stringify(data));
        },function(err){
            log.error("get PromotionInfo error 404",err);
            return res.notFound();
        });
    }
};
