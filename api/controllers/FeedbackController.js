/**
 * FeedbackController
 *
 * @description :: the feedback
 */

module.exports = {
    /**
     * submit the feedback
     * @param req
     * @param res
     */
    feedback: function (req, res) {
        FeedbackService.feedback(req,res);
    },
    reportItem : function(req,res){
        var parameter = {
            item_id : req.param("item_id"),
            reason :  escape(req.param("reason")),
            reason_content : escape(req.param("reason_content"))
        };
    	FeedbackService.report(req,res,"item",parameter).then(function(data){
            res.ok(data);
        },function(err){
            res.ok(err);
        });
    },
    reportSeller : function(req,res){
        var parameter = {
            user_id : req.param("user_id"),
            reason :  escape(req.param("reason")),
            reason_content : escape(req.param("reason_content"))
        };
        FeedbackService.report(req,res,"seller",parameter).then(function(data){
            res.ok(data);
        },function(err){
            res.ok(err);
        });
    }
};

