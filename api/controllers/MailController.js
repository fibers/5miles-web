/**
 * MailController
 * @author ChangQi changqi@wespoke.com
 * @description :: subscribe and unsubscribe maillist.
 * @help        :: See http://links.sailsjs.org/docs/controllers
 * @issue       :: See https://github.com/3rdStone/webapp/issues/3 and https://github.com/3rdStone/fivemiles-auction/issues/61
 */

var MailService = require("../services/MailService.js");
var sails = require("sails");
var log = require('../services/Log.js');
var jwt = require("jwt-simple");
var secret = sails.config.settings.secretKey;

var filtedStatusList = [
	{
        type:1,
		typeName : "New Followers",
		action : 1,
        method:2
	},
	{
        type:2,
		typeName : "Recommendations",
		action : 1,
        method:2
	},
	{
        type:3,
		typeName : "Marketing Activities",
		action : 1,
        method:2
	},
    {
        type:5,
        typeName : "Offer/Message(s)",
        action : 1,
        method:2
    }
]
//use statusList's data index to find filtedStatusList data.
var typeIndexMap = {1:0,2:1,3:2,5:3};

//the email notice is 2,push notice is 1.
var MAIL_METHOD = 2;
/**
 * mail chat: send the offer to seller
 */
var _sendOffer = function(req,res,head) {
    var itemId = req.param("item");
    var price = req.param("body");
    var toUser = req.param("to").user_id;

    var userData = {
        "head": head,
        "item_id":itemId,
        "price":price,
        "to_user":toUser
    };

    MailService.sendOffer(req, res, userData).then(function(result) {
        if (result.status == 200) {
            result["head"] = head;
            var resStr = JSON.stringify(result);
            // send email chat
            res.end(resStr);
        } else {
            res.end('{"status":400,"body":"send error"}');
        }
    });
}
/**
 * mail chat: send the ask to seller
 */
var _sendAsk = function(req,res,head) {
    var itemId = req.param("item");
    var price = req.param("price");
    var text = req.param("body");
    var toUser = req.param("to").user_id;

    var userData = {
        "head": head,
        "item_id":itemId,
        "price":price,
        "text":text,
        "to_user":toUser
    };
    MailService.sendAsk(req, res, userData).then(function(result) {
        if (result.status == 200) {
            result["head"] = head;
            var resStr = JSON.stringify(result);
            // send email chat
            res.end(resStr);
        } else {
            res.end('{"status":400,"body":"send error"}');
        }
    });
}
module.exports = {
    userSubscriptions: function (req, res) {
        res.locals.layout = "mail/mailLayout";
    	var userToken = req.param('user_token');
    	var userId = (jwt.decode(userToken,secret)).user_id;

    	MailService.userSubscriptions({userId:userId,userToken:userToken},req)
    	.then(function(statusList) {
    		var list = _filterStatusList(statusList);
	        res.view('mail/usersubscriptions', {"statusList": list,"userToken":userToken});
	    },function(key) {
	    	//TODO: if no such user id should give some hint rather than jump to 404 page.
            log.error("getUserSubscriptions error 404",key);
            return res.notFound();
        });
    },
    unsubscribe: function(req,res){
    	var args = req.param("submit_args");
    	var userToken = req.param('user_token');
    	//TODO:2Need translate userToken into userId by using jwt decode.
    	var userId = (jwt.decode(userToken,secret)).user_id;

    	MailService.bulkUnsubscribe({userId:userId,userToken:userToken,args:args},req)
    	.then(function(result) {
    		log.info("MailController:unsubscibe success with userId:",userId);
    		res.end('{"status":200}');
	    },function(error) {
	    	log.error("unsubscribe email error 500",JSON.stringify(error));
            res.end('{"status":500}');
        });
    },

    /**
     * mail chat: send the message to seller
     */
    sendAsk: function(req,res){
        // if have user_token, direct send offer
        var user_token = req.param("user_token");
        var user_id = req.param("user_id");

        if (user_token && user_id) {
            var head = {
                "userToken":user_token,
                "userId":user_id
            };
            _sendAsk(req,res,head);
        } else {
            MailService.checkMail(req, res).then(function(result) {
                if (result.status == "200") {
                    var head = {
                        "userToken":result.body.token,
                        "userId":result.body.id,
                        "portrait":result.body.portrait
                    };
                    _sendAsk(req,res,head);
                } else {
                    var obj = result.body;
                    res.end('{"status":400,"body":"' + obj.err_msg + '"}');
                }
            },function(error) {
                log.error("sendAsk error:" + error);
                res.end('{"status":400,"body":' + error + '"}');
            });
        }
    },
    /**
     * mail chat: send the offer to seller
     */
    sendOffer: function(req,res){
        var user_token = req.param("user_token");
        var user_id = req.param("user_id");
        // if have user_token, direct send offer
        if (user_token && user_id) {
            var head = {
                "userToken":user_token,
                "userId":user_id
            };
            _sendOffer(req,res,head);
        } else {
            // first check exist
            MailService.checkMail(req, res).then(function(result) {
                if (result.status == "200") {
                    var head = {
                        "userToken":result.body.token,
                        "userId":result.body.id,
                        "portrait":result.body.portrait
                    };
                    _sendOffer(req,res,head);
                } else {
                    var obj = result.body;
                    res.end('{"status":400,"body":"' + obj.err_msg + '"}');
                }
            },function(error) {
                log.error("MailController sendOffer error:" + error);
                res.end('{"status":400,"body":' + error + '"}');
            });
        }
    }
};

/**
 * replace the filtedStatusList with server side changed datas
 * @param  {[type]} statusList [description]
 * @return {[type]}            [description]
 */
var _filterStatusList = function(statusList){
	for(var len = statusList.length,i=0;i<len;i++){
        var index = typeIndexMap[statusList[i].type];
		if(statusList[i].method == MAIL_METHOD && index!=undefined){
			filtedStatusList[index].action = (statusList[i].state == true ? 1:0);
		}
	}
	return filtedStatusList;
}
