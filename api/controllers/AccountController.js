/**
 * user's account delete
 * @type {exports}
 */


var tools = require('../services/Tools.js');

var ejs = require('ejs');
var act = require("../services/AccountService.js");

module.exports = {
    deactivate: function(req,res) {
        var method = req.method;
        if (method == "GET") {
            var cookie = tools.getClientCookie(req);
            var userId = cookie.userId;//req.session.user.id;
            var userToken = cookie.userToken;//req.session.user.token;
            if (userToken && userId) {//exist login cookie,load the page
                var name = cookie.nick;
                res.render('account/accountLayout',{"nick":name});
            } else {//redirect to login
                res.redirect('/loginShow/?back=/account/deactivate');
            }
        } else if (method == "POST") {
            act.deleteAccount(req,res).then(function(data) {
                tools.clearClientCookie(req, res);
                res.json(data);
            },function(error) {
                res.json(error);
            });
        }
    }
};

