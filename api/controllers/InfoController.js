/**
 * InfoController
 *
 * @description :: Server-side logic for managing infoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var loginCtr = require('../controllers/LoginController.js');

module.exports = {

    index: function (req, res) {
        res.view('terms');
    },

    about: function (req, res) {
        return res.view('info/index', { activeTab: ['active', '', '', '',''], activeContent: ['in active', '', '', '','']});
    },

    support: function (req, res) {
        return res.view('info/index', { activeTab: ['', 'active', '', '',''], activeContent: ['', 'in active', '', '','']});
    },

    privacy: function (req, res) {
        return res.view('info/index', { activeTab: ['', '', 'active', '',''], activeContent: ['', '', 'in active', '','']});
    },

    terms: function (req, res) {
        return res.view('info/index', { activeTab: ['', '', '', 'active',''], activeContent: ['', '', '', 'in active','']});
    },

    helpCenter: function (req, res) {
        loginCtr.zendesk(req,res);
//        return res.view('info/index', { activeTab: ['', '', '', '','active'], activeContent: ['', '', '', '','in active']});
    },

    prohibited: function (req, res) {
        res.redirect("http://help.5milesapp.com/hc/en-us/articles/205079167?from=prohibited");
//        return res.view();
    }
};

