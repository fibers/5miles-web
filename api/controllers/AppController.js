/**
 * AppController
 * @description :: Server-side logic for managing App
 */
var sails = require("sails");
var log = require('../services/Log.js');
var jwt = require("jwt-simple");
var ejs = require('ejs');
var categoryServ = require('../services/CategoryService.js');

var secret = sails.config.settings.secretKey;


var servObj = {
    "service":[{"id": 17,"title": "automotive"},
        {"id": 17,"title": "computer"},
        {"id": 17,"title": "creative"},
        {"id": 17,"title": "cycle"},
        {"id": 17,"title": "farm & garden"},
        {"id": 17,"title": "financial"},
        {"id": 17,"title": "household"},
        {"id": 17,"title": "labor & moving"},
        {"id": 17,"title": "legal"},
        {"id": 17,"title": "lessons & tutoring"},
        {"id": 17,"title": "pet"},
        {"id": 17,"title": "real estate"},
        {"id": 17,"title": "skilled trade"},
        {"id": 17,"title": "therapeutic"},
        {"id": 17,"title": "travel/vacation"},
        {"id": 17,"title": "writing/editing/translation"}],
    "house":[{"id": 13,"title": "apts / housing"},
        {"id": 17,"title": "office / commercial"},
        {"id": 17,"title": "parking / storage"},
        {"id": 17,"title": "real estate for sale"},
        {"id": 17,"title": "rooms / shared"},
        {"id": 17,"title": "sublets / temporary"},
        {"id": 17,"title": "vacation rentals"}],
    "job":["accounting+finance","admin / office","arch / engineering","art / media / design","biotech / science","business / mgmt",
    "customer service","education","food / bev / hosp","general labor","government","human resources",
    "internet engineers","legal / paralegal","manufacturing","marketing / pr / ad","medical / health","nonprofit sector",
    "real estate","retail / wholesale","sales / biz dev","salon / spa / fitness","security","skilled trade / craft",
    "software / qa / dba","systems / network","technical support","transport","tv / film / video","web / info design",
    "writing / editing","[ETC]","[ part-time ]"
    ]
};

var _thumbScale = function(input,size,face) {
    if(input == null)return input;
    if (!size) {
        size = 0.5;
    }
    if(face){
        return input.replace(/upload/, 'upload/w_' + size + ',f_auto,h_' + size + ',c_thumb,g_face').replace(/http:/, 'https:');
    }else{
        return input.replace(/upload/, 'upload/w_' + size + ",f_auto").replace(/http:/, 'https:');
    }

};
//the email notice is 2,push notice is 1.
var typeArray = {"1":"New Followers","2":"Recommendations","3":"Marketing Activities","5":"Offer/Message(s)","6":"Reminders","7":"Courtesy Calls"};
var _filterStatusList = function(statusList) {
    var obj = {"email":[],"push":[],"sms":[],"phone":[]};
    for (var i = 0 ;i < statusList.length;i ++) {
        statusList[i]["typeName"] = typeArray[statusList[i].type];
        statusList[i]["action"] = statusList[i]["state"] == true ? 1:0;
        if (statusList[i].method == 1) {//push
            obj.push.push(statusList[i]);
        } else if (statusList[i].method == 2) {//email
            obj.email.push(statusList[i]);
        } else if (statusList[i].method == 3) {//sms
            obj.sms.push(statusList[i]);
        } else if (statusList[i].method == 4) {//phone
            obj.phone.push(statusList[i]);
        }
    }
    return obj;
}
module.exports = {
    notification : function(req,res) {
        var userToken = req.param('user_token');
        var userId = "1";//(jwt.decode(userToken,secret)).user_id;
        MailService.userSubscriptions({userId:userId,userToken:userToken})
            .then(function(statusList) {
                var list = _filterStatusList(statusList);
                res.render('app_notification/notificationLayout',
                    {"list": list,"userToken":userToken});
            },function(error) {
                res.render('errpage/501.ejs');
            });
    },
    setNotification : function(req,res) {
        var args = req.param("submit_args");
        var userToken = req.param('user_token');
        var userId = "1";//(jwt.decode(userToken,secret)).user_id;

        MailService.subscribe({userId:userId,userToken:userToken,args:args},req)
            .then(function(result) {
                res.json({"status":200});
            },function(key) {
                log.error("setNotification email error 500",key);
                res.json({"status":500});
            });
    },
    service : function(req,res) {
        ejs.filters.thumbScale = function(obj,size,face) {
            return _thumbScale(obj,size,face);
        };
        //home search
//        ItemService.getHomeItemsDistance(req,res).then(function(data) {
//            res.render('app_home/appHomeLayout', {"items":data.objects});
//        },function(error) {
//            res.json(error);
//        });
//        console.log("print::" + JSON.stringify(servObj.service));
        res.locals.layout = "app/service/serviceLayout";

        res.view('app/service/service', {"items":servObj.service});

    },
    job : function(req,res) {
        ejs.filters.thumbScale = function(obj,size,face) {
            return _thumbScale(obj,size,face);
        };
        res.locals.layout = "app/service/serviceLayout";

        categoryServ.getNewCategory(req,res).then(function(data) {
            res.view('app/service/jobs', {"items":data.objects[0].child});
        },function(error) {
            res.json({"status":500});
        });



    },
    house : function(req,res) {
        ejs.filters.thumbScale = function(obj,size,face) {
            return _thumbScale(obj,size,face);
        };
        res.locals.layout = "app/service/serviceLayout";

        res.view('app/service/house', {"items":servObj.house});

    },
    api : function(req,res) {
        res.render('app/service/api', {});
    },
    reviewTopic : function(req,res) {
        ejs.filters.thumbScale = function(obj,size,face) {
            return _thumbScale(obj,size,face);
        };
        res.locals.layout = "app/review/reviewLayout";

        var temp = [
                {"name":"jadon","time":"2015-07-16","desc":"Very Honest person. Just bought paintball guns without trying them. Took them to get tested and guns are in perfect condition like he said. Thank you. Recommend him to anyone trying to buy from him."},
                {"name":"jadon","time":"2015-07-16","desc":"Very Honest person. Just bought paintball guns without trying them. Took them to get tested and guns are in perfect condition like he said. Thank you. Recommend him to anyone trying to buy from him."},
                {"name":"jadon","time":"2015-07-16","desc":"Very Honest person. Just bought paintball guns without trying them. Took them to get tested and guns are in perfect condition like he said. Thank you. Recommend him to anyone trying to buy from him."},
                {"name":"jadon","time":"2015-07-16","desc":"Very Honest person. Just bought paintball guns without trying them. Took them to get tested and guns are in perfect condition like he said. Thank you. Recommend him to anyone trying to buy from him."}
            ];
        res.view('app/review/review', {items:temp});
    }
};

