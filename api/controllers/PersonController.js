/**
 * PersonController
 *
 * @description :: About the person for seller information page
 */

var Q = require('q');
var ejs = require('ejs');
var picCloudPrefix = sails.config.settings.picCloudPrefix;
var tools = require('../services/Tools.js');
var perServ = require('../services/PersonService.js');
var log = require('../services/Log.js');
var categoryDataJson = tools.getCategoryJson();


var _getImgId = function(object,hasContainType){
    if(object.images ==undefined || object.images.length ==0)return;
    var origUrl = object.images[0].imageLink;
    //"http://res.cloudinary.com/fivemiles/image/upload/v1417401619/zf4vqdb6yswlroxxvd8i.jpg"
    var result = origUrl.match(/([^\/]*)\.[^\/]*$/);
    if(!hasContainType){
        return result[1];
    }else{
        return result[0];
    }
}
var _facebookPerson = function(req,res) {
//Need get personInfo and itemInfo to generize item picture in facebook.
    Q.all([perServ.getPersonInfo(req,res),perServ.getMyItems(req,res)]).spread(function(data,itemData){
        if(itemData.objects == undefined || itemData.objects.length ==0){
            data["fbShopIcon"] = data["portrait"];
        }else{
            var positions = [",w_150,h_150,c_fill,g_center,x_150/",",w_150,h_150,c_fill,g_center,y_150,x_-75/",",w_150,h_150,c_fill,g_center,y_75,x_75/"];
            data["fbShopIcon"] = picCloudPrefix+"w_150,h_150,c_fill,g_center/";
            var len=itemData.objects.length>4?4:itemData.objects.length;
            for(var i=1;i<len;i++){
                data["fbShopIcon"] +=("l_"+_getImgId(itemData.objects[i],false)+positions[i-1]);
            }
            data["fbShopIcon"] += _getImgId(itemData.objects[0],true);
        }
        data["url"] = req.baseUrl + "/person/" + req.param("user_id");
        res.render('person/personPlain', {"result": data});

    },function() {
    }).done();
}

module.exports = {
    /**
     * get the person information
     * @param req
     * @param res
     */
    person: function(req, res) {
        ejs.filters.thumb = function(obj) {
            return Tools.thumbFilter(obj,"100","g_face");
        };
        var agent = Tools.judgeClient(req);
        if(!agent.Facebook && !agent.Fragment){
            perServ.getPersonInfo(req,res).then(function(data) {
                var head = {
                    "title":data.nickname + "'s shop - Your Mobile Marketplace",
                    "keywords":data.nickname + ",shop,profile,5miles",
                    "nickname":data.nickname,
                    "city": (data.city==null?"":(data.city+", "))+(data.country==null?"":data.country)
                };
                var downUrl = Tools.getDownloadLinks(req,res);
                res.locals.layout = "person/personLayout";
                var returnData = {"result":data,"down":downUrl,"head":head,"categoryDataJson":categoryDataJson};
                var info = req.params.info;
                if (info) {
                    returnData["ext"] = info;
                }
                res.view('person/person', returnData);
            },function(error) {
                if (error && error.ERROR && error.ERROR.err_code == 9001) {
                    res.forbidden();
                } else {
                    res.notFound();
                }
            });
        } else {
            _facebookPerson(req, res);
        }
    },
    profile: function(req, res) {
        var agent = Tools.judgeClient(req);
        if(!agent.Facebook && !agent.Fragment){
            perServ.getMyInfo(req,res).then(function(data) {
                req.params.info = data;
                module.exports.person(req,res);
            },function(err) {
                if (err.status == tools.ERR.NEED_LOGIN.status) {
                    res.redirect("loginShow/?back=/");
                } else {
                    res.notFound();
                }
            });
        } else {
            _facebookPerson(req, res);
        }
    },
    /**
     * 301 redirect to the other page
     * @param req
     * @param res
     */
    personRedirect: function(req, res) {
        var agent = Tools.judgeClient(req);
        if(!agent.Facebook){
            var userId = req.param("user_id");
            perServ.getPersonInfo(req,res).then(function(data) {
                res.redirect(301,"/person/" + userId + "/" + Tools.titleParse(data.nickname));
            },function(error) {
                res.notFound();
            });
        } else {
            _facebookPerson(req, res);
        }
    },
    /**
     * get the login user's items
     * @param req
     * @param res
     */
    getMyItems: function(req, res) {
        perServ.getMyItems(req,res).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    /**
     * get seller's items use user_id
     */
    getSellerItems: function(req,res){
        perServ.getSellerItems(req,res).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    /**
     * get the person's distance
     * @param req
     * @param res
     */
    personDis: function(req, res) {
        ItemService.getTheDistance(req,res).then(function(data) {
            var dis = Tools.milesFilter(data, false);
            if (data == 0) {
                dis = 1;
            }
            if (dis == "-0.0") {
                dis = -1;
            } else if (dis <= 1) {
                dis += " mile";
            } else {
                dis += " miles";
            }
            var resData = {"distance": dis};
            res.json(resData);
        },function(error) {
            res.json(error);
        });
    },
    getLikerList: function(req,res){
        perServ.getLikerList(req,res).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    getPersonInfo: function(req,res){
        perServ.getPersonInfo(req,res).then(function(data){
            res.json(data);
        },function(error) {
            res.json(error);
        })
    },
    /**
     * get the person's following
     * @param req
     * @param res
     */
    following: function(req, res) {
        perServ.getFollowing(req).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    /**
     * get the person's follower
     * @param req
     * @param res
     */
    follower: function(req, res) {
        perServ.getFollower(req).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    /**
     * get the person's likes
     * @param req
     * @param res
     */
    getMyLikes: function(req, res) {
        perServ.getUserLikes(req).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    /**
     * get the person's purchases
     * @param req
     * @param res
     */
    getMyPurchases: function(req, res) {
        perServ.myPurchases(req).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    /**
     * follow someone
     * @param req
     * @param res
     */
    follow: function(req, res) {
        perServ.follow(req).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    },
    /**
     * unfollow someone
     * @param req
     * @param res
     */
    unfollow: function(req, res) {
        perServ.unfollow(req).then(function(data) {
            res.json(data);
        },function(error) {
            res.json(error);
        });
    }
};
