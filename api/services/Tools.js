var https = require('https');
var http = require('http');
var geoip = require('geoip-lite');
var UAParser = require('ua-parser-js');
var urlDownloadMap = sails.config.settings.urlDownloadMap;
var initCtr = require('../controllers/ABController.js');

module.exports = {
    /**
     * the error object define
     */
    ERR : {
      "NEED_LOGIN": {"ERROR":"need to login","status":555}
    },
    /**
     * depend on the tokens
     *
     **/
    judgeClient: function(request) {
        var ua = request.headers['user-agent'],
            $ = {};
        var parser = new UAParser();
        var browerName = parser.setUA(ua).getResult().browser.name;
        var fragment = request.query._escaped_fragment_;

        if (/mobile/i.test(ua))
            $.Mobile = true;

        if (/like Mac OS X/.test(ua)) {
            $.iOS = /CPU( iPhone)? OS ([0-9\._]+) like Mac OS X/.exec(ua)[2].replace(/_/g, '.');
            $.iPhone = /iPhone/.test(ua);
            $.iPad = /iPad/.test(ua);
        }

        if (/Android/.test(ua))
            $.Android = /Android ([0-9\.]+)[\);]/.exec(ua)[1];

        if (/webOS\//.test(ua))
            $.webOS = /webOS\/([0-9\.]+)[\);]/.exec(ua)[1];

        if (/(Intel|PPC) Mac OS X/.test(ua))
            $.Mac = /(Intel|PPC) Mac OS X ?([0-9\._]*)[\)\;]/.exec(ua)[2].replace(/_/g, '.') || true;

        if (/Windows NT/.test(ua))
            $.Windows = /Windows NT ([0-9\._]+)[\);]/.exec(ua)[1];

        if (browerName && browerName.indexOf("facebookexternalhit") >= 0) {//facebook header
            $.Facebook = true;///facebookexternalhit ([0-9\._]+)[\);]/.exec(ua)[1];
        }
        if (fragment != undefined) {
            $.Fragment = true;
        }
        return $;
    },
    /**
     * judge the mobile client os
     **/
    judgeMbClientOS: function(request) {
        var ua = request.headers['user-agent'],
            $ = {};

        if (/like Mac OS X/.test(ua)) {
            $.IOS = true;
        }

        if (/Android/.test(ua))
            $.Android = true;

        return $;
    },
    /**
     * transfer the number to miles
     * @param number
     * @returns {*}
     * @private
     */
    milesFilter : function(number, suffixFlag) {
        if (number) {
            var res = number / 1609.344;
            if (res <= 1) {
                res = res.toFixed(1);
                if (suffixFlag != false) {
                    res += " mile";
                }
            } else {
                res = res.toFixed(0);
                if (suffixFlag != false) {
                    res += " miles";
                }
            }
            return res;
        } else {
            return "";
        }
    },
    kmFilter : function(number,suffixFlag) {
        if (number) {
            var res = number / 1000;
            if (res <= 1) {
                res = res.toFixed(1);
                if (suffixFlag != false) {
                    res += " km";
                }
            } else {
                res = res.toFixed(0);
                if (suffixFlag != false) {
                    res += " kms";
                }
            }
            return res;
        } else {
            return "";
        }
    },
    // parse title for url
    titleParse : function(url) {
        var res = "";
        if (url) {
            res = url.toLowerCase().trim().replace(/ & /g, " ").replace(/\//g, "").replace(/-/g, "").replace(/%/g, "")
                .replace(/ /g, "-").replace(/,/g, "").replace(/\./g, "").replace(/'/g, "").replace(/"/g, "").replace(/--/g, "-");
        }
        return res;
    },
    /*
     * get download link from different marketing source.
     * @param  {[httpRequest]} req 
     * @return {[array]}     arr[0]:android link|arr[1]:ios link
     */
    getDownloadLinks : function(req,res){
        var utmSource = req.param("utm_source")||req.cookies.utm_source||"default";
        var utmMedia = req.param("utm_medium")||req.cookies.utm_medium ||"default";
        var utmCampaign = req.param("utm_campaign")||req.cookies.utm_campaign||"default";
        var linkMap = [];
        if(urlDownloadMap[utmSource]&&urlDownloadMap[utmSource][utmMedia]&&urlDownloadMap[utmSource][utmMedia][utmCampaign]){
            linkMap = urlDownloadMap[utmSource][utmMedia][utmCampaign];
        } else if ((utmSource.length > 0 &&  utmSource != "default")
            && (utmMedia.length > 0 &&  utmMedia != "default")
            && (utmCampaign.length > 0 &&  utmCampaign != "default")) {
            /**
             * like :&utm_source=parameter_source&utm_medium=CPC&utm_campaign=parameter_campaign
             * auto create url
              */
            var extend = "pid=" + utmSource + "&c=" + utmCampaign;
            linkMap = [urlDownloadMap.baseAndroid + extend,urlDownloadMap.baseIos + extend];
        } else {
            linkMap = urlDownloadMap.default;
        }
        res.cookie("utm_source",utmSource,{maxAge:2*365*24*60*60*1000});
        res.cookie("utm_medium",utmMedia,{maxAge:2*365*24*60*60*1000});
        res.cookie("utm_campaign",utmCampaign,{maxAge:2*365*24*60*60*1000});
        return linkMap;
    },
    // get client ip from http header
    getRemoteIp : function(req) {
        var ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;

        if (/,/.test(ip)) {  // have multiple addresses
            ip = ip.split(/,/)[0].trim();
        }
        return ip;//"58.31.166.0";//"66.228.127.255"
    },
    // get the location from the req
    getTheReqLocation : function(req) {
        //if request has client location info,directly return client location info.
        var clientLat = req.param("clientLat");
        var clientLon = req.param("clientLon");
        if(clientLat != undefined && clientLon != undefined){
            return {latitude:clientLat,longitude:clientLon};
        }
        var ip, loc;
        ip  = module.exports.getRemoteIp(req);

        if (ip) {
            loc = geoip.lookup(ip);
            if (loc && loc.ll) {
                loc = { latitude: loc.ll[0], longitude: loc.ll[1] , "country":loc.country,"city":loc.city};
            }
            else {
                loc = null;
            }
        }
        return loc;
    },
    getClientCookie : function(req) {
        // 获得客户端的Cookie
        var Cookies = {};
        req.headers.cookie && req.headers.cookie.split(';').forEach(function( Cookie ) {
            var parts = Cookie.split('=');
            Cookies[ parts[ 0 ].trim() ] = ( parts[ 1 ] || '' ).trim();
        });
        var obj = {};
        if (Cookies["5miles_head"]) {
            obj = JSON.parse(decodeURIComponent(Cookies["5miles_head"]));
        }
        return obj;
    },
    clearClientCookie : function(req,res) {
        // 获得客户端的Cookie
        var Cookies = {};
        req.headers.cookie && req.headers.cookie.split(';').forEach(function( Cookie ) {
            var parts = Cookie.split('=');
            Cookies[ parts[ 0 ].trim() ] = ( parts[ 1 ] || '' ).trim();
        });
        if (Cookies["5miles_head"]) {
            res.cookie("5miles_head","",{});
        }
    },
    clearClientCookieAndRedirect : function(req,res,returnData) {
        if (returnData.statusCode == 401) {
            // 获得客户端的Cookie
            var Cookies = {};
            req.headers.cookie && req.headers.cookie.split(';').forEach(function( Cookie ) {
                var parts = Cookie.split('=');
                Cookies[ parts[ 0 ].trim() ] = ( parts[ 1 ] || '' ).trim();
            });
            if (Cookies["5miles_head"]) {
                res.cookie("5miles_head","",{});
            }
            res.redirect("/");
        }
    },
    isTokenNotUsed : function(req,res,returnData) {
        if (returnData.statusCode == 401) {
            // 获得客户端的Cookie
            var Cookies = {};
            req.headers.cookie && req.headers.cookie.split(';').forEach(function( Cookie ) {
                var parts = Cookie.split('=');
                Cookies[ parts[ 0 ].trim() ] = ( parts[ 1 ] || '' ).trim();
            });
            if (Cookies["5miles_head"]) {
                res.cookie("5miles_head","",{});
            }
            return true;
        }
        return false;
    },
    /**
     * append cookie value 
     * @param {[http request]} req   http request
     * @param {[http response]} res   http request
     * @param {[type]} key   cookie key
     * @param {[type]} value append cookie data to the end of the cookie value with _
     * @param {[type]} limit the append max number.
     */
    appendClientCookie : function(req,res,key,value,limit){
        var originCookie = req.cookies[key]||null,originCookieArr = originCookie==null?[]:originCookie.split("_");
        originCookieArr.push(value);
        if(originCookieArr.length>limit){
            originCookieArr.splice(0,originCookieArr.length-limit);
        }
        res.cookie(key,originCookieArr.join("_"),{maxAge:2*365*24*60*60*1000});
    },
    parseTitle : function(url){
        var res = url.toLowerCase().trim().replace(/ & /g, " ").replace(/\//g, "").replace(/-/g, "").replace(/%/g, "")
                    .replace(/ /g, "-").replace(/,/g, "").replace(/\./g, "").replace(/'/g, "").replace(/"/g, "").replace(/--/g, "-");
        return res;
    },
    /**
     * the filter for image crop
     * @param input
     * @param size
     * @param mode
     * @returns {XML|string}
     * @private
     */
    thumbFilter : function(input, size, mode) {
        if (!input) {
            return "/images/common/person-head_def.png";
        }

        var REGEXP_IMG = /(\/fivemiles\/image\/upload\/)(.*)$/i;
        var w = 50, h = 50;

        // default & max size is 100
        if (size > 0) {
            w = size;
            h = size;
        }

        // to get the @2x image
        mode = mode ? ',' + mode : '';
        var res = input;

        if (mode && mode.length > 0) {
            if (res.indexOf("facebook") > 0) {
                res = res.replace(/\/facebook/, '/facebook/w_' + w + ',h_' + h + ',c_thumb' + mode).replace(/http:/, 'https:');
            } else {
                res = res.replace(/upload/, 'upload/w_' + w + ',f_auto,h_' + h + ',c_thumb' + mode).replace(/http:/, 'https:');
            }
        } else {
            res = res.replace(/upload/, 'upload/w_' + w + ",f_auto").replace(/http:/, 'https:');
            res = res.replace(/\/facebook/, '/facebook/w_' + w).replace(/http:/, 'https:');
        }
        return res;
    },
    /**
     * get the category info
     */
    getCategoryJson : function() {
        return initCtr.getCategoryJson();
    },
    getCategoryArray : function() {
        return initCtr.getCategoryArray();
    },
    /**
     * get the report reasons list
     */
    getReportArray : function() {
        return initCtr.getReportArray();
    },
    /**
     * extend header
     */
    extendHeader : function(req,options) {
        if (req) {
            options.headers["Accept-Language"] = req.headers["accept-language"];
            options.headers["X-FIVEMILES-SESSION-ID"] = req.sessionID;
        }
        return options;
    }
}

