'use strict';

/*
 * 5 miles api for category.
 */
var request = require('request');
var http = require('http');
var Q = require('q');
var needle = require('needle');
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');

var categoryApiSettings = sails.config.settings.category;

exports.getCategoryInfo = function() {
    var deferred = Q.defer();
    var options = {
        headers: {
        }
    };
    //extend header
    options = tools.extendHeader(null,options);
    // post request
    var url = categoryApiSettings.uri;
    needle.get(url, options,function(error, result) {
        if (error) {
            log.error("getCategoryInfo:",error);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error

                log.error("getCategoryInfo:",body);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};
exports.getNewCategory = function(req,res) {
    var deferred = Q.defer();
    var options = {
        headers: {
            'Content-Type': 'application/json',
            'X-FIVEMILES-USER-TOKEN': categoryApiSettings.usertoken,
            'X-FIVEMILES-USER-ID': categoryApiSettings.userid ,
            'User-Agent':req.headers['user-agent']
        }
    };
    //extend header
    options = tools.extendHeader(null,options);
    // post request
    var url = categoryApiSettings.uri_new;
    needle.get(url, options,function(error, result) {
        if (error) {
            log.error("getCategoryById:",error);
            res.serverError();
        } else {
            var body = result.body;
            var statusCode = result.statusCode;
            if (statusCode == 200) {
                deferred.resolve(body);
            } else {
                log.error("getCategoryById:400:",JSON.stringify(body));
                deferred.reject({"ERROR":body});
            }
        }
    });
    return deferred.promise;
};