var https = require('https');
var http = require('http');
var isJSON = require('is-json');
var querystring = require('querystring');
var Q = require('q');
var needle = require('needle');
var setting = sails.config.settings;
var tools = require('../services/Tools.js');
var log = require('../services/Log.js');

module.exports = {
    deleteAccount : function(req,res,type,parameter){
        var deferred = Q.defer();
        var cookie = tools.getClientCookie(req);
        if (!cookie.userToken) {
            deferred.reject({"status":"400","msg":"Please login!"});
        }
        var userId = cookie.userId;//req.session.user.id;
        var userToken = cookie.userToken;//req.session.user.token;
        var options = {
            headers: {
                'X-FIVEMILES-USER-TOKEN': userToken ,
                'X-FIVEMILES-USER-ID':userId
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        var content = req.param("content");
        parameter = {"comment":content};
        var url = setting.account.uri;

        needle.post(url, parameter, options,function(error, result) {
            if (error) {
                deferred.reject({"status":"400","msg":error});
            } else {
                var obj = result.body;
                var statusCode = result.statusCode;
                if (statusCode == "200") {// success
                    deferred.resolve({"status":"200","msg":"Success"});
                } else {//error
                    log.error("deleteAccount",JSON.stringify(obj));
                    deferred.reject({"status":"400","msg":obj.err_msg});
                }
            }
        });
        return deferred.promise;
    }
}

