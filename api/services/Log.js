/**
 * Dependencies
 */
var sails = require('sails');
var log = sails.log;

var raven = require('raven');
var client = new raven.Client(false);
if (process.env.NODE_ENV == "production") {
    client = new raven.Client('https://448e80f8b46a4145aa01c51a5f8ff152:63bb4926c5354ef2909303acb304f0c6@app.getsentry.com/30778');
    client.patchGlobal();
}
/**
 * Fixtures to be used directly by mocha's `before`, `after`, `beforeEach`,
 * `afterEach`, and `it` methods.
 *
 * @type {Object}
 */
module.exports = {
        silly: function () {
            var args = Array.prototype.slice.call(arguments);
            log.silly(args.join(" "));
        },

        verbose: function () {
            var args = Array.prototype.slice.call(arguments);
            log.verbose(args.join(" "));
        },

        info: function () {
            var args = Array.prototype.slice.call(arguments);
            log.info(args.join(" "));
        },

        blank: function () {
            var args = Array.prototype.slice.call(arguments);
            log.blank(args.join(" "));
        },

        debug: function () {
            var args = Array.prototype.slice.call(arguments);
            log.debug(args.join(" "));
        },

        warn: function () {
            var args = Array.prototype.slice.call(arguments);
            log.warn(args.join(" "));
        },

        error: function () {
            var args = Array.prototype.slice.call(arguments);
            log.error(args.join(" "));
            // 捕获一个异常
            client.captureError(new Error(args.join(" ")));
        }
};


