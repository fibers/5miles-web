'use strict';

/*
 * 5 miles api for items.
 */
var request = require('request');
var http = require('http');
var sails = require('sails');
var Q = require('q');
var needle = require('needle');
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');


var messageSettings = sails.config.settings.newMessageSettings;



/**
 * Fetch new messages infomation
 * @param id [Number] id of the production
 * @return a promised product detail
 */
exports.getNewMsg = function(req) {
    var deferred = Q.defer();
    var options = {};
    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {//have session
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':cookie.userId,
                'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                'User-Agent':req.headers['user-agent']
            }
        };
    }else{
        log.error("Get New Message Error: not log in",error);
        deferred.reject({});
        return deferred.promise;
    }
    var url = messageSettings.url;
    //extend header
    options = tools.extendHeader(req,options);
    needle.get(url, options,function(error, result){
        if (error) {
            log.error("Get New Message Error:",error);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.statusCode == 200) {//success
                deferred.resolve(body);
            } else {// fail
                log.error("Get New Message Error:",JSON.stringify(body));
                deferred.reject({"ERROR":body});
            }
        }
    });

    return deferred.promise;
};
