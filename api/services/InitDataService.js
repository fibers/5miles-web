'use strict';

/*
 * 5 miles api for category.
 */
var request = require('request');
var http = require('http');
var Q = require('q');
var needle = require('needle');
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');

var reportApiSettings = sails.config.settings.feedback;

exports.getReportList = function() {
    var deferred = Q.defer();
    var options = {
        headers: {
        }
    };
    //extend header
    options = tools.extendHeader(null,options);
    // post request
    var url = reportApiSettings.reportReasons;
    needle.get(url, options,function(error, result) {
        var statusCode = result.statusCode;
        if (error) {
            log.error("getReportList:error:",error);
            deferred.reject({"ERROR":error});
        }
        var body = result.body;
        if (statusCode == 200) {// success
            deferred.resolve(body);
        } else {
            log.error("getReportList:",body);
            deferred.reject({"ERROR":body});
        }
    });
    return deferred.promise;
};