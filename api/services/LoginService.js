var https = require('https');
var http = require('http');
var isJSON = require('is-json');
var querystring = require('querystring');
var Q = require('q');
var needle = require('needle');
var login = sails.config.settings.login;
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');


module.exports = {
    /**
     * depend on the tokens
     *
     **/
    login: function(req, res) {
        // header setting
        var options = {
            headers: {
                'User-Agent':req.headers['user-agent']
            }
        };
        var parameter = {
            email:req.param("email"),
            password:req.param("password")
        };
        //extend header
        options = tools.extendHeader(req,options);
        needle.post(login.uri, parameter, options,function(error, result) {
            if (error) {
                log.error('The server login return data: ' + error);
                req.session.authenticated = false;
                res.ok({"status":400,"msg":error});
            }
            var statusCode = result.statusCode;
            var body = result.body;
            if (statusCode == 200) {// success
                var obj = result.body;
                if (obj.token) {
                    req.session.authenticated = true;
                    req.session.user = obj;
                    res.ok(obj);
                } else {
                    req.session.authenticated = false;
                    res.ok(obj);
                }
            } else {//error
                log.error("login:",JSON.stringify(body));
                req.session.authenticated = false;
                res.ok({"status":400,"msg":body.err_msg});
            }


        });
    },
    /**
     * login with facebook
     * @param req
     * @param res
     */
    loginFacebook: function(req, res) {
        var fbUserId = req.param("fb_userid");
        var fbUserName = req.param("fb_username");
        var fbUserImg = req.param("fb_headimage") || "";
        var fbEmail = req.param("fb_email") || "";
        var fbToken = req.param("fb_token") || "";
        var timezone = req.param("timezone");

        var ip = tools.getRemoteIp(req);
        // header setting
        var options = {
            headers: {
                'User-Agent':req.headers['user-agent'],
                'X-FIVEMILES-REAL-IP':ip

            }
        };
        var parameter = {
            "fb_userid": fbUserId,
            "fb_username": fbUserName,
            "fb_headimage": fbUserImg,
            "fb_email": fbEmail,
            "fb_token": fbToken,
            "timezone": timezone
        };
        //extend header
        options = tools.extendHeader(req,options);

        // post request
        needle.post(login.facebookuri, parameter, options,function(error, result) {
            if (error) {
                log.error('The server loginFacebook return data: ' + error);
                req.session.authenticated = false;
                res.ok({"status":400,"msg":error});
            }
            var statusCode = result.statusCode;
            var body = result.body;
            if (statusCode == 200) {// success
                var obj = result.body;
                if (obj.token) {
                    req.session.authenticated = true;
                    req.session.user = obj;
                    res.ok(obj);
                } else {
                    req.session.authenticated = false;
                    res.ok(obj);
                }
            } else {//error
                log.error("loginFacebook:",JSON.stringify(body));
                req.session.authenticated = false;
                res.ok({"status":400,"msg":body.err_msg});
            }


        });
    },
    /**
     * just login out
     * @param req
     * @param res
     */
    loginOut: function(req, res) {
        req.session.authenticated = false;
        res.ok({"SUCCESS":true});
    },
    /**
     * sign new user
     * @param req
     * @param res
     */
    signUp: function(req, res) {
        var deferred = Q.defer();
        var email = req.param("email");
        var password = req.param("password");
        var nickname = req.param("nickname");
        var timezone = req.param("timezone");
        var ip = tools.getRemoteIp(req);
        // header setting
        var options = {
            headers: {
                'User-Agent':req.headers['user-agent'],
                'X-FIVEMILES-REAL-IP':ip
            }
        };
        var parameter = {
            "email": email,
            "password": password,
            "nickname": nickname,
            "timezone": timezone
        };
        //extend header
        options = tools.extendHeader(req,options);
        // post request
        needle.post(login.signuri, parameter, options,function(error, result) {
            var obj = result.body;
            if (result.statusCode == 200) {
                if (obj.token) {
                    deferred.resolve(obj);
                } else {
                    deferred.reject({"status":400,"msg":obj.err_msg});
                }
            } else {
                log.error('The server signUp return data: ' + JSON.stringify(obj));
                deferred.reject({"status":400,"msg":obj.err_msg});
            }
        });
        return deferred.promise;
    },
    /**
     * sign new user
     * @param req
     * @param res
     */
    signUpAndUpload: function(req, res) {
        var email = req.param("email");
        var password = req.param("password");
        var nickname = req.param("nickname");

        module.exports.signUp(req,res).then(function(data) {
            // upload item
        });

    },
    /**
     * send passCode
     * @param req
     * @param res
     */
    sendPassCode: function(req, res) {
        var phone = req.param("phone");
        var deferred = Q.defer();
        // header setting
        var options = {
        };
        var cookie = tools.getClientCookie(req);
        if (cookie.userId) {//have session
            options = {
                headers: {
                    'X-FIVEMILES-USER-ID':cookie.userId,
                    'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                    'User-Agent':req.headers['user-agent']
                }
            };
        }
        var parameter = {
            "phone": phone
        };
        //extend header
        options = tools.extendHeader(req,options);
        // post request
        needle.post(login.sendpasscodeuri, parameter, options,function(error, result) {
            if (error) {
                deferred.reject({"ERROR":error});
            } else {
                var body = result.body;
                if (result.body && result.body.length) {//error
                    deferred.reject({"ERROR":body});
                } else {// success
                    deferred.resolve(body);
                }
            }
        });
        return deferred.promise;
    },
    /**
     * verify passCode
     * @param req
     * @param res
     */
    verifyPassCode: function(req, res) {
        var phone = req.param("phone");
        var passCode = req.param("passCode");
        var deferred = Q.defer();
        // header setting
        var options = {
        };
        var cookie = tools.getClientCookie(req);
        if (cookie.userId) {//have session
            options = {
                headers: {
                    'X-FIVEMILES-USER-ID':cookie.userId,
                    'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                    'User-Agent':req.headers['user-agent']
                }
            };
        }
        var parameter = {
            "phone": phone,
            "pass_code": passCode
        };
        //extend header
        options = tools.extendHeader(req,options);
        // post request
        needle.post(login.verifypasscodeuri, parameter, options,function(error, result) {
            if (error) {
                deferred.reject({"ERROR":error});
            } else {
                var body = result.body;
                if (result.body && result.body.length) {//error
                    deferred.reject({"ERROR":body});
                } else {// success
                    deferred.resolve(body);
                }
            }
        });
        return deferred.promise;
    }
}

