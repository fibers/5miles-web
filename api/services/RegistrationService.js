var https = require('https');
var http = require('http');
var needle = require('needle');
var querystring = require('querystring');
var Q = require('q');
var needle = require('needle');
var extend = require('extend');
var sails = require('sails');
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');

var settings = sails.config.settings;

module.exports = {
    /**
    * get promotion items.
    *
    **/
    verifyEmail: function(req,code){
        // header setting
        var options = {
            headers: {
                'X-FIVEMILES-USER-ID':settings.defaultUserid,
                'X-FIVEMILES-USER-TOKEN':settings.defaultUsertoken
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        var deferred = Q.defer();
        var url = settings.verifyEmail+"?email="+code;

        needle.get(url, options,function(error, result){
            if (error) {
                log.error("Email Verify Error:",error,"code:"+code);
                deferred.reject({"ERROR":error});
            } else {
                var body = result.body;
                if (result.statusCode == 200) {//success
                    deferred.resolve(body);
                } else {// fail
                    log.error("Email Verify Error 2:",body.err_code,body.err_msg,"code:" + code);
                    deferred.reject({"ERROR":body});
                }
            }
        });

        return deferred.promise;
    }
}