var https = require('https');
var http = require('http');
var sails = require('sails');
var needle = require('needle');
var Q = require('q');
var querystring = require('querystring');
var pwdSettings = sails.config.settings.resetpwd;
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');



module.exports = {
    /**depend on the tokens**/
    resetPassword: function(requ,resp) {
        var errorObj = {"ERROR":"Something wrong!"};
        var email = requ.param("email");
        var pwd = requ.param("password");
        if (requ.param("email") && requ.param("password")) {//exist the must params
            var parameter = {
                'email': email,
                'password': pwd
            };
            //extend header
//            opts = tools.extendHeader(requ,opts);
//            var req = http.request(opts, function (res) {
//                res.setEncoding('utf8');
//                var content = '';
//
//                res.on('data', function (chunk) {
//                    content += chunk;
//                });
//
//                res.on('end', function () {
//                    console.log("print:end:" + content);
//                    if (content.indexOf("ok") > 0) {
//                        resp.ok({"SUCCESS":true});
//                    } else {
//                        log.error('The server return data: ' + content);
//                        resp.ok({"ERROR":content});
//                    }
//                });
//            });
//            req.write(strParameter);
//            req.on('error', function (e) {
//                log.error('Request error: ' + e.message);
//                resp.ok(errorObj);
//            });
//            req.end();
            var options = {
                headers: {
                    'X-FIVEMILES-USER-TOKEN':pwdSettings.usertoken,
                    'X-FIVEMILES-USER-ID':pwdSettings.userid,
                    'User-Agent':requ.headers['user-agent']
                }
            };
            //extend header
            options = tools.extendHeader(requ,options);
            // post request
            needle.post(pwdSettings.pathuri, parameter, options,function(error, result) {
                if (error) {
                    log.error("resetPassword:",error);
                    resp.ok({"status":400,"msg":error});
                } else {
                    var statusCode = result.statusCode;
                    var body = result.body;
                    if (statusCode == 200) {// success
                        resp.ok({"status":200,"msg":"Success"});
                    } else {//error
                        log.error("resetPassword::",JSON.stringify(body));
                        resp.ok({"status":400,"msg":body.err_msg});

                    }
                }
            });
        } else {
            return resp.ok(errorObj);
        }
    },
    resetEmail: function(req,res) {
        var deferred = Q.defer();
        var email = req.param("email");
        // header setting
        var options = {
            headers: {
                'User-Agent':req.headers['user-agent']
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        // post request
        var url = pwdSettings.forgeturi + "?email=" + email;

        needle.get(url, options,function(error, result) {
            if (error) {
                log.error("resetEmail:",error);
                deferred.reject({"status":400,"msg":error});
            } else {
                var statusCode = result.statusCode;
                var body = result.body;
                if (statusCode == 200) {// success
                    deferred.resolve({"status":200,"msg":"Success"});
                } else {//error
                    log.error("resetEmail::",JSON.stringify(body));
                    deferred.reject({"status":400,"msg":body.err_msg});
                }
            }
        });
        return deferred.promise;
    }
}