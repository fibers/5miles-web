var https = require('https');
var http = require('http');
var isJSON = require('is-json');
var querystring = require('querystring');
var Q = require('q');
var needle = require('needle');
var setting = sails.config.settings;
var tools = require('../services/Tools.js');
var log = require('../services/Log.js');

module.exports = {
    /**
     * depend on the tokens
     *
     **/
    feedback: function(req, res) {
        // header setting
        var options = {};
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':req.param("id"),
                'X-FIVEMILES-USER-TOKEN':req.param("token"),
                'User-Agent':req.headers['user-agent']
            }
        };
        var parameter = {
            text:req.param("text")
        };
        //extend header
        options = tools.extendHeader(req,options);
        needle.post(setting.feedback.uri, parameter, options,function(error, result) {
            if (error) {
                log.error('The server feedback return data: ' + error);
                res.ok({"status":400,"msg":error});
            } else {
                var statusCode = result.statusCode;
                var body = result.body;
                if (200 <= statusCode < 300) {// success
                    res.ok({"status":200,"msg":body});
                } else {//error
                    log.error("feedback::",JSON.stringify(body));
                    res.ok({"status":400,"msg":body.err_msg});

                }
            }
        });
    },
    report : function(req,res,type,parameter){
        // header setting
        var cookie = {};
        var deferred = Q.defer();
        cookie = tools.getClientCookie(req);
        if (!cookie.userToken) {
            //not login use default userid and usertoken.
            deferred.reject({"ERROR":"invalide user"});
        }
        var userId = cookie.userId;//req.session.user.id;
        var userToken = cookie.userToken;//req.session.user.token;
        var options = {
            headers: {
                'X-FIVEMILES-USER-TOKEN': userToken ,
                'X-FIVEMILES-USER-ID':userId 
            }
        };
        if(type=="item"){
            var url = setting.feedback.reportItemUri;
        }else{
            var url = setting.feedback.reportSellerUri;
        }
        //extend header
        options = tools.extendHeader(req,options);
        needle.post(url, parameter, options,function(error, result) {
            if (error) {
                deferred.reject({"ERROR":error});
            } else {
                var body = result.body;
                if (result.body && result.body.length) {//error
                    deferred.reject({"ERROR":body});
                } else {// success
                    deferred.resolve({});
                }
            }
        });

        return deferred.promise;

    }
}

