var https = require('https');
var http = require('http');
var needle = require('needle');
var querystring = require('querystring');
var Q = require('q');
var needle = require('needle');
var extend = require('extend');
var sails = require('sails');
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');


var mailSettings = sails.config.settings.mail;
var loginSettings = sails.config.settings.login;
var msgSettings = sails.config.settings.msg;
var settings = sails.config.settings;

module.exports = {
    /**
    * get users subscriptions list. contain push and email list.
    *
    **/
    userSubscriptions: function(userInfo,req){
        // header setting
        var options = {
            headers: {
                'X-FIVEMILES-USER-ID':userInfo.userId,
                'X-FIVEMILES-USER-TOKEN':userInfo.userToken
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        var deferred = Q.defer();
        // get request
        needle.get(mailSettings.userSubscriptions, options,function(error, result){
            if (error) {
                log.error("userSubscriptions:500:",error);
                deferred.reject({"ERROR":error});
            } else {
                var body = result.body;
                if (result.statusCode == 200) {//success
                    deferred.resolve(body);
                } else {// fail
                    log.error("userSubscriptions:400:",JSON.stringify(body));
                    deferred.reject({"ERROR":body});
                }
            }
        });

        return deferred.promise;
    },

    bulkUnsubscribe: function(userInfo,req) {
        // header setting
        var options = {
            headers: {
                'X-FIVEMILES-USER-ID':userInfo.userId,
                'X-FIVEMILES-USER-TOKEN':userInfo.userToken
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        var deferred = Q.defer();
        needle.post(mailSettings.uri, userInfo.args, options,function(error, result) {
            if (error) {
                log.error('unsubscribe:error: ' + error);
                deferred.reject({status:500,"ERROR":error});
            }
            if (result.statusCode == 200) {// ok
                deferred.resolve({status:200,"result":result});
            } else { // error
                log.error('unsubscribe: ' + result);
                deferred.reject({status:500,"ERROR":error});
            }
        });

        return deferred.promise;
    },
    subscribe: function(userInfo,req) {
        // header setting
        var options = {
            headers: {
                'X-FIVEMILES-USER-ID':userInfo.userId,
                'X-FIVEMILES-USER-TOKEN':userInfo.userToken
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        var deferred = Q.defer();
        needle.post(mailSettings.subscribeuri, userInfo.args, options,function(error, result) {
            if (error) {
                log.error('subscribe:error: ' + error);
                deferred.reject({status:500,"ERROR":error});
            }
            if (result && result.statusCode == 200) {// ok
                deferred.resolve({status:200,"result":result});
            } else { // error
                log.error('subscribe: ' + result);
                deferred.reject({status:500,"ERROR":error});
            }
        });

        return deferred.promise;
    },
    sendEmail: function(mailAddr,req){
        // header setting
        var options = {
            headers: {
                'X-FIVEMILES-USER-ID':settings.defaultUserid,
                'X-FIVEMILES-USER-TOKEN':settings.defaultUsertoken
            }
        };
        postData = {
            to: mailAddr,
            template: mailSettings.tmplName
        };
        //extend header
        options = tools.extendHeader(req,options);
        var deferred = Q.defer();
        needle.post(mailSettings.sendDirectUri, postData, options,function(error, result) {
            if (error) {
                log.error('sendEmail: ' + error);
                deferred.reject({status:500,"ERROR":error});
            }
            if (result != undefined && (result.statusCode == 200||result.statusCode == 201)) {// ok
                deferred.resolve({status:200,"result":result});
            } else { // error
                log.error('sendEmail:' + result);
                deferred.reject({status:500,"ERROR":error});
            }
        });
    },
    /**
    * mail chat: for the first time sending,register the user
    */
    checkMail: function(req,res){
        // header setting
        var ip = tools.getRemoteIp(req);
        var options = {
            headers: {
                'X-FIVEMILES-REAL-IP':ip
            }
        };
        var deferred = Q.defer();
        var email = req.param("from").user_email;
        var nickName = req.param("from").user_nick;
        var parameter = {
            "email":email,
            "nickname":nickName
        };
        //extend header
        options = tools.extendHeader(req,options);
        needle.post(loginSettings.signweburi, parameter, options,function(error, result) {
            var res = {"status":result.statusCode,"body":result.body};
                deferred.resolve(res);
        });
        return deferred.promise;
    },
    /**
     * mail chat: send the offer to seller
     */
    sendOffer: function(req,res,userInfo){
        var options = {
            headers: {
                'X-FIVEMILES-USER-ID':userInfo.head.userId,
                'X-FIVEMILES-USER-TOKEN':userInfo.head.userToken
            }
        };
        //extend header
        options = tools.extendHeader(req,options);

        var deferred = Q.defer();
        var itemId = userInfo["item_id"];
        var price = userInfo["price"];
        var toUser = userInfo["to_user"];

        var parameter = {
            "item_id":itemId,
            "price":price,
            "to_user":toUser
        };

        needle.post(msgSettings.uri, parameter, options,function(error, result) {
            if (error) {
                log.error('sendOffer:' + JSON.stringify(error));
                deferred.resolve({status:405,"body":error});
            } else {
                deferred.resolve({status:200,"body":result.body});
            }
        });
        return deferred.promise;
    },
    /**
     * mail chat: send the message to seller
     */
    sendAsk: function(req,res,userInfo){
        var options = {
            headers: {
                'X-FIVEMILES-USER-ID':userInfo.head.userId,
                'X-FIVEMILES-USER-TOKEN':userInfo.head.userToken
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        var deferred = Q.defer();
        var itemId = userInfo["item_id"];
        var price = userInfo["price"];
        var text = userInfo["text"];
        var toUser = userInfo["to_user"];

        var parameter = {
            "item_id":itemId,
            "price":price,
            "text":text,
            "to_user":toUser
        };
        needle.post(msgSettings.uri, parameter, options,function(error, result) {
            if (error) {
                log.error('sendAsk: ' + JSON.stringify(error));
                deferred.resolve({status:405,"body":error});
            } else {
                deferred.resolve({status:200,"body":result.body});
            }
        });
        return deferred.promise;
    },
    _test: function(req,res){
        var ip = tools.getRemoteIp(req);
        var options = {
            headers: {
                'X-FIVEMILES-USER-ID':settings.defaultUsertoken,
                'X-FIVEMILES-USER-TOKEN':settings.defaultUsertoken,
                'X-FIVEMILES-REAL-IP':ip
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        var deferred = Q.defer();

        needle.get(msgSettings.uri_test, options,function(error, result) {
            if (error) {
                log.error('_test: ' + JSON.stringify(error));
                deferred.resolve({status:405,"body":error});
            } else {
                deferred.resolve({status:200,"body":result.body});
            }
        });
        return deferred.promise;
    }
}