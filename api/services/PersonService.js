'use strict';

/*
 * 5 miles api for person.
 */
var request = require('request');
var http = require('http');
var Q = require('q');
var needle = require('needle');
var extend = require('extend');
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');


var personApiSettings = sails.config.settings.person;
exports.getFollowing = function(req) {
    var deferred = Q.defer();
    var userId = req.param("user_id");
    var options = {
        headers: {
            'X-FIVEMILES-USER-ID':personApiSettings.userid,
            'X-FIVEMILES-USER-TOKEN':personApiSettings.usertoken,
            'User-Agent':req.headers['user-agent']
        }
    };
    var offset = 0;
    var limit = 10;
    if (req.param("limit")) {
        limit = req.param("limit");
    }
    var normalUrl = "&offset=" + offset + "&limit=" + limit;
    if (req.param("next")) {
        normalUrl = "&" + req.param("next").substring(1);
    }
    // post request
    var url = personApiSettings.followinguri + userId + normalUrl;
    needle.get(url, options,function(error, result) {
        if (error) {
            var refer = req.headers['referer'];
            var agent = req.headers['user-agent'];
            log.error("getFollowing:",error,userId,":refer:" + refer,":agent:" + agent);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];

                log.error("getFollowing:",body,userId,":refer:" + refer,":agent:" + agent);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};
exports.getFollower = function(req) {
    var deferred = Q.defer();
    var userId = req.param("user_id");
    var options = {
        headers: {
            'X-FIVEMILES-USER-ID':personApiSettings.userid,
            'X-FIVEMILES-USER-TOKEN':personApiSettings.usertoken,
            'User-Agent':req.headers['user-agent']
        }
    };
    var offset = 0;
    var limit = 10;
    if (req.param("limit")) {
        limit = req.param("limit");
    }
    var normalUrl = "&offset=" + offset + "&limit=" + limit;
    if (req.param("next")) {
        normalUrl = "&" + req.param("next").substring(1);
    }
    // post request
    var url = personApiSettings.followeruri + userId + normalUrl;
    needle.get(url, options,function(error, result) {
        if (error) {
            var refer = req.headers['referer'];
            var agent = req.headers['user-agent'];
            log.error("getFollower:",error,userId,":refer:" + refer,":agent:" + agent);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];

                log.error("getFollower:",body,userId,":refer:" + refer,":agent:" + agent);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};
/**
 * get login user's likes
 * @param req
 * @returns
 */
exports.getUserLikes = function(req) {
    var deferred = Q.defer();
    var options = {};
    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {//have session
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':cookie.userId,
                'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                'User-Agent':req.headers['user-agent']
            }
        };
    }
    var offset = 0;
    var limit = 10;
    if (req.param("limit")) {
        limit = req.param("limit");
    }
    var normalUrl = "?offset=" + offset + "&limit=" + limit;
    if (req.param("next")) {
        normalUrl = req.param("next");
    }
    // post request
    var url = personApiSettings.userlikesuri + normalUrl;
    needle.get(url, options,function(error, result) {
        if (error) {
            var refer = req.headers['referer'];
            var agent = req.headers['user-agent'];
            log.error("getUserLikes:",error,userId,":refer:" + refer,":agent:" + agent);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];

                log.error("getUserLikes:",body,cookie.userId,":refer:" + refer,":agent:" + agent);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};
/**
 * get login user's purchases
 * @param req
 * @returns
 */
exports.myPurchases = function(req) {
    var deferred = Q.defer();
    var options = {};
    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {//have session
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':cookie.userId,
                'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                'User-Agent':req.headers['user-agent']
            }
        };
    }
    var offset = 0;
    var limit = 10;
    if (req.param("limit")) {
        limit = req.param("limit");
    }
    var normalUrl = "?offset=" + offset + "&limit=" + limit;
    if (req.param("next")) {
        normalUrl = req.param("next");
    }

    // post request
    var url = personApiSettings.mypurchasesuri + normalUrl;
    needle.get(url, options,function(error, result) {
        if (error) {
            var refer = req.headers['referer'];
            var agent = req.headers['user-agent'];
            log.error("getUserLikes:",error,cookie.userId,":refer:" + refer,":agent:" + agent);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];
                log.error("getUserLikes:",body,cookie.userId,":refer:" + refer,":agent:" + agent);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};

exports.follow = function(req) {
    var deferred = Q.defer();
    var followUserId = req.param("user_id");
    var cookie = tools.getClientCookie(req);
    if (!cookie.userToken) {
        deferred.reject({"ERROR":"invalide user"});
    }
    var userId = cookie.userId;//req.session.user.id;
    var userToken = cookie.userToken;//req.session.user.token;
    var options = {
        headers: {
            'X-FIVEMILES-USER-ID':userId,
            'X-FIVEMILES-USER-TOKEN':userToken,
            'User-Agent':req.headers['user-agent']
        }
    };
    // post data
    var parameter = {
        "user_id":followUserId
    };
    // post request
    var url = personApiSettings.followuri;
    needle.post(url, parameter,options,function(error, result) {
        if (error) {
            var refer = req.headers['referer'];
            var agent = req.headers['user-agent'];
            log.error("getFollower:",error,userId,":refer:" + refer,":agent:" + agent);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];

                log.error("getFollower:",body,userId,":refer:" + refer,":agent:" + agent);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};
exports.unfollow = function(req) {
    var deferred = Q.defer();
    var followUserId = req.param("user_id");
    var cookie = tools.getClientCookie(req);
    if (!cookie.userToken) {
        deferred.reject({"ERROR":"invalide user"});
    }
    var userId = cookie.userId;//req.session.user.id;
    var userToken = cookie.userToken;//req.session.user.token;
    var options = {
        headers: {
            'X-FIVEMILES-USER-ID':userId,
            'X-FIVEMILES-USER-TOKEN':userToken,
            'User-Agent':req.headers['user-agent']
        }
    };
    // post data
    var parameter = {
        "user_id":followUserId
    };
    // post request
    var url = personApiSettings.unfollowuri;
    needle.post(url, parameter,options,function(error, result) {
        if (error) {
            var refer = req.headers['referer'];
            var agent = req.headers['user-agent'];
            log.error("unfollow:",error,userId,":refer:" + refer,":agent:" + agent);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];

                log.error("unfollow:",body,userId,":refer:" + refer,":agent:" + agent);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};

exports.getMyItems = function(req,res) {
    var deferred = Q.defer();
    var sellId = "";
    // header setting
    var options = {};

    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {//have session
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':cookie.userId,
                'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                'User-Agent':req.headers['user-agent']
            }
        };
        sellId = cookie.userId;
    } else {
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':personApiSettings.userid,
                'X-FIVEMILES-USER-TOKEN':personApiSettings.usertoken,
                'User-Agent':req.headers['user-agent']
            }
        };
    }

    if (req.body && req.body.user_id) { // direct get
        sellId = req.body.user_id;
    } else if(req.originalUrl.indexOf("person/")!=-1){
        //use url's person id.
        var temp = req.originalUrl.split("person/")[1];
        sellId = temp.split("/")[0];
    }
    var offset = 0;
    var limit = 10;
    if (req.param("limit")) {
        limit = req.param("limit");
    }
    var normalUrl = "&offset=" + offset + "&limit=" + limit;
    if (req.param("next")) {
        normalUrl = "&" + req.param("next").substring(1);
    }
    // post request
    var sellUrl = personApiSettings.myselluri + "?seller_id=" + sellId + normalUrl;
    needle.get(sellUrl, options,function(error, result) {
        if (error) {
            var refer = req.headers['referer'];
            var agent = req.headers['user-agent'];
            log.error("getMyItems:",error,sellId,":refer:" + refer,":agent:" + agent);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                var refer = req.headers['referer'];
                var agent = req.headers['user-agent'];
                log.error("getMyItems:",body,sellId,":refer:" + refer,":agent:" + agent);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};


exports.getSellerItems = function(req,res) {
    var deferred = Q.defer();
    var sellId = "";
    // header setting
    var options = {};
    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {//have session
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':cookie.userId,
                'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                'User-Agent':req.headers['user-agent']
            }
        };
        sellId = cookie.userId;
    } else {
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':personApiSettings.userid,
                'X-FIVEMILES-USER-TOKEN':personApiSettings.usertoken,
                'User-Agent':req.headers['user-agent']
            }
        };
    }
    if (req.body && req.body.user_id) { //post way direct get
        sellId = req.body.user_id;
    } else if(req.originalUrl.indexOf("person/")!=-1){//facebook access
        var temp = req.originalUrl.split("person/")[1];
        sellId = temp.split("/")[0];
    } else if (req.param("user_id")) {
        sellId = req.param("user_id");
    }
    var offset = 0;
    var limit = 10;
    if (req.param("limit")) {
        limit = req.param("limit");
    }
    var normalUrl = "&offset=" + offset + "&limit=" + limit;
    if (req.param("next")) {
        normalUrl = "&" + req.param("next").substring(1);
    }
    // post request
    var sellUrl = personApiSettings.selluri + "?seller_id=" + sellId + normalUrl;
    needle.get(sellUrl, options,function(error, result) {
        if (error) {
            log.error("getSellerItems:",error);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                log.error("getSellerItems:",body);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};

/**
 * Fetch liker list by item id
 * @param id [Number] id of the production
 * @return a promised product detail
 */
exports.getLikerList = function(req,id) {
    var id = req.param("prod_id");
    var url = personApiSettings.likeruri + id+"&offset="+personApiSettings.offset+"&limit="+personApiSettings.limit;
    var options = {
        headers: {
            'Content-Type': 'application/json',
            'X-FIVEMILES-USER-TOKEN':personApiSettings.usertoken,
            'X-FIVEMILES-USER-ID':personApiSettings.userid,
            'User-Agent':req.headers['user-agent']
        }
    };
    //extend header
    options = tools.extendHeader(req,options);
    var deferred = Q.defer();

    needle.get(url, options,function(error, result) {
        if (error) {
            log.error("getLikerList:",error);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                log.error("get liker list error ",personApiSettings.userid);
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};

exports.getPersonInfo = function(req,res) {
    var deferred = Q.defer();
    var userId = req.param("user_id");

    var options = {};
    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':cookie.userId,
                'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                'User-Agent':req.headers['user-agent']
            }
        };
    } else {
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':personApiSettings.userid,
                'X-FIVEMILES-USER-TOKEN':personApiSettings.usertoken,
                'User-Agent':req.headers['user-agent']
            }
        };
    }
    //extend header
    options = tools.extendHeader(req,options);
    // post request
    var url = personApiSettings.userdetail + userId;
    needle.get(url, options,function(error, result) {
        if (error) {
            log.error("getPersonInfo:500:",error,userId);
            res.serverError();
        } else {
            var body = result.body;
            var statusCode = result.statusCode;
            if (statusCode == 200) {
                deferred.resolve(body);
            } else if (statusCode == 401) {//token error
                log.error("getPersonInfo:401:",JSON.stringify(body),userId);
                tools.clearClientCookieAndRedirect(req,res,result);
                deferred.reject({"ERROR":body});
            } else {
                log.error("getPersonInfo:400:",JSON.stringify(body),userId);
                deferred.reject({"ERROR":body});
            }
        }
    });
    return deferred.promise;
};
exports.getMyInfo = function(req,res) {
    var deferred = Q.defer();
    var options = {};
    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':cookie.userId,
                'X-FIVEMILES-USER-TOKEN':cookie.userToken,
                'User-Agent':req.headers['user-agent']
            }
        };
        //extend header
        options = tools.extendHeader(req,options);
        // post request
        var url = personApiSettings.infouri;
        needle.get(url, options,function(error, result) {
            var userId = cookie.userId;

            if (error) {
                log.error("getMyInfo:500:",error,userId);
                res.serverError();
            } else {
                var body = result.body;
                var statusCode = result.statusCode;
                if (statusCode == 200) {
                    deferred.resolve(body);
                } else if (statusCode == 401) {//token error
                    log.error("getMyInfo:401:",JSON.stringify(body),userId);
                    tools.clearClientCookieAndRedirect(req,res,result);
                    deferred.reject({"ERROR":body});
                } else {
                    log.error("getMyInfo:400:",JSON.stringify(body),userId);
                    deferred.reject({"ERROR":body});
                }
            }
        });
    } else {
        deferred.reject(tools.ERR.NEED_LOGIN);
    }
    return deferred.promise;
};
