var AWS = require('aws-sdk');
var sesSettings = sails.config.settings.ses;

AWS.config.update({
    accessKeyId: sesSettings.accessKey,
    secretAccessKey: sesSettings.secretKey,
    region: sesSettings.region,
    maxRetries: 5,
    logger: sails.log
});

var ses = new AWS.SES();

module.exports = {
    sendEmail: function (to, title, content) {

        var params = {
            Source: 'support@5milesapp.com',
            Destination: {
                ToAddresses: [ to ]
            },
            Message: {
                Subject: {
                    Data: title,
                    Charset: 'utf-8'
                },
                Body: {
                    Text: {
                        Data: content,
                        Charset: 'utf-8'
                    }
                }
            }
        };

        ses.sendEmail(params, function (err, data) {
            if (err) {
                sails.log.error(err, err.stack);
            }
        });
    }
}