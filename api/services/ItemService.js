'use strict';

/*
 * 5 miles api for items.
 */
var request = require('request'),
    geolib = require('geolib');
var http = require('http');
var sails = require('sails');
var Q = require('q');
var needle = require('needle');
var extend = require('extend');
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');
var cat = tools.getCategoryJson();

var querystring = require('querystring');
var itemApiSettings = sails.config.settings.item;
var personApiSettings = sails.config.settings.person;
var likerApiSettings = sails.config.settings.liker;

// validate coordinate
var isValidCoordinate = function (coords) {
    return coords && _.isNumber(coords.latitude) && _.isNumber(coords.longitude);
};


/**
 * Fetch details of a product
 * @param id [Number] id of the production
 * @return a promised product detail
 */
exports.getProductDetail = function(req,id,res) {
    // header setting
    var cookie = tools.getClientCookie(req);
    if (!cookie.userToken) {
        //not login use default userid and usertoken.
        cookie.userId = itemApiSettings.userid;
        cookie.userToken = itemApiSettings.usertoken;
    }
    var userId = cookie.userId;//req.session.user.id;
    var userToken = cookie.userToken;//req.session.user.token;
    var options = {
        headers: {
            'Content-Type': 'application/json',
            'X-FIVEMILES-USER-TOKEN': userToken,
            'X-FIVEMILES-USER-ID':userId ,
            'User-Agent':req.headers['user-agent']
        }
    };
    //extend header
    options = tools.extendHeader(req,options);
    var deferred = Q.defer();

    var url = itemApiSettings.uri + id;
    needle.get(url, options,function(error, result) {
        if (error) {
            log.error("getProductDetail:500:",id,error);
            res.serverError();
        } else {
            var statusCode = result.statusCode;
            var body = result.body;
            if (statusCode == 200) {// success
                var item = result.body;
                //add item category title by cat_id;
                var category = cat[item.cat_id];
                if(category){
                    item.cat_info = category;
                }
                deferred.resolve(item);
            } else if (statusCode == 401){//token error
                log.error("getProductDetail:401:",JSON.stringify(body),id);
                tools.clearClientCookieAndRedirect(req,res,result);
                deferred.reject("error");
            } else {
                log.error("getProductDetail:400:",JSON.stringify(body),url,JSON.stringify(options),statusCode);
                deferred.reject("error");
            }
        }
    });
    return deferred.promise;
};
/**
 * Fetch liker list by item id
 * @param id [Number] id of the production
 * @return a promised product detail
 */
exports.getLikerList = function(req,id) {
    var id = req.param("prod_id");
    var url = likerApiSettings.likeruri + id+"&offset="+likerApiSettings.offset+"&limit="+likerApiSettings.limit;
    var options = {
        headers: {
            'Content-Type': 'application/json',
            'X-FIVEMILES-USER-TOKEN':likerApiSettings.usertoken,
            'X-FIVEMILES-USER-ID':likerApiSettings.userid,
            'User-Agent':req.headers['user-agent']
        }
    };
    //extend header
    options = tools.extendHeader(req,options);
    var deferred = Q.defer();
    needle.get(url, options,function(error, result) {
        if (error) {
            log.error("getPersonInfo:",error);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                log.error("get liker list error");
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};

exports.like = function(req,res,itemId) {
    return likeOp(req,res,itemId,itemApiSettings.likeuri);

}
exports.unlike = function(req,res,itemId) {
    return likeOp(req,res,itemId,itemApiSettings.unlikeuri);

}
var likeOp = function(req,res,itemId,op) {
    // header setting
    var cookie = {};
    cookie = tools.getClientCookie(req);
    if (!cookie.userToken) {
        //not login use default userid and usertoken.
        cookie.userId = itemApiSettings.userid;
        cookie.userToken = itemApiSettings.usertoken;
    }
    var userId = cookie.userId;//req.session.user.id;
    var userToken = cookie.userToken;//req.session.user.token;
    var options = {
        headers: {
            'X-FIVEMILES-USER-TOKEN': userToken ,
            'X-FIVEMILES-USER-ID':userId 
        }
    };

    //extend header
    options = tools.extendHeader(req,options);
    var deferred = Q.defer();
    needle.post(op, {"item_id":itemId}, options,function(error, result) {
        if (error) {
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve({});
            }
        }
    });

    return deferred.promise;

}
exports.follow = function(req,res,userId){
    return followOp(req,res,userId,itemApiSettings.followuri);
}
exports.unfollow = function(req,res,userId){
    return followOp(req,res,userId,itemApiSettings.unfollowuri);
}
var followOp = function(req,res,itemOwnerId,op){
    // header setting
    var deferred = Q.defer();

    var cookie = {};
    cookie = tools.getClientCookie(req);
    if (!cookie.userToken) {
        //not login use default userid and usertoken.
        deferred.reject({"ERROR":"invalide user"});
    }
    var userId = cookie.userId;//req.session.user.id;
    var userToken = cookie.userToken;//req.session.user.token;
    var options = {
        headers: {
            'X-FIVEMILES-USER-TOKEN': userToken ,
            'X-FIVEMILES-USER-ID':userId 
        }
    };
    //extend header
    options = tools.extendHeader(req,options);
    needle.post(op, {"user_id":itemOwnerId}, options,function(error, result) {
        if (error) {
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve({});
            }
        }
    });
    return deferred.promise;
}
/**
 * get the items with recommend
 * @param itemId
 * @param lat
 * @param lon
 * @returns {promise}
 */
exports.getProductRecommend = function(req,itemId,lat,lon) {
    var ip = tools.getRemoteIp(req);
    var options = {
        url: itemApiSettings.suggesturi + "?item_id=" + itemId + "&lat=" + lat + "&lon=" + lon,
        headers: {
            'Content-Type': 'application/json',
            'X-FIVEMILES-USER-TOKEN':itemApiSettings.usertoken,
            'X-FIVEMILES-USER-ID':itemApiSettings.userid,
            'X-FIVEMILES-REAL-IP':ip,
            'User-Agent':req.headers['user-agent']
        }
    };
    //extend header
    options = tools.extendHeader(req,options);
    var deferred = Q.defer();
    request(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var item = JSON.parse(body);
            deferred.resolve(item);
        } else {
            deferred.reject("error");
        }
    });
    return deferred.promise;
};


/**
 * Fetch details of a product, and calc distance based on a given coordinate.
 * @param id [Number] id of the production
 * @param coordinate [Object] client location for distance calculation, optional
 * latitude [Number]
 * longitude [Number]
 * @return a promised product detail
 */
exports.getProductDetailWithDistance = function(id, req,res) {
    return exports.getProductDetail(req,id,res).then(function (detail) {
        //transfer the location info into validate data
        if (detail.location && detail.location.lat && detail.location.lon) {
            detail.latitude = detail.location.lat;
            detail.longitude = detail.location.lon;
            detail.latBase = new Buffer(detail.latitude + "").toString("base64");
            detail.lonBase = new Buffer(detail.longitude + "").toString("base64");
        }

        var coordinate = tools.getTheReqLocation(req);
        if (isValidCoordinate(coordinate) && isValidCoordinate(detail)) {
            detail.distance = tools.milesFilter(geolib.getDistance(coordinate, detail));
        }
        var updateTime = detail.updated_at;
        var now = (new Date()).getTime()/1000;
        var span = Math.round(now - updateTime);
        var timeSpanConfig = [

            {minTime:60*60*24*31*2,factor:60*60*24*31,txt:"#time# months ago"},
            {minTime:60*60*24*31,factor:60*60*24*31,txt:"#time# month ago"},
            {minTime:60*60*24*2,factor:60*60*24,txt:"#time# days ago"},
            {minTime:60*60*24,factor:60*60*24,txt:"#time# day ago"},
            {minTime:60*60*2,factor:60*60,txt:"#time# hours ago"},
            {minTime:60*60,factor:60*60,txt:"#time# hour ago"},
            {minTime:60*2,factor:60,txt:"#time# minutes ago"},
            {minTime:60,factor:60,txt:"#time# minute ago"},
            {minTime:1,factor:1,txt:"Just now"}
        ];
        detail.updatedTimeTxt = "Just now";
        for(var i in timeSpanConfig){
            var item = timeSpanConfig[i];
            if((span - item.minTime)>0){
                //less than this time span.
                var time = Math.floor(span/item.factor);
                detail.updatedTimeTxt = item.txt.replace("#time#",time);
                break;
            }
        }

        return detail;
    });
};

/**
 * post the item
 * @param req
 * @param res
 * @returns promise
 */
exports.postItem = function(req, res, header) {
    // use the ip to get the lat and lon
    var obj = tools.getTheReqLocation(req);
    var lat = (obj && obj.latitude) || 0;
    var lon = (obj && obj.longitude) || 0;
    var deferred = Q.defer();
    // post data
    var parameter = {
        "title":req.body.title,
        "category":req.body.category,
        "shipping_method":req.body.shipping_method,
        "images":req.body.images,
        "mediaLink":req.body.mediaLink,
        "currency":req.body.currency,
        "price":req.body.price,
        "desc":req.body.description,
        "lat":lat,
        "lon":lon
    };
    // header setting
    var cookie = {};
    if (header) {
        cookie = header;
    } else {
        cookie = tools.getClientCookie(req);
        if (!cookie.userToken) {
            deferred.reject({"ERROR":"invalide user"});
        }
    }
    var userId = cookie.userId;//req.session.user.id;
    var userToken = cookie.userToken;//req.session.user.token;

    var ip = tools.getRemoteIp(req);
    var options = {
        headers: {
            'X-FIVEMILES-USER-ID':userId,
            'X-FIVEMILES-USER-TOKEN':userToken,
            'User-Agent':req.headers['user-agent'],
            'X-FIVEMILES-REAL-IP':ip
        }
    };
    //extend header
    options = tools.extendHeader(req,options);
    // post request
    needle.post(itemApiSettings.posturi, parameter, options,function(error, result) {
        if (error) {
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                deferred.reject({"ERROR":body});
            } else {// success
                extend(parameter,body);
                deferred.resolve(parameter);
            }
        }
    });

    return deferred.promise;
};
exports.signForPicture = function(req) {
    var deferred = Q.defer();
    // header setting
    var userId = itemApiSettings.userid;//cookie.userId;//req.session.user.id;
    var userToken = itemApiSettings.usertoken;//cookie.userToken;//req.session.user.token;
    var options = {
        headers: {
            'X-FIVEMILES-USER-ID':userId,
            'X-FIVEMILES-USER-TOKEN':userToken,
            'User-Agent':req.headers['user-agent']
        }
    };
    var parameter = {
        "callback": req.baseUrl + "/cloudinary_cors.html"
    };
    //extend header
    options = tools.extendHeader(req,options);
    // post request
    needle.post(itemApiSettings.signuri, parameter, options,function(error, result) {
        if (error) {
            log.error(error);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                log.error(body);
                deferred.reject({"ERROR":body});
            } else {// success
                body.cloudinary_upload_url = "http://api.cloudinary.com/api/v1_1/fivemiles/upload";
                if (!body.callback) {
                    body.callback = "";
                }
                deferred.resolve(body);
            }
        }
    });

    return deferred.promise;
};


/**
 * 根据经纬度和req的定位返回距离
 * @param req
 * @param res
 * @returns promise
 */
exports.getTheDistance = function(req,res) {
    var deferred = Q.defer();
    var distance = -1;
    // post request
    var coordinate = tools.getTheReqLocation(req);
    var detail = {"latitude":req.body.latitude, "longitude":req.body.longitude};
    if (isValidCoordinate(coordinate) && isValidCoordinate(detail)) {
        deferred.resolve(geolib.getDistance(coordinate, detail));
    } else  {
        deferred.resolve(distance);
    }
    return deferred.promise;
}
/**
 * get the home view data
 * @param req
 * @param res
 * @returns promise
 */
exports.getHomeItems = function(req,res) {
    var deferred = Q.defer();
    // header setting
    var options = {};
    var ip = tools.getRemoteIp(req);
    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {
        var userToken = cookie.userToken;
        var userId = cookie.userId;
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':userId,
                'X-FIVEMILES-USER-TOKEN':userToken,
                'User-Agent':req.headers['user-agent'],
                'X-FIVEMILES-REAL-IP':ip
            }
        };
    } else {
        options = {
            headers: {
                'X-FIVEMILES-REAL-IP':ip,
                'User-Agent':req.headers['user-agent']
            }
        };
    }

    var offset = 0;
    var limit = 20;
    if (req.param("limit")) {
        limit = req.param("limit");
    }
    if(req.param("offset")){
        offset = req.param("offset");
    }
    var lat = 0;
    var lon = 0;
    var coordinate = tools.getTheReqLocation(req);
    if (isValidCoordinate(coordinate)) {
        lat = coordinate.latitude;
        lon = coordinate.longitude;
    }
    var normalUrl = "lat=" + lat + "&lon=" + lon;

    if (req.param("next")) {
        normalUrl += "&" + req.param("next").substring(1);
    } else {
        normalUrl += "&offset=" + offset + "&limit=" + limit;
    }
    //extend header
    options = tools.extendHeader(req,options);
    // post request
    var viewuri = itemApiSettings.viewuri + normalUrl;
    needle.get(viewuri, options,function(error, result) {
        if (error) {
            log.error("getHomeItems:error:",error);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                log.error("getHomeItems:",body);
                if (tools.isTokenNotUsed(req,res,result)) {
                    deferred.reject({"STATE":401,"ERROR":body});
                }
                deferred.reject({"ERROR":body});
            } else {// success
                // 捕获一个异常
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};

/**
 * get the home view data include the distance data
 * @param req
 * @param res
 * @returns promise
 */
exports.getHomeItemsDistance = function(req, res) {
    var deferred = Q.defer();
    exports.getHomeItems(req, res).then(function(data) {
        var coordinate = tools.getTheReqLocation(req);
        var objs = data.objects;
        var res = [];
        for (var i = 0; i < objs.length; i++) {
            var obj = objs[i];
            var detail = {"latitude":obj.location.lat, "longitude":obj.location.lon};
            if (isValidCoordinate(coordinate) && isValidCoordinate(detail)) {
                obj["distance"] = Tools.milesFilter(geolib.getDistance(coordinate, detail));
            }
            res.push(obj);
        }
        data["objects"] = res;
        data["loc"] = coordinate;
        deferred.resolve(data);
    },function(error) {
        log.error("getHomeItemsDistance:",error);
        deferred.reject(error);
    });
    return deferred.promise;
};

/**
 * get the keyword search
 * @param req
 * @param res
 * @returns promise
 */
exports.getSearchItems = function(req,res) {
    var deferred = Q.defer();
    // header setting
    var options = {};
    var ip = tools.getRemoteIp(req);
    var cookie = tools.getClientCookie(req);
    if (cookie.userId) {
        var userToken = cookie.userToken;
        var userId = cookie.userId;
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':userId,
                'X-FIVEMILES-USER-TOKEN':userToken,
                'User-Agent':req.headers['user-agent'],
                'X-FIVEMILES-REAL-IP':ip
            }
        };
    } else {
        options = {
            headers: {
                'X-FIVEMILES-USER-ID':itemApiSettings.userid,
                'X-FIVEMILES-USER-TOKEN':itemApiSettings.usertoken,
                'X-FIVEMILES-REAL-IP':ip,
                'User-Agent':req.headers['user-agent']
            }
        };
    }
    var offset = 0;
    var limit = 20;
    if (req.param("limit")) {
        limit = req.param("limit");
    }
    var lat = 0;
    var lon = 0;
    var coordinate = tools.getTheReqLocation(req);
    if (isValidCoordinate(coordinate)) {
        lat = coordinate.latitude;
        lon = coordinate.longitude;
    }
    var keyword = req.param("keyword");
    var category = req.param("category");
    var distance = req.param("distance");
    var orderby = req.param("orderby");
    var normalUrl = "lat=" + lat + "&lon=" + lon;

    if (req.param("next")) {
        normalUrl += "&" + req.param("next").substring(1);
    } else {
        normalUrl += "&offset=" + offset + "&limit=" + limit;
    }
    var viewuri = "";
    if (category && category.length > 0) {
        normalUrl +=  "&cat_id=" + category;
        viewuri = itemApiSettings.searchcategoryuri + normalUrl;
    } else if (keyword && keyword.length > 0) {
        normalUrl +=  "&q=" + keyword;
        viewuri = itemApiSettings.searchuri + normalUrl;
    } else {
        viewuri = itemApiSettings.searchcategoryuri + normalUrl + "&cat_id=-1";
    }
    if (distance && distance.length > 0) {
        viewuri += "&distance=";
        viewuri += distance;
    }
    if (orderby && orderby.length > 0) {
        viewuri += "&orderby=";
        viewuri += orderby;
    }
    //extend header
    options = tools.extendHeader(req,options);
    // post request
    needle.get(viewuri, options,function(error, result) {
        if (error) {
            log.error("getSearchItems:",error);
            deferred.reject({"ERROR":error});
        } else {
            var body = result.body;
            if (result.body && result.body.length) {//error
                log.error("getSearchItems:",body);
                if (tools.isTokenNotUsed(req,res,result)) {
                    deferred.reject({"STATE":401,"ERROR":body});
                }
                deferred.reject({"ERROR":body});
            } else {// success
                deferred.resolve(body);
            }
        }
    });
    return deferred.promise;
};
/**
 * search items with distance
 * @param req
 * @param res
 * @returns promise
 */
exports.getSearchItemsDistance = function(req, res) {
    var deferred = Q.defer();
    exports.getSearchItems(req, res).then(function(data) {
        var coordinate = tools.getTheReqLocation(req);
        var objs = data.objects;
        var res = [];
        for (var i = 0; i < objs.length; i++) {
            var obj = objs[i];
            var detail = {"latitude":obj.location.lat, "longitude":obj.location.lon};
            if (isValidCoordinate(coordinate) && isValidCoordinate(detail)) {
                obj["distance"] = Tools.milesFilter(geolib.getDistance(coordinate, detail));
            }
            res.push(obj);
        }
        data["objects"] = res;
        data["loc"] = coordinate;
        deferred.resolve(data);
    },function(error) {
        log.error("getSearchItemsDistance:",error);
        deferred.reject(error);
    });
    return deferred.promise;
};

