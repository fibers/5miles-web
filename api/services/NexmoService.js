var https = require('https');
var msgSettings = sails.config.settings.message;
var log = require('../services/Log.js');
var tools = require('../services/Tools.js');
var Q = require('q');
var needle = require('needle');

module.exports = {
    sendMessage: function (to, text) {
        //!important: change from Nexmo service to local services.
        var options = {};
        var parameter = {
            mobile_phone : to,
            text : text
        };
        var deferred = Q.defer();
        needle.post(msgSettings.path, parameter, options,function(error, result){
            if (error) {
                log.error(to + " sendMessage:send msg error:",error);
                deferred.reject({"ERROR":error});
            } else {
                var body = result.body;
                if (result.statusCode == 200 || result.statusCode == 201) {//success
                    deferred.resolve(body);
                } else {// fail
                    log.error("NexmoService:reject:",body);
                    deferred.reject({"ERROR":body});
                }
            }
        });

        return deferred.promise;

    }
}