/**
 * Created by liujinkai on 15-5-15.
 */
angular.module("app")
    .controller('accountCtrl',
        ['$rootScope', '$scope','$http',
            function($rootScope, $scope, $http) {
                $scope.submit = function() {
                    if ($scope.deactivateFlag == true) {
                        return false;
                    }
                    $scope.returnTip = "";
                    $scope.deactivateFlag = false;
                    var con = $("#ac_con").val();
                    con = con.trim();
                    if (con.length > 1024) {
                        alert("Please input less than 1000 words.");
                        return false;
                    }
                    var param = {"content":con};
                    $http.post("/account/deactivate", param).then(function(result) {
                        var data = result.data;
                        if (data.status == "200") {
                            $scope.deactivateFlag = true;
                            $scope.returnTip = "We have deactivated your account.";
                            $rootScope.$broadcast("LOGIN:user:deactivate",{});
                        } else {
                            $scope.returnTip = data.msg;
                        }
                    });
                    ga('send', 'event', 'Deleteaccount', 'deactivate');
                }
                $scope.cancel = function() {
                    ga('send', 'event', 'Deleteaccount', 'cancel');
                    window.location.href = "/";
                }
            }]);

