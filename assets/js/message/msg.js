/**
 * the public file to define banner、filter、service..
 */
angular.module("app")
    .controller('MessageController',
        ['$rootScope', '$scope','$http','parseURL','$timeout',
            function($rootScope, $scope, $http, parseURL, $timeout) {
                $scope.$on("LOGIN:user:msg",function(event,transData) {
                    $scope.msgTotal = transData.total;
                    if($scope.msgTotal != 1){
                        $scope.msgMulti = "s";
                    }
                    $scope.msgData = transData.data;
                });

            }]);