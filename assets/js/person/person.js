angular.module("app")
    .controller('personCtrl',
        ['$rootScope', '$scope','$http','cookie','$q','parseTitle','categoryData',
            function($rootScope, $scope, $http,cookie,$q,parseTitle,categoryData) {
                var cateData = categoryData;
                $scope.myItems = [];
                $scope.per_ver_pop_flag = false;
                $scope._checkLoginStatus = function() {
                    if ($.cookie) {
                        var appStr = $.cookie("5miles_head");
                        if (appStr) {
                            appStr = JSON.parse(appStr);
                            $scope.login_user_id = appStr.userId;
                            $scope.login_user_token = appStr.userToken;
                            if ($scope.login_user_id && $scope.login_user_token) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
                $scope.verPop = function(event) {
                    $scope.per_ver_pop_flag = !$scope.per_ver_pop_flag;
                    $scope.itsverifyinfo();
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $(".personPage").bind("click", function(event) {
                    $scope.$apply(function() {
                        $scope.per_ver_pop_flag = false;
                    });
                });
                $scope._getTheDistance = function(obj) {
                    if (obj && obj.lat && obj.lon) {
                        var lat = obj.lat;
                        var lon = obj.lon;
                        var param = {"latitude":parseInt(lat),"longitude":parseInt(lon)};
                        if (cookie.get("5__location","lat")) {// have cookie location
                            param['clientLat'] = parseInt(cookie.get("5__location","lat").toFixed(3));
                            param['clientLon'] = parseInt(cookie.get("5__location","lng").toFixed(3));
                        }
                        $http.post("/@person/distance", param).then(function(res) {
                            if (res.data) {
                                var dis = res.data.distance;
                                if (dis == -1) {
                                    $scope.per_miles = "";
                                } else {
                                    $scope.per_miles = dis;
                                }
                                var city = $("#per_city").attr("data-city");
                                if (($scope.per_miles.length == 0) && (city.length == 0)) {
                                    $scope.per_map_tip = true;
                                }
                            } else {
                                alert("error");
                            }
                        });
                    }
                };
                $scope._getPersonId = function() {
                    var path = window.location.href;
                    var arr = [];
                    if (path.indexOf("profile/") > 0) {
                        arr = path.split("profile/");
                    } else {
                        arr = path.split("person/");
                    }
                    var temp = arr[1];
                    return temp.split("/")[0];
                }
                $scope._checkCurrentUser = function() {
                    if ($scope._checkLoginStatus()) {
                        var id = $scope._getPersonId();
                        if ($scope.login_user_id == id) {
                            $scope.followingBtnHideFlag = true;
                        }
                    }
                }
                $scope._checkCurrentUser();
                //get the screen size
                var screenSize = jQuery(window).width();
                var containerSize = jQuery(".per_items").width();
                var wid = 256;
                if ((310 < screenSize) && (screenSize < 768)) {
                    wid = Math.floor((containerSize)/2 - 7);
                } else if (769 < screenSize) {
                    wid = Math.floor(containerSize/3) - 12;
                }
                function _filter(data) {
                    var res = [];
                    angular.forEach(data, function(item,key) {
                        var image = item.images[0];
                        var imageWidth = image.width;
                        var imageHeight = image.height;
                        var hei = Math.floor(imageHeight*(wid/imageWidth));
                        var preTitle = cateData[item.cat_id] ? cateData[item.cat_id].title : "";
                        var idParse = parseTitle.getParse(preTitle + " " + item.title);
                        var temp = {
                            "id":item.id,
                            "title":item.title,
                            "idParse":idParse,
                            "local_price":item.local_price,
                            "original_price":item.original_price || "",
                            "state":item.state,
                            "owner":{
                                "portrait":item.owner.portrait,
                                "nickname":item.owner.nickname
                            },
                            "images":{
                                "imageLink":image.imageLink,
                                "width":wid,
                                "height":hei
                            },
                            "hide":true
                        };
                        res.push(temp);
                    });
                    return res;
                }
                function _loaded(id) {// after loaded to initalize the waterfall
                    var cId = "#container";
                    if (id) {
                        cId = "#" + id;
                    }
                    var container = document.querySelector(cId);
                    var msnry;
                    // initialize Masonry after all images have loaded
                    var fixed = 6;
                    imagesLoaded( container, function() {
                        msnry = new Masonry( container ,{
                            // options
                            columnWidth: wid + fixed,
                            "isFitWidth": true,
                            itemSelector: '.waterItem'
                        });
                        $(".waterItemInvisible").removeClass("waterItemInvisible");
                        $("#waterCon").css("height","auto");
                        $scope.$apply(function() {
                            $scope.upload_list_more_load_flag = false;
                            $scope.personPage_load_flag = false;
                        });
                    });
                }
                $scope.per_item_more_flag = false;
                $scope.getItems = function(event,moreFlag,profileFlag) {
                        var param = {"limit":9};
                        if (moreFlag) {
                            param["next"] = $scope.myMeta.next;
                        } else {
                            $scope.personPage_load_flag = true;
                        }
                        param["user_id"] = $scope._getPersonId();
                        var url = "/sellerItems";

                        $http.post(url, param).then(function(res) {
                            if (res.data) {
                                if (moreFlag) {
                                    var fData = _filter(res.data.objects);
                                    $scope.myItems = $scope.myItems.concat(fData);
                                } else  {
                                    $scope.myItems = _filter(res.data.objects);
                                }
                                $scope.myMeta = res.data.meta;
                                // show or hide the more
                                $scope.upload_list_more_load_flag = false;
                                if ($scope.myMeta.next == null) {
                                    $scope.per_item_more_flag = false;
                                } else {
                                    $scope.per_item_more_flag = true;
                                }
                                if (!moreFlag) {
                                    var obj = $(".per_map_tip").attr("data-loc");
                                    if (obj) {
                                        var cor = {"lat":obj.split(",")[0],"lon":obj.split(",")[1]};
                                        $scope._getTheDistance(cor);
                                    }
                                }
                                _loaded();
                            } else {
                                alert("error");
                            }
                        });
                }
                $scope.items = function() {
                    $scope.activePage = "items";
                    if (!$scope.myMeta) {
                        $scope.getItems();
                    }
                    $scope.itsproducttab();
                }
                $scope.likeCount = function(event,moreFlag) {
                    if (!$scope.personLikes || moreFlag) {
                        var param = {"limit":9};
                        if (moreFlag) {
                            param["next"] = $scope.likesMeta.next;
                        } else {
                            $scope.personPage_load_flag = true;
                        }
                        $http.post("/@person/userLikes", param).then(function(res) {
                            if (res.data) {
                                if (moreFlag) {
                                    var fData = _filter(res.data.objects);
                                    $scope.personLikes = $scope.personLikes.concat(fData);
                                } else  {
                                    $scope.personLikes = _filter(res.data.objects);
                                }
                                $scope.likesMeta = res.data.meta;
                                $scope.upload_like_more_load_flag = false;
                                if ($scope.likesMeta.next == null) {
                                    $scope.per_like_more_flag = false;
                                } else {
                                    $scope.per_like_more_flag = true;
                                }
                                $scope.personPage_load_flag = false;
                                _loaded("container_likes");
                            } else {
                                alert("error");
                            }
                        });
                    }
                    $scope.activePage = "likes";
                    $scope.mylikes();
                }
                $scope.buyCount = function(event,moreFlag) {
                    if (!$scope.personBuy || moreFlag) {
                        var param = {"limit":9};
                        if (moreFlag) {
                            param["next"] = $scope.buyMeta.next;
                        } else {
                            $scope.personPage_load_flag = true;
                        }
                        $http.post("/@person/myPurchases", param).then(function(res) {
                            if (res.data) {
                                if (moreFlag) {
                                    var fData = _filter(res.data.objects);
                                    $scope.personBuy = $scope.personBuy.concat(fData);
                                } else  {
                                    $scope.personBuy = _filter(res.data.objects);
                                }
                                $scope.buyMeta = res.data.meta;
                                $scope.upload_buy_more_load_flag = false;
                                if ($scope.buyMeta.next == null) {
                                    $scope.per_buy_more_flag = false;
                                } else {
                                    $scope.per_buy_more_flag = true;
                                }
                                $scope.personPage_load_flag = false;
                                _loaded("container_buy");
                            } else {
                                alert("error");
                            }
                        });
                    }
                    $scope.activePage = "buy";
                    $scope.mybuying();
                }
                $scope.following = function(event,moreFlag) {
                    if (!$scope.personFollowing || moreFlag) {
                        // show the following page
                        var param = {"limit":12};
                        if (moreFlag) {
                            param["next"] = $scope.followingMeta.next;
                        } else {
                            $scope.personPage_load_flag = true;
                        }
                        param["user_id"] = $scope._getPersonId();
                        $http.post("/@person/following", param).then(function(res) {
                            if (res.data) {
                                if (moreFlag) {
                                    $scope.personFollowing = $scope.personFollowing.concat(res.data.objects);
                                } else  {
                                    $scope.personFollowing = res.data.objects || [];
                                }
                                $scope.followingMeta = res.data.meta;
                                $scope.per_following_loading_flag = false;
                                if ($scope.followingMeta.next == null) {
                                    $scope.per_following_more_flag = false;
                                } else {
                                    $scope.per_following_more_flag = true;
                                }
                                $scope.personPage_load_flag = false;
                            } else {
                                alert("error");
                            }
                        });
                    }
                    $scope.activePage = "following";
                    $scope.itsfollowingtab();
                }
                $scope.per_follower_more_flag = false;
                $scope.follower = function(event,moreFlag,directDo) {
                    if (!$scope.personFollower || moreFlag || directDo) {
                        // show the following page
                        var param = {"limit":12};
                        if (moreFlag) {
                            param["next"] = $scope.followerMeta.next;
                        } else if (directDo) {
                        } else {
                            $scope.personPage_load_flag = true;
                        }
                        param["user_id"] = $scope._getPersonId();
                        $http.post("/@person/follower", param).then(function(res) {
                            if (res.data) {
                                if (moreFlag) {
                                    $scope.personFollower = $scope.personFollower.concat(res.data.objects);
                                } else  {
                                    $scope.personFollower = res.data.objects || [];
                                }
                                $scope.followerMeta = res.data.meta;
                                $scope.per_follower_loading_flag = false;
                                if ($scope.followerMeta.next == null) {
                                    $scope.per_follower_more_flag = false;
                                } else {
                                    $scope.per_follower_more_flag = true;
                                }
                                $scope.personPage_load_flag = false;
                            } else {
                                alert("error");
                            }
                        });
                    }
                    if (!directDo) {
                        $scope.activePage = "follower";
                        if (moreFlag) {
                        } else {
                            $scope.itsfollowertab();
                        }
                    }
                }
                if ($(".per_fl_btn").attr("data-followed") == "true") {
                    $scope.followActive = false;
                    $scope.followValue = "Following";
                } else {
                    $scope.followActive = true;
                    $scope.followValue = "Follow";
                }

                $scope.triggerFollow = function() {
                    // no login to login page
                    if ($scope._checkLoginStatus()) {
                        if ($scope.followActive) {
                            $scope.follow();
                            $scope.sellerpagefollow();
                        } else {
                            $scope.unfollow();
                            $scope.sellerpageunfollow();
                        }
                    } else {
                        window.location.href = "/loginShow/?back=" + window.location.pathname;
                    }
                }

                $scope.follow = function() {
                    // show the following page
                    var param = {};
                    param["user_id"] = $scope._getPersonId();
                    $scope.per_fl_load = true;
                    $http.post("/@person/follow", param).then(function(res) {
                        if (res.status == "200") {
                            $scope.followActive = false;
                            $scope.followValue = "Following";
                            $scope.per_fl_load = false;
                            $scope.follower(null,null,true);
                            $("#followers-count").text(parseInt($("#followers-count").text()) + 1);
                        } else {
                            alert("error");
                        }
                    });
                }
                $scope.unfollow = function() {
                    // show the following page
                    var param = {};
                    param["user_id"] = $scope._getPersonId();
                    $scope.per_fl_load = true;
                    $http.post("/@person/unfollow", param).then(function(res) {
                        if (res.status == "200") {
                            $scope.followActive = true;
                            $scope.followValue = "Follow";
                            $scope.per_fl_load = false;
                            $scope.follower(null,null,true);
                            $("#followers-count").text(parseInt($("#followers-count").text()) - 1);
                        } else {
                            alert("error");
                        }
                    });
                }
                $scope.mapClick = function() {
                    $scope.itsmap();
                    $('#item_msg_modal').modal('show');
                }
                $('#item_msg_modal').modal({
                    keyboard: false,
                    backdrop:'static',
                    show:false
                });

                // list more
                $scope.listMore = function(event) {
                    $scope.upload_list_more_load_flag = true;
                    $scope.getItems(event,true);
                    $scope.itsmoreitems();//ga
                }
                // like more
                $scope.likeMore = function(event) {
                    $scope.upload_like_more_load_flag = true;
                    $scope.likeCount(event,true);
                }
                // buy more
                $scope.buyMore = function(event) {
                    $scope.upload_buy_more_load_flag = true;
                    $scope.buyCount(event,true);
                }
                // following more
                $scope.followingMore = function(event) {
                    $scope.per_following_loading_flag = true;
                    $scope.following(event,true);
                    $scope.itsmorefollowing();//ga
                }
                // follower more
                $scope.followerMore = function(event) {
                    $scope.per_follower_loading_flag = true;
                    $scope.follower(event,true);
                    $scope.itsmorefollowers();//ga
                }
                // download
                $scope.goDown = function(event,state) {
                    window.location.href = "/download";
                    if (state == "likes_inapp") {
                        $scope.mylikes_exploringinapp();
                    } else if (state == "buy_inapp") {
                        $scope.mybuying_shoppinginapp();
                    } else if (state == "following_inapp") {
                        $scope.myfollowing_exploring();
                    } else if (state == "follower_inapp") {
                        $scope.myfollowers_list();
                    } else if (state == "items_inapp") {
                        $scope.myitem_list();
                    }
                }
                $scope.goHome = function(event,state) {
                    window.location.href = "/";
                    if (state == "likes_exp") {
                        $scope.mylikes_exploringinapp();
                    } else if (state == "buy_exp") {
                        $scope.mybuying_exploring();
                    } else if (state == "following_exp") {
                        $scope.myfollowing_exploring();
                    }
                }
                // view in app
                $scope.viewMore = function() {
                    window.location.href = "/download";
                    $scope.viewmoreinapp();
                }
                //
                $scope.appleStore = function(event) {
                    var obj = event.currentTarget;
                    var items = $(obj).parents(".per_items");
                    var followers = $(obj).parents(".per_follower");
                    var manageInApp = $(obj).parents(".manageInApp");
                    if (items.length == 1) {//items
                        ga('send', 'event', 'My profile', 'myitem_appstore');
                    } else if (followers.length == 1) {//followers
                        ga('send', 'event', 'My profile', 'myfollowers_appstore');
                    } else if (manageInApp.length == 1) {
                        ga('send', 'event', 'My profile', 'manageprofileinapp_ios');
                    }
                }
                $scope.googleStore = function(event) {
                    var obj = event.currentTarget;
                    var items = $(obj).parents(".per_items");
                    var followers = $(obj).parents(".per_follower");
                    var manageInApp = $(obj).parents(".manageInApp");
                    if (items.length == 1) {//items
                        ga('send', 'event', 'My profile', 'myitem_googleplay');
                    } else if (followers.length == 1) {//followers
                        ga('send', 'event', 'My profile', 'myfollowers_googleplay');
                    } else if (manageInApp.length == 1) {
                        ga('send', 'event', 'My profile', 'manageprofileinapp_android');
                    }

                }
                $scope.sendLink = function() {
                    var obj = event.currentTarget;
                    var items = $(obj).parents(".per_items");
                    var followers = $(obj).parents(".per_follower");
                    var manageInApp = $(obj).parents(".manageInApp");
                    if (items.length == 1) {//items
                        ga('send', 'event', 'My profile', 'myitem_smssend');
                    } else if (followers.length == 1) {//followers
                        ga('send', 'event', 'My profile', 'myfollowers_smssend');
                    } else if (manageInApp.length == 1) {
                        ga('send', 'event', 'My profile', 'manageprofileinapp_sms');
                    }
                }
                // profile GA
                $scope.viewmoreinapp = function() {
                    ga('send', 'event', 'My profile', 'viewmoreinapp');
                };
                $scope.getverified = function() {//
                    ga('send', 'event', 'My profile', 'getverified');
                };
                $scope.myitem_list = function() {
                    ga('send', 'event', 'My profile', 'myitem_list');
                };
                $scope.mylikes = function() {
                    ga('send', 'event', 'My profile', 'mylikes');
                };
                $scope.mylikes_product = function() {
                    ga('send', 'event', 'My profile', 'mylikes_product');
                };
                $scope.mylikes_exploringinapp = function() {
                    ga('send', 'event', 'My profile', 'mylikes_exploringinapp');
                };
                $scope.mybuying = function() {
                    ga('send', 'event', 'My profile', 'mybuying');
                };
                $scope.mybuying_product = function() {
                    ga('send', 'event', 'My profile', 'mybuying_product');
                };
                $scope.mybuying_exploring = function() {
                    ga('send', 'event', 'My profile', 'mybuying_exploring');
                };
                $scope.mybuying_shoppinginapp = function() {
                    ga('send', 'event', 'My profile', 'mybuying_shoppinginapp');
                };
                $scope.myfollowing_exploring = function() {
                    ga('send', 'event', 'My profile', 'myfollowing_exploring');
                };
                $scope.myfollowers_list = function() {
                    ga('send', 'event', 'My profile', 'myfollowers_list');
                };

                $scope._profileInApp = function() {
                    ga('send', 'event', 'My profile', 'manageprofileinapp');
                }
                $scope._profileInApp_ios = function() {
                    ga('send', 'event', 'My profile', 'manageprofileinapp_ios');
                }
                $scope._profileInApp_android = function() {
                    ga('send', 'event', 'My profile', 'manageprofileinapp_android');
                }
                $scope._profileInApp_sms = function() {
                    ga('send', 'event', 'My profile', 'manageprofileinapp_sms');
                }
                // seller GA
                $scope.itsproduct = function() {
                    if ($scope.profile) {
                        ga('send', 'event', 'My profile', 'myitem_product');
                    } else {
                        ga('send', 'event', 'sellerprofile_view', 'itsproduct');
                    }
                };
                $scope.itsmap = function() {
                    ga('send', 'event', 'sellerprofile_view', 'itsmap');
                };
                $scope.itsproducttab = function() {
                    if ($scope.profile) {
                        ga('send', 'event', 'My profile', 'myitem');
                    } else {
                        ga('send', 'event', 'sellerprofile_view', 'itsproducttab');
                    }
                };
                $scope.itsfollowingtab = function() {
                    if ($scope.profile) {
                        ga('send', 'event', 'My profile', 'myfollowing');
                    } else {
                        ga('send', 'event', 'sellerprofile_view', 'itsfollowingtab');
                    }
                };
                $scope.itsfollowingpeople = function() {
                    if ($scope.profile) {
                        ga('send', 'event', 'My profile', 'myfollowing_people');
                    } else {
                        ga('send', 'event', 'sellerprofile_view', 'itsfollowingpeople');
                    }
                };
                $scope.itsfollowertab = function() {
                    if ($scope.profile) {
                        ga('send', 'event', 'My profile', 'myfollowers');
                    } else {
                        ga('send', 'event', 'sellerprofile_view', 'itsfollowertab');
                    }
                };
                $scope.itsfollowerpeople = function() {
                    if ($scope.profile) {
                        ga('send', 'event', 'My profile', 'myfollowers_people');
                    } else {
                        ga('send', 'event', 'sellerprofile_view', 'itsfollowerpeople');
                    }
                };

                $scope.sellerpagefollow = function() {
                    ga('send', 'event', 'sellerprofile_view', 'sellerpagefollow');
                };
                $scope.itsverifyinfo = function() {
                    if ($scope.profile) {
                        ga('send', 'event', 'My profile', 'myverifyinfo');
                    } else {
                        ga('send', 'event', 'sellerprofile_view', 'itsverifyinfo');
                    }
                };
                $scope.sellerpageunfollow = function() {
                    ga('send', 'event', 'sellerprofile_view', 'sellerpageunfollow');
                };

                $scope.itsmoreitems = function() {
                    if ($scope.profile) {
                        ga('send', 'event', 'My profile', 'myitem_moreproduct');
                    } else {
                        ga('send', 'event', 'sellerprofile_view', 'itsmoreitems');
                    }
                };
                $scope.itsmorefollowing = function() {
                    ga('send', 'event', 'sellerprofile_view', 'itsmorefollowing');
                };
                $scope.itsmorefollowers = function() {
                    ga('send', 'event', 'sellerprofile_view', 'itsmorefollowers');
                };


                // init
                $scope.init = function() {
                    // get hash
                    var loc = window.location.href;
                    if (loc.indexOf("#following") > 0) {
                        $scope.activePage = "following";
                        $scope.following();
                    } else if (loc.indexOf("#followers") > 0) {
                        $scope.activePage = "follower";
                        $scope.follower();
                    } else {
                        $scope.activePage = "items";
                        $scope.getItems();// init
                    }
                    if ($(".per_fl").length == 0) {//
                        $scope.profile = true;
                    }
                }
                $scope.init();

                //send email and phone
                $('.btn-send').on('click', function(e) {
                    function filterCountryCode(str) {
                        var arr2 = str.split("(+");
                        if (arr2[1]) {
                            var arr = arr2[1].split(")");
                            return ("+"+arr[0]);
                        }
                        return null;
                    }
                    var obj = $(this).parents('.form-group');
                    var tip = obj.find('.send-tip');
                    var data, dest, input, type;
                    input = obj.find("#phone");
                    dest = input.val();
                    type = "phone";
                    data = {
                        dest: dest,
                        type: type
                    };
                    if (dest !== '' && type !== '') {
                        //before process
                        if (type == "phone") {
                            tip.removeClass("send-tip-ok").html("");
                            //validate
                            var countryCodeStr = $("#sms_link_country option:selected").html();
                            var countryCode = filterCountryCode(countryCodeStr);
                            if (!countryCode) {
                                tip.html("Please select country code !");
                                return false;
                            }
                            tip.html("");
                            //prevent too much times click
                            if (tip.data("phoneSendFlag") == "true") {
                                tip.html("Please try again after 1 minute !");
                                return true;
                            } else {
                                tip.html("");
                                tip.data("phoneSendFlag","true");
                                setTimeout(function() {
                                    tip.data("phoneSendFlag","false");
                                    tip.html("");
                                }, 60000);
                            }
                            data.dest = countryCode + data.dest;
                        }
                        //phone || email ,send message and params
                        $.post('/message', data, function(result) {
                            if (result.success) {
                                input.val('');
                                tip.addClass("send-tip-ok").html("Link sent successfully!");
                            } else {
                                alert(result.data)
                            }
                        });
                    } else if (dest === '') {
                        if (type === "phone") {
                            return tip.html("Please input your phone number!");
                        }
                    }
                });
            }]
    );

