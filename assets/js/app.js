/**
 * the public file to define banner、filter、service..
 */
angular.module("app",[])
    .controller('BannerController',
        ['$rootScope', '$scope','$http','parseURL','$timeout','cookie',
            function($rootScope, $scope, $http, parseURL, $timeout, cookie) {
                $scope.login_user_name = "";
                $scope.login_user_img = "";
                $scope.showLoginFlag = false;
                $scope.showLoginFlag_mb_list = false;
                $scope.showLoginInfo = function(event) {
                    $scope.showLoginFlag = !$scope.showLoginFlag;
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.toSignPage = function(event) {
                    $scope.topBarJoin();
                    window.location.href = "/loginShow/?back=" + window.location.pathname + "&param=sign";
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.showLoginInfo_mb_list = function(event) {
                    $scope.showLoginFlag_mb_list = !$scope.showLoginFlag_mb_list;
                    $scope.topBarJoin();
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }

                $("body").bind("click", function(event) {
                    $scope.$apply(function() {
                        $scope.showLoginFlag = false;
                        $scope.showLoginFlag_mb_list = false;
                    });
                });
                $scope.initUser = function() {
                    if ($.cookie) {
                        var appStr = $.cookie("5miles_head");
                        if (appStr) {
                            appStr = JSON.parse(appStr);
                            if (appStr.nick) {
                                $scope.login_user_name = appStr.nick;
                                if ($scope.login_user_name.length > 8) {
                                    $scope.login_user_name_show = $scope.login_user_name.substring(0,8) + "..";
                                } else {
                                    $scope.login_user_name_show = $scope.login_user_name;
                                }
                                if (appStr.fb_user_id && appStr.fb_user_id.length > 0) {
                                    $scope.login_user_img = "https://res.cloudinary.com/fivemiles/image/facebook/w_100/" + appStr.fb_user_id
                                        + ".jpg" || "/images/common/person-head_def.png";
                                } else {
                                    $scope.login_user_img = (appStr.portrait && appStr.portrait.replace(/http:/, "https:")) || "/images/common/person-head_def.png";
                                }
                                $scope.login_user_id = appStr.userId;
                                $scope.login_user_token = appStr.userToken;
                                checkMsgInfo();
                            }
                        }
                        // init dataList
                        var dataList = $.cookie("5_dataList");
                        if (dataList) {
                            dataList = JSON.parse(dataList);
                            var str = "";
                            if (dataList) {
                                for (var i = 0; i < dataList.length;i++) {
                                    var item = dataList[i];
                                    str += "<option value=" + item + ">";
                                    str += item;
                                    str += "</option>";
                                }
                                $(".pasta").prepend($(str));
                            }
                        }
                    }
                }

                function checkMsgInfo(){
                    $scope.msgTotalShow = false;
                    $http.get("/new_messages").then(function(data){
                        if(data.data == undefined)return;
                        $scope.msgTotal =0;
                        $scope.msgData = [];
                        for(var i in data.data){
                            $scope.msgTotal += data.data[i];
                            var name = i;
                            // the messages have order.
                            var index = 0;
                            if(i == "offerers"){
                                if(data.data[i]==1){
                                    name = "person has made an offer";
                                }else if(data.data[i]==0){
                                    name = "people have made an offer";
                                }else{
                                    name = "people have made offers";    
                                }
                                
                            }else if(i == "notifications"){
                                name = "new notification"+(data.data[i]==1?"":"s");
                                index = $scope.msgData.length;
                            }
                            $scope.msgData.splice(index,0,{"name":name,"value":data.data[i]});
                        }
                        if($scope.msgTotal != 0){
                            $scope.msgTotalShow = true;
                        }
                        $rootScope.$broadcast("LOGIN:user:msg",{"total":$scope.msgTotal,"data":$scope.msgData});
                    });
                }
                $scope.initUser();
                $scope.$on("LOGIN:user:reload",function(event,data) {
                    $scope.initUser();
                });
                $scope.$on("LOGIN:user:deactivate",function(event,data) {
                    $scope.login_user_id = null;
                    $scope.login_user_token = null;
                    $scope.login_user_name = "";
                });
                $scope.logout = function(event) {
                    // clear cookie
                    $.removeCookie("5miles_head",{"path":"/"});
                    var logoutToHome = ["/profile/","/item/uploadView","/#items","#search_items"];
                    var locUrl = window.location.href;
                    for (var i = 0; i < logoutToHome.length;i++) {
                        var item = logoutToHome[i];
                        if (locUrl.indexOf(item) > 0) {
                            window.location.href = "/";
                            return false;
                        }
                    }
                    window.location.href = window.location.href;
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.userLogin = function(event) {
                    $scope.topSignIn();
                    //now is login page
                    var path = window.location.pathname;
                    if (path == "/loginShow/") {
                        window.location.href = "/loginShow/?back=/";
                    } else {
                        window.location.href = "/loginShow/?back=" + window.location.pathname;
                    }
                }
                $scope.userSign = function(event) {
                    $scope.topBarRegister();
                    //now is login page
                    var path = window.location.pathname;
                    if (path == "/loginShow/") {
                        window.location.href = "/loginShow/?back=/&param=sign";
                    } else {
                        window.location.href = "/loginShow/?back=" + window.location.pathname + "&param=sign";
                    }
                }

                $scope.searchKeyword = "";

                var keyword = parseURL.getParam(window.location,"keyword");
                var category = parseURL.getParam(window.location,"category");
                if (!category && keyword && keyword.length > 0) {
                    $scope.searchKeyword = keyword;
                }
                $scope.searchSubmit = function(event) {
                    var key = $(event.currentTarget).parent().find("input").val();
                    if (key && key.length > 0) {
                        //add the key world to cookie
                        var dataExist = $(".pasta option[value='" + key + "']");
                        if (dataExist.length == 0) {
                            var words = $.cookie("5_dataList");
                            if (words) {
                                words = JSON.parse(words);
                                var flag = true;
                                for (var i = 0; i < words.length;i++) {
                                    var item = words[i];
                                    if (item == key) {
                                        flag = false;
                                    }
                                }
                                if (flag) {
                                    words.unshift(key);
                                    if (words.length > 6) {
                                        words.pop();
                                    }
                                }
                            } else {
                                words = [key];
                            }
                            $.cookie("5_dataList",JSON.stringify(words),{"path":"/","expires":730});
                        }
                        window.location = "/search/?keyword=" + encodeURIComponent(key);
                    }
                    $scope.topSearch();
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                };
                // keypress
                $scope.doKeyPress = function(event) {
                    if (event.keyCode == 13) {
                        $scope.searchSubmit(event);
                    }
                };
                $scope.sell = function(event) {
                    $scope.topSellNow();
                    window.location.href = "/sell";
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.barSellNow = function(event) {
                    $scope.bar_sellNow();
                    window.location.href = "/sell";
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.myShop = function(event) {
                    $scope.bar_myshop();
                    window.location.href = "/person/" + $scope.login_user_id;
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.sell_mb = function(event) {
                    $scope.topSellNowMb();
                    window.location.href = "/item/uploadView";
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.blog = function() {
//                    window.location.href = "http://blog.5milesapp.com/";
                }
                // feedback
                $scope.feedbackShow = function(event) {
                    $("#feedback_body").val("");
                    $('#banner_feedback_modal').modal('show');
                    $scope.showLoginFlag_mb_list = false;
                    $scope.topBarFeedback();
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.feedbackHide = function(event) {
                    $('#banner_feedback_modal').modal('hide');
                    if (event) {
                        event.stopPropagation();
                        event.preventDefault();
                    }
                    return false;
                }
                $scope.feedbackSubmit = function(event) {
                    var text = $("#feedback_body").val();
                    if (text.length == 0) {
                        return false;
                    }
                    var param = {
                        "text" : text,
                        "id" : $scope.login_user_id,
                        "token" : $scope.login_user_token
                    }
                    $scope.banner_submit_load_flag = true;
                    $scope.topBarFeedbackSubmit();
                    $.post('/@feedback', param, function(res) {
                        if (res && res.status == 200) {
                            $scope.feedbackHide();
                            $('#banner_msg_modal').modal('show');
                            $scope.$apply(function() {
                                $scope.modal_banner_body = "We have received your feedback, thank you very much.";
                            });
                            $timeout(function() {
                                $('#banner_msg_modal').modal('hide');
                            },1500);
                        } else if (res.status == 400) {
                            $scope.$apply(function() {
                                $scope.modal_banner_body = res.msg;
                            });
                        } else {
                            $scope.$apply(function() {
                                $scope.modal_banner_body = "send fail!";
                            });
                        }
                        $scope.banner_submit_load_flag = false;
                    });
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                };
                $scope.myProfile = function(event,userId) {
                    window.location.href = "/profile/" + userId;
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.topSearch = function(event) {
                    ga('send', 'event', 'home_view', 'search');
                }
                $scope.topSellNow = function(event) {
                    ga('send', 'event', 'home_view', 'sell_now');
                }
                $scope.bar_sellNow = function(event) {
                    ga('send', 'event', 'home_view', 'bar_sellnow');
                }
                $scope.topSellNowMb = function(event) {
                    ga('send', 'event', 'home_view', 'sell_now_mb');
                }
                $scope.topBarRegister = function(event) {
                    ga('send', 'event', 'home_view', 'bar_register');
                }
                $scope.topSignIn = function(event) {
                    ga('send', 'event', 'home_view', 'bar_signin');
                }
                $scope.topBarJoin = function(event) {
                    ga('send', 'event', 'home_view', 'bar_join');
                }
                $scope.topBarDownload = function(event) {
                    ga('send', 'event', 'home_view', 'bar_download');
                }
                $scope.topBarFeedback = function(event) {
                    ga('send', 'event', 'home_view', 'bar_feedback');
                }
                $scope.topBarFeedbackSubmit = function() {
                    ga('send', 'event', 'home_view', 'bar_feedbackSubmit');
                }
                $scope.category = function(event) {
                    if (event.target && event.target.className == "banner_category_item") {
                        ga('send', 'event', 'home_view', 'category');
                    }
                }
                $scope.categoryMobileIcon = function() {
                    ga('send', 'event', 'home_view', 'categorymobileicon');
                }
                $scope.bar_myshop = function() {
                    ga('send', 'event', 'home_view', 'bar_myshop');
                }
                $scope.message = function() {
                    ga('send', 'event', 'home_view', 'message');
                }
                $scope.logoClick = function(){
                    ga('send', 'event', 'home_view', 'bar_logo');
                }
                $scope.msgIos = function(){
                    ga('send', 'event', 'message_view', 'message_ios');
                }
                $scope.msgAndroid = function(){
                    ga('send', 'event', 'message_view', 'message_android');
                }
                if($.smartbanner != undefined){
                    $.smartbanner();
                }
    }])
    .controller('FooterController',
        ['$rootScope', '$scope','$http','parseURL','$timeout',
            function($rootScope, $scope, $http, parseURL, $timeout) {
                $scope.facebooklike = function(event) {
                    ga('send', 'event', 'home_view', 'facebooklike');
                }
                $scope.twitterfollow = function(event) {
                    ga('send', 'event', 'home_view', 'twitterfollow');
                }
                $scope.instagramfollow = function(event) {
                    ga('send', 'event', 'home_view', 'instagramfollow');
                }
                $scope.googleplusfollow = function(event) {
                    ga('send', 'event', 'home_view', 'googleplusfollow');
                }
                $scope.footerhome = function(event) {
                    ga('send', 'event', 'home_view', 'footer_home');
                }
                $scope.footerabout = function(event) {
                    ga('send', 'event', 'home_view', 'footer_about');
                }
                $scope.footerblog = function(event) {
                    ga('send', 'event', 'home_view', 'footer_blog');
                }
                $scope.footerprivacy = function(event) {
                    ga('send', 'event', 'home_view', 'footer_privacy');
                }
                $scope.footerterms = function(event) {
                    ga('send', 'event', 'home_view', 'footer_terms');
                }
                $scope.footerhelpcenter = function(event) {
                    ga('send', 'event', 'home_view', 'footer_helpcenter');
                }
                $scope.footerrecommend = function(event) {
                    ga('send', 'event', 'home_view', 'footer_recommend');
                }
                $scope.footersitemap = function(event) {
                    ga('send', 'event', 'home_view', 'footer_sitemap');
                }
                $scope.footersupport = function(event) {
                    ga('send', 'event', 'home_view', 'footer_support');
                }
            }])
    .controller('CategoryController',
        ['$rootScope', '$scope','$http','parseURL','$timeout',
            function($rootScope, $scope, $http, parseURL, $timeout) {
                $scope.categoryShowFlag = false;
                $scope.categoryTrigger = function(event) {
                    $scope.categoryShowFlag = !$scope.categoryShowFlag;
                    $scope.categoryMore();
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $("body").bind("click", function(event) {
                    $scope.$apply(function() {
                        $scope.categoryShowFlag = false;
                    });
                });
                $scope.categoryMore = function() {
                    ga('send', 'event', 'home_view', 'category_more');
                }
            }])
    .filter("thumbScale",function() {
        return function(str,size,face) {
            if (str) {
                if (!size) {
                    size = 400;
                }
                if (face) {
                    str = str.replace(/upload/, 'upload/w_' + size + ',f_auto,h_' + size + ',c_thumb,g_face');
                    str = str.replace(/\/facebook/, '/facebook/w_' + size);
                } else {
                    str = str.replace(/upload/, 'upload/w_' + size + ",f_auto");
                    str = str.replace(/\/facebook/, '/facebook/w_' + size);
                }
                str = str.replace(/http:/, "https:");
                return str;
            } else {
                return str;
            }
        }
    })
    .filter("listThumbScale",function() {
        var wid = document.body.clientWidth;
        var miniFlag = false;
        if (wid < 501) {
            miniFlag = true;
        }
        return function(str,size) {
            if (str) {
                if (!size) {
                    if (miniFlag) {
                        size = 200;
                    } else {
                        size = 400;
                    }
                }
                str = str.replace(/upload/, 'upload/w_' + size + ",f_auto");
                str = str.replace(/http:/, "https:");
                return str;
            } else {
                return str;
            }
        }
    })
    .filter("timeFilter",function() {
        function dateTotime(date_time) {
            var timestr = new Date(parseInt(date_time) * 1000);
            var datetime = timestr.toLocaleString().replace(/年|月/g, "-").replace(/日/g, " ");
            return datetime;
        }
        return function(str) {
            var localStr = dateTotime(str);
            return localStr;
        }
    })
    .filter("titleParseFilter",function() {
        return function(url) {
            var res = url.toLowerCase().trim().replace(/ & /g, " ").replace(/\//g, "").replace(/-/g, "").replace(/%/g, "")
                .replace(/ /g, "-").replace(/,/g, "").replace(/\./g, "").replace(/'/g, "").replace(/"/g, "").replace(/--/g, "-");
            return res;
        }
    })
    .filter("titleCut",function() {
        return function(title) {
            if (title.length > 100) {
                title = title.substring(0,100) + "...";
            }
            return title;
        }
    })
    .filter("getCategoryName",
    ['categoryData',function(categoryData) {
        return function(url,catId) {
            if (catId) {
                url = categoryData[catId].title + " " + url
            }
            return url;
        }
    }])
    .factory('parseURL', function() {
        function parse(url) {
            var a =  document.createElement('a');
            a.href = url;
            return {
                source: url,
                protocol: a.protocol.replace(':',''),
                host: a.hostname,
                port: a.port,
                query: a.search,
                params: (function(){
                    var ret = {},
                        seg = a.search.replace(/^\?/,'').split('&'),
                        len = seg.length, i = 0, s;
                    for (;i<len;i++) {
                        if (!seg[i]) { continue; }
                        s = seg[i].split('=');
                        ret[s[0]] = s[1];
                    }
                    return ret;
                })(),
                file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
                hash: a.hash.replace('#',''),
                path: a.pathname.replace(/^([^\/])/,'/$1'),
                relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
                segments: a.pathname.replace(/^\//,'').split('/')
            };
        }
        return {
            /**
             * get the param
             * @return the param
             */
            getParam : function(url,param) {
                var res = decodeURIComponent(parse(url)["params"][param]);
                if (res.indexOf("/?") > 0) {
                    res = res.split("/?")[0];
                }
                if (res == "undefined") {
                    res = null;
                }
                return res;
            }
        };
    })
    .factory('parseTitle', function() {
        return {
            /**
             * get the parsed title
             * @return the title
             */
            getParse : function(url) {
                var res = url.toLowerCase().trim().replace(/ & /g, " ").replace(/\//g, "").replace(/-/g, "").replace(/%/g, "")
                    .replace(/ /g, "-").replace(/,/g, "").replace(/\./g, "").replace(/'/g, "").replace(/"/g, "").replace(/--/g, "-");
                return res;
            }
        };
    })
    .factory('cookie', function() {
        return {
            /**
             * get the cookie value
             * @return the value
             */
             get : function(name,key) {
                if ($.cookie) {
                    var appStr = $.cookie(name);
                    if (appStr) {
                        appStr = JSON.parse(appStr);
                        return appStr[key];
                    }
                }
                return null;
            },
            /**
             * set the cookie value
             * @return the value
             */
            set : function(name,key,value) {
                if ($.cookie) {
                    var appStr = $.cookie(name);
                    if (appStr) {
                        appStr = JSON.parse(appStr);
                        appStr[key] = value;
                        $.cookie(name,JSON.stringify(appStr),{"path":"/","expires":730});
                    }
                }
            }
        };
    })
    .factory('categoryData', function() {
        var obj = $(".categoryDataJson").data("category-json");
        return obj;
    });

jQuery.trackingDownlaod = function(className){
    var defaultClass = ".img-google-play,.img-apple-app,.item_banner_img_down,.sb-button";
    defaultClass = className == undefined?defaultClass:(defaultClass+","+className);
    $(defaultClass).click(function(){
        $('<iframe class="hide" src="/download-tracking">').appendTo('body');
    })
}

jQuery.trackingDownlaod();

(function(window, document, undefined) {//autocomplete
    var id = "searchKeyword";
    if ($("#searchKeyword:visible").length == 0) {
        id = "searchKeyword_mb";
    }
    var myAutocomplete = new Autocomplete(id, {
    useNativeInterface : false,
    srcType : "dom"
    });
})(this, this.document);
