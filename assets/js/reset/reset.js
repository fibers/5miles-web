$(document).ready(function() {
    /**
     *  resetpwd layout
     */
    $("#resetButton").on("click", function(event) {
        if ($("#resetButton").hasClass("btn-disable")) {
            return true;
        }
        var emailStr = window.location.href;
        var emailArray = emailStr.split("reset/");
        var email = emailArray[1];
        var pwd = $("#form_password").val();
        if (!email || !pwd) {
            return true;
        }
        var data = {"email":email,"password":pwd};
        $("#resetButton").addClass("btn-disable");//disable the button
        $("#resetButtonLoad").show(); //show loading
        $.post('/resetpassword', data, function(result) {
            $("#resetButtonLoad").hide();
            if (result.status == 200) {
                $("#resetButton").unbind("click");
                $("#resetButtonTip").removeClass("text-danger").addClass("text-success").html("Success!");
            } else {
                $("#resetButton").removeClass("btn-disable")
                $("#resetButtonTip").removeClass("text-success").addClass("text-danger").html(result.msg);
            }
        });
    });
    //change the container height
    var clientHei = $(window).height() - 80;
    $("#resetPwdContainer").css("height",clientHei);

    /**
     *     reset layout
     */
    function validateEmail(str) {
        var reg=/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        if(reg.test(str) ){
            return true;
        }else{
            return false;
        }
    }
    $("#resetEmail").on("click", function(event) {
        if ($("#resetEmail").hasClass("btn-disable")) {
            return true;
        }
        var email = $("#form_password").val();
        if (email.length == 0) {
            return true;
        }
        $("#resetButtonTip").html("");
        if ((email.length > 0) && !validateEmail(email)) {
            $("#resetButtonTip").addClass("text-danger").html("invalidate email");
            return false;
        }
        var data = {"email":email};
        $("#resetEmail").addClass("btn-disable");//disable the button
        $("#resetButtonLoad").show(); //show loading
        $.post('/resetemail', data, function(result) {
            $("#resetButtonLoad").hide();
            if (result.status == 200) {
                $("#resetEmail").unbind("click");
                $("#resetButtonTip").removeClass("text-danger").addClass("text-success").html("Success send, please check your email!");
            } else {
                $("#resetEmail").removeClass("btn-disable")
                $("#resetButtonTip").removeClass("text-success").addClass("text-danger").html(result.msg);
            }
        });
    });
});

