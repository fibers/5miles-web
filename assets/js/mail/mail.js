angular.module("app")
    .controller('mailCtrl',
        ['$rootScope', '$scope','$http','$location',
            function($rootScope, $scope, $http) {
                var init = function(){
                    $scope.statusList = $('#userSubscriptions').data("statusList");
                    $scope.userToken = $("#userToken").attr("value");
                    $scope.isDisabled = false;
                    $scope.infoArea = {
                        classes : "",
                        html : ""
                    };
                    $scope.display = true;
                }
                init();
                $scope.mailTriggler = function(status){
                    status.action = status.action == 0? 1 : 0;
                }

                $scope.submitModify = function() {
                    $scope.isDisabled = true;
                    $http({method:"post",url:"/unsubscribe",data:{submit_args:JSON.stringify($scope.statusList),user_token:$scope.userToken }})
                        .success(function(data,status){
                            if(data.status ==200 ){
                                $scope.infoArea.classes = "text-success bg-success";
                                $scope.infoArea.html = "Update Success";

                            }else{
                                $scope.infoArea.classes = "text-danger bg-danger";
                                $scope.infoArea.html = "Update Fail";

                            }
                            $scope.isDisabled = false;
                        })
                        .error(function(data,status){
                            $scope.infoArea.classes = "text-danger bg-danger";
                            $scope.infoArea.html = "Update Fail";
                            $scope.isDisabled = false;
                        })
                };
            }]
    );

