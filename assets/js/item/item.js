angular.module("app")
    .controller('itemController',
        ['$rootScope', '$scope','$http','parseURL', 'parseTitle', 'categoryData',
            function($rootScope, $scope, $http,parseURL,parseTitle,categoryData) {
                // GA
                $scope.product_seller = function(event) {
                    ga('send', 'event', 'product_view', 'product_seller');
                };
                $scope.similarProduct = function(event) {
                    ga('send', 'event', 'product_view', 'similar_product');
                };
                $scope.similar_item = function(){
                    ga('send', 'event', 'product_view', 'similaritem');
                }
                $scope.seller_other_item = function(){
                    ga('send', 'event', 'product_view', 'sellerotheritem');
                }
                $scope.other_item_product = function(event){
                    ga('send', 'event', 'product_view', 'sellerotheritem_product');
                }
                $scope.add_to_likes = function(event){
                    ga('send', 'event', 'product_view', 'addtolikes');
                }
                $scope.remove_from_likes = function(event){
                    ga('send', 'event', 'product_view', 'removefromlikes');
                }
                $scope.seller_items = function(event){
                    ga('send', 'event', 'product_view', 'selleritems');
                }
                $scope.seller_following = function(event){
                    ga('send', 'event', 'product_view', 'sellerfollowing');
                }
                $scope.seller_folowers = function(event){
                    ga('send', 'event', 'product_view', 'sellerfollowers');
                }
                $scope.verify_info = function(event){
                    ga('send', 'event', 'product_view', 'sellerverifyinfo');
                }
                $scope.crumb_home = function(event){
                    ga('send', 'event', 'produt_view', 'pcrumb_home');
                }
                $scope.crumb_category = function(event){
                    ga('send', 'event', 'product_view', 'pcrumb_category');
                }
                $scope.renew = function(event){
                    ga('send', 'event', 'product_view', 'renew');
                }
                $scope.renew_ios = function(event){
                    ga('send', 'event', 'product_view', 'renew_ios');
                }
                $scope.renew_android = function(event){
                    ga('send', 'event', 'product_view', 'renew_android');
                }
                $scope.renew_smssend = function(event){
                    ga('send', 'event', 'product_view', 'renew_smssend');
                }
                $scope.voice_play = function(event){
                    ga('send', 'event', 'product_view', 'voiceplay');
                }
                $scope.voice_stop = function(event){
                    ga('send', 'event', 'product_view', 'voicestop');
                }
                $scope.pinterest_share = function(event){
                    ga('send', 'event', 'product_view', 'pinterestshare');
                }
                $scope.twitter_share = function(event){
                    ga('send', 'event', 'product_view', 'twittershare');
                }
                $scope.facebook_share = function(event){
                    ga('send', 'event', 'product_view', 'facebookshare');
                }

                $scope.item_like_avatar = function(event){
                    ga('send', 'event', 'product_view', 'itemlikeavatar');
                }
                $scope.report_item = function(event){
                    ga('send', 'event', 'product_view', 'reportitem');
                }
                $scope.report_item_send = function(event){
                    ga('send', 'event', 'product_view', 'reportitemsend');
                }
                $scope.report_seller = function(event){
                    ga('send', 'event', 'product_view', 'reportseller');
                }
                $scope.report_seller_send = function(event){
                    ga('send', 'event', 'product_view', 'reportsellersend');
                }

                $scope.sign_submit = function() {
                    ga('send', 'event', 'quicksign_view', 'submit');
                };
                $scope.sign_cancel = function(event) {
                    ga('send', 'event', 'quicksign_view', 'cancel');
                };
                $scope.sign_login = function(event) {
                    ga('send', 'event', 'quicksign_view', 'login');
                };
                $scope.buy_close = function(event) {
                    ga('send', 'event', 'buy_view', 'close');
                };
                $scope.buy_show = function(event){
                    ga('send', 'event','product_view','buy');
                    ga('send', 'event','buy_view','show');
                }
                $scope.offer_send = function(event){
                    ga('send', 'event','buy_view','makeoffer');
                }
                $scope.askmore = function(event){
                    ga('send', 'event','buy_view','askmore');

                }
                $scope.change_price = function(event){
                    ga('send', 'event','buy_view','changeprice');

                }
                $scope.ask_show = function(event) {
                    ga('send', 'event','product_view','ask');
                    ga('send', 'event', 'ask_view', 'show');
                };
                $scope.ask_close = function(event) {
                    ga('send', 'event', 'ask_view', 'close');
                };
                $scope.ask_send = function(event) {
                    ga('send', 'event', 'ask_view', 'send');
                };

                var cateData = categoryData;
                $scope.modal_msg_body = "";
                var userId = $("#item_user_info").attr("userid");
                var itemId = $("#item_user_info").attr("itemid");
                $("body").bind("click", function(event) {
                    $(".ver-infos").removeClass("in");
                });

                var _showLikers = function(){
                    $http.get("/likerlist/"+itemId).then(function(revData){
                        var data = revData.data;
                        if(data.meta == undefined){
                            $scope.likes.count = 0;
                            return;
                        }
                        var count = data.meta.total_count;
                        var list = data.objects.length<=14?data.objects:data.objects.splice(13);
                        $scope.likes={
                            count: count,
                            list:list
                        }
                    })
                }
                var iniSellerItem = false;

                var _showSellerItem = function(){
                    if(iniSellerItem == false){
                        $http.get("/sellerItems/"+userId+"?limit=4").then(function(receiveData){
                            $scope.sellerItems = receiveData.data.objects;
                            angular.forEach($scope.sellerItems, function(item,key) {
                                var preTitle = cateData[item.cat_id] ? cateData[item.cat_id].title : "";
                                $scope.sellerItems[key].idParse = parseTitle.getParse(preTitle + " " + item.title);
                            });
                        });
                    }
                }
                var _showSellerInfo = function(){
                    $http.get("/personinfo/"+userId).then(function(httpData){
                        var data = httpData.data;
                        $scope.person = {};
                        $scope.person.personItem = data.count_sellings;
                        $scope.person.personFollowing = data.count_following;
                        $scope.person.personFollowers = data.count_followers;
                        $scope.person.followed = data.followed;
                        $scope.person.mobile_ver = data.mobile_phone_verified;
                        $scope.person.email_ver = data.email_verified;
                        $scope.person.facebook_ver = data.facebook_verified;
                        if(data.count_sellings>0){
                            $(".nav-seller").removeClass("hide");
                        }
                        
                        if($.cookie("5miles_head")&& $.cookie("5miles_follow")){
                            $scope.followPerson();
                        }
                    })
                }
                var init = function(){
                    _showSellerInfo();
                    $scope.likes = {};
                    _showLikers();
                    _showSellerItem();
                    $scope.otherItemDis = false;
                    $scope.recommendItemDis = true;
                }
                init();

                $scope.showOtherItems = function(){
                    $scope.otherItemDis = true;
                    $scope.recommendItemDis = false;
                    $scope.seller_other_item();
                }
                $scope.showSimilarItems = function(){
                    $scope.otherItemDis = false;
                    $scope.recommendItemDis = true;
                    $scope.similar_item();
                }

                $scope.showAskModal = function(event,flag) {
                    if(flag.indexOf("pop")!=-1){
                        $scope.askmore();
                    }
                    if(flag.indexOf("ask")!=-1){
                        $scope.buy_item_show = "false";
                        $scope.ask_item_show = "true";
                        $scope.ask_show();
                    }else if(flag.indexOf("buy")!=-1){
                        $scope.buy_item_show = "true";
                        $scope.ask_item_show = "false";
                        $("#ask_body").val("");
                        $scope.buy_show();
                    }
                    // judge the repeat send
                    if($scope.buy_item_show == "true"){
                        var itemTimestamp = $.cookie("5miles_item__" + itemId);
                    }

                    if (itemTimestamp) {
                        var nowTimestamp = $.now();
                        var divNum=1000*60;
                        var min = parseInt((nowTimestamp-itemTimestamp)/parseInt(divNum));
                        if (min < 3) {
                            $scope.modal_msg_body = $scope.modalMsg.tooTimes.body;
                            $scope.modal_msg_footer = $scope.modalMsg.tooTimes.footer;
                            $('#item_register_modal').modal('hide');
                            $('#item_ask_modal').modal('hide');
                            $('#item_msg_modal').modal('show');
                            return false;
                        }
                    }
                    $scope.offer_number_tip = "";
                    $('#item_ask_modal').modal('show');
                };

                $scope.askThenSubmit = function() {
                    //ga
                    if($scope.ask_item_show){
                        $scope.ask_send();
                    }else if($scope.buy_item_show){
                        $scope.offer_send();
                    }
                    // validate the value
                    $("#req_user_nick").val("");
                    $("#req_user_email").val("");

                    var askValue = $("#ask_body").val();
                    var offerNumber = $("#offer_number").val();
                    if ((askValue.length == 0) && (offerNumber.length == 0)) {
                        $scope.offer_number_tip = "";
                    } else if ((offerNumber.length > 0) && !$scope._validateOfferNumber(offerNumber)) {
                        $scope.offer_number_tip = "Please enter a number.";
                        return true;
                    } else {
                        if ($scope._existSession()) {
                            $scope.sendAsk();
                        } else {
                            $('#item_ask_modal').modal('hide');
                            $scope.modal_login_msg_title = $scope.modalLoginMsg.send.title;
                            $scope.modal_login_msg_body = $scope.modalLoginMsg.send.body;
                        
                            $('#item_register_modal').modal('show');
                            $scope._restSubmitTip();
                        }
                    }
                };
                $scope.switchLikes = function(){
                    if (!$scope._existSession()) {
                        $.cookie("5miles_likes","true",{"path":"/"});
                        window.location.href = "/loginShow/?back=/item/" + itemId + "&param=sign";
                        return;
                    }
                    //user already login
                    $scope.liked = ($scope.liked == undefined)? $("#item_user_info").attr("liked"):$scope.liked;
                    
                    if($scope.liked == "false"){
                        $scope.add_to_likes();
                        var likeUrl = "/like/";
                    }else{
                        $scope.remove_from_likes();
                        var likeUrl = "/unlike/";
                    }
                    $http.post( likeUrl +$("#item_user_info").attr("itemid") ,{}).then(function(revData){
                        $.removeCookie("5miles_likes",{"path":"/"});
                        if($scope.liked == "false"){
                            $scope.liked = "true"; 
                            $scope.hoverRemoveDisplay = true;
                        } else{
                            $scope.liked = "false";
                            $scope.hoverRemoveDisplay = false;
                        }
                        _showLikers();
                    }) 
                    
                }
                
                $scope.triggerTips = function(event){
                    $(".ver-infos").toggleClass("in");
                    $scope.verify_info();
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.askBuyInfo = function(){
                    if (!$scope._existSession()) {
                        $.cookie("5miles_likes","true",{"path":"/"});
                        window.location.href = "/loginShow/?back=/my_message&param=sign";
                        return;
                    }
                    window.location.href = "/my_message";
                }
                $scope._validateEmail = function(str) {
                    var reg=/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                    if( reg.test(str) ){
                        return true;
                    }else{
                        return false;
                    }
                }
                $scope._validateNickName = function(str) {
                    var re=/[\\:*;?"'$<>|]/;
                    if(str.match(re)){
                        return false;
                    }
                    return true;
                }
                $scope._validateOfferNumber = function(s) {
                    if (s!=null && s!="") {
                        return !isNaN(s);
                    }
                    return false;
                }
                $scope._restSubmitTip = function() {
                    $scope.submitNickTip = "";
                    $scope.submitEmailTip = "";
                }
                $scope.submit = function(event) {
                    //ga
                    $scope.sign_submit();
                    // validate
                    var toNick = $("#req_user_nick").val();
                    var toEmail = $("#req_user_email").val();

                    if ((toNick.length == 0) || (toEmail.length == 0)) {
                        return true;
                    }

                    if (toNick.length > 0) {//email validate
                        if (toNick.length > 20) {
                            $scope.submitNickTip = "Too long nick name!";
                            return false;
                        }
                        if (!$scope._validateNickName(toNick)) {
                            $scope.submitNickTip = "nick name not right!";
                            return false;
                        }
                    }
                    if (toEmail.length > 0) {//email validate
                        if (!$scope._validateEmail(toEmail)) {
                            $scope.submitEmailTip = "email not right!";

                            return false;
                        }
                    }
                    // send the msg
                    $scope.sendAsk();
                };
                $scope._existSession = function() {
                    if ($.cookie("5miles_head")) {
                        return true;
                    } else {
                        return false;
                    }
                }
                $scope.getCookie = function() {
                    return JSON.parse($.cookie("5miles_head"));
                }
                // the buy
                function _postBackFunc (res,param) {
                    if (res && (res.status == 200)) {
                        var data = res.data;
                        var statusCode = data.status;
                        if (statusCode == 200 ) {

                            var exitHead = {};
                            var fiveHead = $.cookie("5miles_head");
                            if (fiveHead) {//get the exist head
                                exitHead = JSON.parse(fiveHead);
                            }
                            if ($("#req_user_nick").val().length > 0) {// first time for sending
                                $scope.modal_msg_body = $scope.modalMsg.welcome.body;
                                $scope.modal_msg_footer = $scope.modalMsg.welcome.footer;
                                data.head["nick"] = param.from.user_nick;
                                data.head.email = param.from.user_email;

                                // signok
                                $("#sign_ok_frame_item").attr("src","/ga_signOK");
                            } else {
                                $scope.modal_msg_body = $scope.modalMsg.msgSend.body;
                                $scope.modal_msg_footer = "";

                                data.head["nick"] = param.from.user_nick;
                                data.head.email = param.user_token;
                            }
                            data.head = $.extend(exitHead,data.head);//merge the head

                            $.cookie("5miles_head",JSON.stringify(data.head),{"path":"/","expires":730});
                            //ga offer ok
                            $("#offer_ok_frame_item").attr("src","/ga_offerOK");

                            $('#item_register_modal').modal('hide');
                            $('#item_ask_modal').modal('hide');
                            $('#item_msg_modal').modal('show');
                            // send ok record timestamp
                            var timestamp = $.now();
                            //only if buy request have time restrict
                            
                            if($scope.buy_item_show == "true"){
                                $.cookie("5miles_item__" + param.item,timestamp,{"path":"/item"});
                            }
                            // the first time to broadcast
                            $rootScope.$broadcast("LOGIN:user:reload");
                            // hide the modal
                            setTimeout(function() {
                                $('#item_msg_modal').modal('hide');
                            },1500);
                        } else {
                            $scope.submitEmailTip = data.body;
                        }
                    } else {
                        alert("error");
                    }
                };


                // the ask
                $scope.sendAsk = function(event) {
                    if ($scope.submit_load_flag) {
                        return true;
                    }
                    $scope.submit_load_flag = true;
                    var offerNumber = $scope.buy_item_show == "true"?$("#offer_number").val():"";

                    var body = $scope.ask_item_show == "true"?$("#ask_body").val():"";
                    var fromNick = $("#req_user_nick").val();
                    var fromEmail = $("#req_user_email").val();
                    // after login then auto send the cookie data
                    if ($.cookie("5miles_head") && $.cookie("5miles_offer")) {
                        var obj = JSON.parse($.cookie("5miles_head"));
                        var offerObj = JSON.parse($.cookie("5miles_offer"));
                        offerNumber = offerObj.offer;
                        body = offerObj.ask;
                        fromNick = obj.nick;
                        fromEmail = obj.mail;
                    }

                    var param = {"to":{"user_id":userId},
                        "from": {"user_nick":fromNick,"user_email":fromEmail},
                        "item": itemId,
                        "price": offerNumber,
                        "body": body};
                    if ($.cookie("5miles_head")) {
                        var obj = JSON.parse($.cookie("5miles_head"));
                        param["user_token"] = obj.userToken;
                        param["user_id"] = obj.userId;
                        param.from.user_nick =  obj.nick;
                        param.from.user_email =  obj.mail;
                    }
                    $http.post("/@mail/ask", param).then(function(res) {
                        _postBackFunc(res,param);
                        $scope.submit_load_flag = false;
                        $.removeCookie("5miles_offer",{"path":"/"});
                    },function(error) {
                        $scope.submitEmailTip = "Send error!";
                        $scope.submit_load_flag = false;
                    });
                };
                $scope.forwardLogin = function(event) {
                    //ga
                    $scope.sign_login(event);
                    //stash the vars
                    var offer = {"offer":$("#offer_number").val(),"ask":$("#ask_body").val()};
                    $.cookie("5miles_offer",JSON.stringify(offer),{"path":"/"});
                    window.location.href = "/loginShow/?back=/item/" + itemId;
                };
                var reportListArray = $("#report_modal").data("report");
                var reportTxt = {
                    item : {
                        title:"Report Item",
                        reason:reportListArray[0].objects,
                        placeholder:"Why are you reporting this item?(Optional)"
                    },
                    seller : {
                        title:"Report Seller",
                        reason:reportListArray[1].objects,
                        placeholder:"Why are you reporting this seller?(Optional)"
                    }
                }
                $scope.currentReportItem = {};
                /**
                 * show report popup modal
                 * @param  {[type]} reportType item|seller
                 */
                $scope.showReportModel = function(reportType){
                    if (!$scope._existSession()) {
                        $.cookie("5miles_report",reportType,{"path":"/"});
                        window.location.href = "/loginShow/?back=/item/" + itemId + "&param=sign";
                        return;
                    }
                    if(reportType == "all"){
                        
                    }
                    $scope.currentReportItem = {
                        reportType : reportType
                    };
                    $("#reason_content").val("");

                    $scope.reportShow = reportTxt[reportType];
                    $(".item_modal_body-msg .form-group div").removeClass("selected");

                    if(reportType == "item"){
                        $scope.currentReportItem.item_id = $("#item_user_info").attr("itemid");
                        $scope.contentPlaceholder = reportTxt.item.placeholder;
                    }else{
                        $scope.currentReportItem.user_id = $("#item_user_info").attr("userid");
                        $scope.contentPlaceholder = reportTxt.seller.placeholder;
                    }
                    
                    $('#report_modal').modal('show');
                }
                $scope.selectReason = function(event,id){
                    $(event.currentTarget.parentElement.children).removeClass("selected");
                    $(event.currentTarget).addClass("selected");
                    $scope.currentReportItem.reason = id;
                }
                $scope.sendRequest = function(){
                    $scope.currentReportItem.reason_content = $("#reason_content").val();
            
                    if($.cookie("5miles_head")&&$.cookie("5miles_report")){
                        //already login and have set report ;
                        $scope.currentReportItem = JSON.parse($.cookie("5miles_report"));
                    }

                    if($scope.currentReportItem.reportType == "item"){
                        var postUrl = "/reportItem/";
                        $scope.report_item_send();
                    } else {
                        var postUrl = "/reportSeller/";
                        $scope.report_seller_send();
                    }
                    $http.post( postUrl ,$scope.currentReportItem).then(function(revData){
                        $scope.modal_msg_body = $scope.modalMsg.report.body ;
                        $('#report_modal').modal('hide');
                        $('#item_msg_modal').modal('show');
                        setTimeout(function() {
                            $('#item_msg_modal').modal('hide');
                            $scope.modal_msg_body = "";
                        },2000);
                    }) 
                }
                // after redirect login then send the ask
                $scope._autoSend = function(event) {
                    if ($.cookie("5miles_head") && $.cookie("5miles_offer")) {
                        $scope.sendAsk();
                    }else if($.cookie("5miles_head")&& $.cookie("5miles_report")){
                        $scope.showReportModel($.cookie("5miles_report"));
                        $.removeCookie("5miles_report",{"path":"/"});
                    }else if($.cookie("5miles_head")&& $.cookie("5miles_likes")){
                        $scope.switchLikes();
                    }
                };
                $scope._autoSend();
                $scope.modalMsg = {"tooTimes" : {"body":"You have recently sent a message","footer":"Please wait about 3 minutes to send another message."},
                    "msgSend": {"body":"Message sent!","footer":""},
                    "welcome":{"body":"Welcome to 5miles !","footer":"We have already sent login information to your email."},
                    "report":{"body":"Message Sent!"}
                };
                $scope.modalLoginMsg = {
                    send : {title:"Send...",body:"Please provide an email and name.<br>so that we can send the seller your message."},
                    report : {title:"Thanks for reporting...",body:"Please provide an email and name.<br>so that we can contact with about the report."}
                }
                // the video dialog
                $('#item_register_modal').modal({
                    keyboard: false,
                    backdrop:'static',
                    show:false
                });
                $('#item_ask_modal').modal({
                    keyboard: false,
                    backdrop:'static',
                    show:false
                });
                $('#item_msg_modal').modal({
                    keyboard: false,
                    backdrop:'static',
                    show:false
                });
                $scope._verifiedPhone = function() {
                    var reffer = document.referrer;
                    if (reffer.indexOf("validate/phone") > 0) {
                        var verified = parseURL.getParam(window.location,"verified");
                        if (verified == "phone") {
                            $scope.modal_msg_body = "Thanks for verifying your phone number!";
                            $('#item_msg_modal').modal('show');
                            setTimeout(function() {
                                $('#item_msg_modal').modal('hide');
                                $scope.modal_msg_body = "";
                            },2000);
                        }
                    }
                };

                $scope._verifiedPhone();


                
            }]
    ).filter('to_trusted',['$sce',function($sce){
        return function(text){
            return $sce.trustAsHtml(text);
        }
    }]);

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip("show");
    function smsSend() {
        ga('send', 'event', 'item_view', 'item_smssend');
    }
    //send email and phone
    $('.btn-send').on('click', function(e) {
        function filterCountryCode(str) {
            var arr2 = str.split("(+");
            if (arr2[1]) {
                var arr = arr2[1].split(")");
                return ("+"+arr[0]);
            }
            return null;
        }
        var data, dest, input, type;
        input = $(this).parents('.form-group').find('input');
        dest = input.val();
        type = input.attr('id');
        data = {
            dest: dest,
            type: type
        };
        if (dest !== '' && type !== '') {
            //before process
            if (type == "phone") {
                $("#phoneTip").removeClass("send-tip-ok").html("");
                //validate
                var countryCodeStr = $("#sms_link_country option:selected").html();
                var countryCode = filterCountryCode(countryCodeStr);
                if (!countryCode) {
                    $("#phoneTip").html("Please select country code !");
                    return false;
                }
                var emailExp = /^[0-9]*$/;
                if(!emailExp.exec(dest)){//validate for phone
                    $("#phoneTip").html("Please check your number!");
                    return false;
                } else  {
                    $("#phoneTip").html("");
                }
                //prevent too much times click
                if ($("#phoneTip").data("phoneSendFlag") == "true") {
                    $("#phoneTip").html("Please try again after 1 minute !");
                    return true;
                } else {
                    $("#phoneTip").html("");
                    $("#phoneTip").data("phoneSendFlag","true");
                    setTimeout(function() {
                        $("#phoneTip").data("phoneSendFlag","false");
                        $("#phoneTip").html("");
                    }, 60000);
                }
                smsSend();
                data.dest = countryCode + data.dest;
            } else if (type == "email") {
                var mailpatrn = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!mailpatrn.exec(dest)){//validate for email
                    $("#emailTip").html("Please check your number!");
                    return false;
                } else  {
                    $("#emailTip").html("");
                }
                emailSend();
            }
            //phone || email ,send message and params
            $.post('/message', data, function(result) {
                if (result.success) {
                    input.val('');
                    $("#phoneTip").addClass("send-tip-ok").html("Link sent successfully!");
                } else {
                    alert(result.data)
                }
            });
            //hide the dialog
            $('#item_renew_modal').modal('hide');
        } else if (dest === '') {
            if (type === "phone") {
                return $("#phoneTip").html("Please input your phone number!");
            } else if (type === "email") {
                return $("#emailTip").html("Please input your email !");
            }
        }
    });
})