angular.module("app")
    .controller('itemUpload',
        ['$rootScope', '$scope','$http','cookie',
            function($rootScope, $scope, $http, cookie) {
                $scope.nowPage = "ITEMUPLOAD";
                $scope.uploadNow = function($event) {
                    alert("upload Now");
                }
                $scope.initUser = function() {
                    if ($.cookie) {
                        var appStr = $.cookie("5miles_head");
                        if (appStr) {
                            appStr = JSON.parse(appStr);
                            $scope.login_user_id = appStr.userId;
                            $scope.login_user_token = appStr.userToken;
                            return true;
                        }
                    }
//                    window.location.href = "/loginShow";
                }
                // logout
                $scope.doLogout = function($event) {
                    var param = {
                    }
                    $http.post("/loginOut", param).then(function(res) {
                        if (res.data && res.data.SUCCESS) {
                            window.location.href = "/loginShow";
                        } else {
                            alert("error");
                        }
                    });
                }
                // get my items
                $scope.myItems = [];
                $scope.getItems = function(moreFlag) {
                    var param = {};
                    if (moreFlag) {
                        param["next"] = $scope.myMeta.next;
                    }
                    $http.post("/sellerItems", param).then(function(res) {
                        if (res.data) {
                            if (moreFlag) {
                                $scope.myItems = $scope.myItems.concat(res.data.objects);
                            } else  {
                                $scope.myItems = res.data.objects;
                            }
                            $scope.myMeta = res.data.meta;
                            // show or hide the more
                            $scope.upload_list_more_load_flag = false;
                            if ($scope.myMeta.next == null) {
                                $scope.upload_list_more_flag = false;
                            } else {
                                $scope.upload_list_more_flag = true;
                            }
                        } else {
                            alert("error");
                        }
                    });
                }
                if ($scope.login_user_id) {
//                    $scope.getItems();// init
                }
                // form post
                function resetTheForm() {
                    jQuery("#title").val("");
                    jQuery("#price").val("");
                    jQuery("#description").val("");
                    window.images = [];
                    jQuery(".upload_img_block_pre").remove();
                }
                function hideRequire() {
                    $scope.images_req_flag = false;
                    $scope.title_req_flag = false;
                    $scope.category_req_flag = false;
                    $scope.price_req_flag = false;
                }
                function _validateInt(str) {
                    var reg = /^[0-9]+([.]\d{1,2})?$/;
                    if(!reg.test(str)) {
                        return false
                    }
                    return true;
                }
                $scope.validatePrice = function(event) {
                    $scope.price_req_flag = false;
                    var price = jQuery("#price").val();
                    if (price.length == 0) {
                        $scope.price_req_flag = true;
                        return false;
                    }
                }
                hideRequire();
                $scope.initUser();
                $scope.upload_load_flag = false;

                $scope.formUpload = function(event) {
                    // vars
                    hideRequire();

                    var title = jQuery("#title").val();
                    var category = jQuery("#category option:selected").attr("id");
                    var currency = jQuery("#currency option:selected").attr("id");
                    var delivery = jQuery("#delivery option:selected").attr("id");
                    var price = jQuery("#price").val();
                    var description = jQuery("#description").val();
                    var imagesArray = window.images;

                    // validate
                    if (imagesArray.length == 0) {
                        $scope.images_req_flag = true;
                        return false;
                    } else if (title.length == 0) {
                        $scope.title_req_flag = true;
                        return false;
                    } else if (category.length == 0 || category == "default") {
                        $scope.category_req_flag = true;
                        return false;
                    } else if (price.length == 0) {
                        $scope.price_req_flag = true;
                        return false;
                    } else if (price.length > 0) {
                        if (!_validateInt(price)) {
                            $scope.price_req_flag = true;
                            return false;
                        }
                    }
                    // item obj
                    var fd = {};
                    fd["title"] = title;
                    fd["category"] = category;
                    fd["shipping_method"] = delivery;
                    fd["currency"] = currency;
                    fd["price"] = price;
                    fd["description"] = description;
                    for(var i = 0; i < imagesArray.length; i++) {
                        imagesArray[i].width = parseInt(imagesArray[i].width);
                        imagesArray[i].height = parseInt(imagesArray[i].height);
                    }
                    fd["images"] = JSON.stringify(imagesArray);
                    // loading icon
                    $scope.upload_load_flag = true;
                    // not login status set the data to cookie
                    if ($scope.login_user_id) {
                        // post the item
                        $http.post("/item/uploadFile", fd).then(function(res) {
                            if (res.data && res.data.new_id) {
                                //set the newId to cookie
                                cookie.set("5miles_head","new_id",res.data.new_id);
                                // ga
                                $scope.listnow();
                                // get the verified status
                                var verified = cookie.get("5miles_head","verified");
                                if (verified) {
                                    $.removeCookie($scope.login_user_id + "_pop",{path:"/"});
                                    window.location.href = "/item/" + res.data.new_id;
                                } else {
                                    // pop the validate phone number
                                    var time = $.cookie($scope.login_user_id + "_pop");
                                    if (time == null || time == undefined) {
                                        $.cookie($scope.login_user_id + "_pop",1,{"path":"/","expires":730});
                                        window.location.href = "/validate/phone";
                                    } else if(time == 1) {
                                        $.cookie($scope.login_user_id + "_pop",2,{"path":"/","expires":730});
                                        window.location.href = "/validate/phone";
                                    } else {
                                        window.location.href = "/item/" + res.data.new_id;
                                    }
                                }
                            }
                        });
                    } else {
                        $.cookie("5miles_item",JSON.stringify(fd),{"path":"/","expires":730});
                        window.location.href = "/loginShow/?param=sign&state=upload";
                    }
                }
                // list more
                $scope.listMore = function(event) {
                    $scope.upload_list_more_load_flag = true;
                    $scope.getItems(true);
                }
                // ga
                $scope.takephoto = function(event) {
                    ga('send', 'event', 'sellh5_view', 'takephoto');
                }
                $scope.selectcategory = function(event) {
                    ga('send', 'event', 'sellh5_view', 'selectcategory');
                }
                $scope.selectdelivery = function(event) {
                    ga('send', 'event', 'sellh5_view', 'selectdelivery');
                }
                $scope.selectcurrency = function(event) {
                    ga('send', 'event', 'sellh5_view', 'selectcurrency');
                }
                $scope.listnow = function(event) {
                    ga('send', 'event', 'sellh5_view', 'listnow');
                }
            }]
    );
// ga
function ga_selectalbum () {
    ga('send', 'event', 'sellh5_view', 'selectalbum');
}
function ga_deletephoto () {
    ga('send', 'event', 'sellh5_view', 'deletephoto');
}





// the uploaded images delete
window.imagesCount = 6;
function uploadImgBlockDelete(link) {
    if (link) {
        var flag = confirm("Are you sure to delete this picture?");
        if (flag) {
            var images = window.images;
            var tempArray = [];
            for (var i = 0;i < images.length;i++) {
                if (link == images[i].imageLink) {
                    link = link.replace("upload","upload/w_200,h_200");
                    $(".upload_img[src='" + link + "']").parent().remove();
                } else {
                    tempArray.push(images[i]);
                }
            }
            window.images = tempArray;
            if (window.images.length < window.imagesCount) {
                $("#upload_add_con").show();
            }
        }
    }
    ga_deletephoto();
}
// sign ok
function goToTheUploadPage() {
    window.location.href = "/item/uploadView";
}
$(document).ready(function() {
    // set the file multi
    if (/(iPhone|iPad|iPod)/i.test(navigator.userAgent)) {//iphone show the camera
    } else {
        $('#file').attr("multiple","");
    }
    $('#file').fileupload({
        disableImageResize: false,
        imageMaxWidth: 900,
        imageMaxHeight: 900,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|bmp|ico)$/i,
        maxFileSize: 20000000 // 20MB
    });
    // process the images upload begin
    $('#file').bind('fileuploadstart', function(e){
        $("#images_req_flag").addClass("ng-hide");
        $('#progress').css('display','block');
        ga_selectalbum();
    });
    $('#file').bind('fileuploadprogressall', function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        })
    // Upload finished
    window.images = [];
    $('#file').bind('fileuploaddone', function(e, data) {
        if (window.images.length == window.imagesCount) { //the limit is 6
            alert("Just can upload 6 pictures!");
            return false;
        }
        if (data && data.result) {
            var item = data.result;
            var url = "";
            var orgUrl = "";

            if (item.url) {
                orgUrl = item.url;
                url = item.url;
                url = url.replace("upload","upload/w_200");
            } else  {
                url = "http://res.cloudinary.com/fivemiles/image/upload/w_200,h_200/v" + item.version + "/" + item.public_id + "." + item.format;
                orgUrl = "http://res.cloudinary.com/fivemiles/image/upload/v" + item.version + "/" + item.public_id + "." + item.format;;
            }
            var blockStr = "<div class='upload_img_block upload_img_block_pre'><img class='upload_img' src='"
                + url + "'/>"
                + "<div class='upload_img_block_delete' onclick=\"uploadImgBlockDelete('" + orgUrl + "')\"></div></div>";
            $(blockStr).insertBefore($("#upload_add_con"));

            $('#progress').css('display','none');
            $('#progress .progress-bar').css('width','0%');

            // reset the width and height
            var obj = {"width":"" + (item.width || "auto"),"height": "" + (item.height || "auto"),"imageLink": "" + orgUrl};
            var img_url = url;
            var img = new Image();
            img.src = img_url;
            var picState = "vetical";
            if(img.complete){
                if (img.width > img.height) {
                    picState = "horizontal";
                }
                if ((picState == "vetical") && (item.width > item.height)) {//reverse when the return data width and height not right
                    obj["width"] = item.height;
                    obj["height"] = item.width;
                }
                window.images.push(obj);
                if (window.images.length == window.imagesCount) {
                    $("#upload_add_con").hide();
                }
            }else{
                // loading ok
                img.onload = function(){
                    if (img.width > img.height) {
                        picState = "horizontal";
                    }
                    if ((picState == "vetical") && (item.width > item.height)) {//reverse when the return data width and height not right
                        obj["width"] = item.height;
                        obj["height"] = item.width;
                    }
                    window.images.push(obj);
                    if (window.images.length == window.imagesCount) {
                        $("#upload_add_con").hide();
                    }
                };
            }
        }
        return true;
    });
    $('#file').bind('fileuploadfail', function (e, data) {
        alert("upload fail!");
    });

    $('#file').cloudinary_fileupload();
    // process the images upload end


    // fill the form by cookie
    function checkTempItem() {
        if ($.cookie) {
            var appStr = $.cookie("5miles_item");
            if (appStr) {
                appStr = JSON.parse(appStr);
                if (appStr.title) {
                    return true;
                }
            }
        }
        return false;
    }
    function fillTheForm () {
        if (checkTempItem()) {
            var appStr = $.cookie("5miles_item");
            if (appStr) {
                appStr = JSON.parse(appStr);
                $("#title").val(appStr.title);
                $("#category option[selected=selected]").removeAttr("selected");
                $("#category option[id=" + appStr.category +"]").attr("selected","selected");
                $("#delivery option[selected=selected]").removeAttr("selected");
                $("#delivery option[id=" + appStr.shipping_method +"]").attr("selected","selected");
                $("#currency option[selected=selected]").removeAttr("selected");
                $("#currency option[id='" + appStr.currency +"']").attr("selected","selected");
                $("#price").val(appStr.price);
                $("#description").val(appStr.description);

                // the images
                var images = appStr.images;
                images = JSON.parse(images);
                for (var i = 0; i < images.length; i++) {
                    var item = images[i];
                    var url = item.imageLink.replace("upload","upload/w_200,h_200");
                    var blockStr = "<div class='upload_img_block upload_img_block_pre'><img class='upload_img' src='"
                        + url + "'/>"
                        + "<div class='upload_img_block_delete' onclick=\"uploadImgBlockDelete('" + item.imageLink + "')\"></div></div>";
                    $(blockStr).insertBefore($("#upload_add_con"));
                }
                window.images = images;
                if (window.images.length == window.imagesCount) {
                    $("#upload_add_con").hide();
                }
            }

        }
    }
    fillTheForm();
});

//# sourceMappingURL=main.js.map
