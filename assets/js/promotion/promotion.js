angular.module("app")
    .controller('promotionCtl',
        ['$rootScope', '$scope','$http','$location',
            function($rootScope, $scope, $http) {
                var d_currentTime,d_startTime;
                var init = function(){
                    var currentTime = $("#currentTime").val();
                    var startTime = $("#startTime").val();
                    d_currentTime = new Date(currentTime);
                    d_startTime = new Date(startTime);
                    $scope.time = {
                    };
                    getTimeSpan(d_currentTime,d_startTime,$scope.time);

                    prettyTimeSpan();
                    startCounter();
                    trackingDownload();
                    if($(".tops").length >0){
                        updateNewItems();
                        updateTopUsers();
                        updateWinners();
                        $interval(updateTopUsers,1000*60);
                    }
                }
                init();



                $scope.showRules = function(){
                    $(".official-rules").removeClass("hide");
                    $(".click-to-know").remove();
                    location.hash = "terms";
                }

                function trackingDownload(){
                    $(".ios-download,.android-download").click(function(){
                        $('<iframe class="hide" src="/grand-prize-tracking">').appendTo('body');
                    })
                }

                function startCounter(){
                    if(d_startTime.getTime()<=d_currentTime.getTime())return;
                    var timer = setInterval(function(){
                        var newTime = d_currentTime.getTime()+100;
                        if(newTime <0) {
                            clearInterval(timer);
                            return;
                        }
                        d_currentTime = new Date(newTime);
                        getTimeSpan(d_currentTime,d_startTime,$scope.time);
                        prettyTimeSpan();
                        $scope.$apply();
                    },100);
                }
                function prettyTimeSpan(){
                    //Pretty view logic.
                    for(var i in $scope.time){

                        if(i == "days" && $scope.time[i]==0){
                            $scope.time.days ="";
                            $scope.daysTxt = "";
                            continue;
                        }else if(i =="days" && $scope.time[i] == 1){
                            $scope.daysTxt = "day";
                        }else if(i == "days"){
                            $scope.daysTxt = "days";
                        }
                        var s_number = "00"+String($scope.time[i]);
                        s_number = s_number.substring(s_number.length-2);
                        if(i=="millsec"){
                            s_number = String($scope.time.millsec);
                        }
                        s_number = s_number.split('').join("</b><b>")
                        $scope.time[i] = $sce.trustAsHtml("<b>"+s_number+"</b>");
                    }

                }
                function getTimeSpan(startDate,endDate,dateArgs){
                    var span = 0;
                    if(d_startTime.getTime()>d_currentTime.getTime()){
                        span = endDate.getTime() - startDate.getTime();
                    }

                    dateArgs.days = Math.floor(span/(1000*60*60*24));
                    span = span%(1000*60*60*24);
                    dateArgs.hours = Math.floor(span/(1000*60*60));
                    span = span%(1000*60*60);
                    dateArgs.min = Math.floor(span/(1000*60));
                    span = span%(1000*60)
                    dateArgs.sec = Math.floor(span/1000);
                    span = span%1000;
                    dateArgs.millsec = Math.floor(span/100);

                }

                function updateTopUsers(){
                    //TODO: refresh every 1 min
                    $http.get("/grand-prize-info?op=promotionRanking", {}).then(function(res) {
                        if (res.data && res.data.ERROR) {
                            alert(res.data.ERROR);
                        } else if (res.data) {
                            var users = res.data.users;
                            if(users.length == 0){
                                return;
                            }
                            $scope.showData = true;
                            $scope.topFive = [];
                            $scope.topOther =[];
                            var topLastIndex = 4;
                            if((users.length -1) < 4) topLastIndex = (users.length-1) ;
                            var topFiveSpan = users[0].item_num - users[topLastIndex].item_num;
                            for(var i=0,len= users.length;i<len;i++){
                                if(i<5){
                                    var percent = (users[i].item_num-users[topLastIndex].item_num)/topFiveSpan;
                                    var top = 45 + percent*45;
                                    users[i].top = top;
                                    //TODO: implement width;
                                    users[i].width = top;
                                    $scope.topFive.push(users[i]);
                                }else{
                                    $scope.topOther.push(users[i]);
                                }
                            }

                        }
                    });
                }

                function updateWinners(){
                    $scope.winners = [];
                    $http.get("/grand-prize-info?op=giftCard", {}).then(function(res) {
                        if (res.data && res.data.ERROR) {
                            alert(res.data.ERROR);
                        } else if (res.data) {
                            var users = res.data.users;
                            if(users.length == 0){
                                //hide winner-banner
                                $(".winner-banner").hide();
                                $(".gift-card-content .coupon").addClass("col-sm-offset-4").removeClass("col-sm-offset-1");
                            }else{
                                $scope.winners = users;

                                $timeout(function(){
                                    scroll($scope);

                                },0);
                            }
                        }
                    });
                }
                function updateNewItems(){
                    $scope.newItems = [];
                    $http.get("/grand-prize-info?op=promotionItem", {}).then(function(res) {
                        if (res.data && res.data.ERROR) {
                            alert(res.data.ERROR);
                        } else if (res.data) {
                            var items = [];
                            angular.forEach(res.data.items,function(item,key) {
                                item["image"] = item.image.replace(/upload/g,"upload/w_200");
                                items[key] = item;
                            });
                            $scope.newItems = items;
                        }
                    });
                }
                function scroll($scope){

                    $scope.winnerScrollTop = 264;
                    var coverHeight = 264;
                    var totalHeight = $(".scroll-winner").height();
                    var duration = $scope.winners.length*3200;
                    $(".scroll-winner").animate({
                        top:-totalHeight
                    },{
                        duration : duration,
                        easing : "linear",
                        complete : function(){
                            $(".scroll-winner").css("top",coverHeight+"px");
                            scroll($scope);
                        }
                    })
                }
            }]
    );

