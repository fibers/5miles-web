window.onload = function() {
    function parseURL(url) {
        var a =  document.createElement('a');
        a.href = url;
        return {
            source: url,
            protocol: a.protocol.replace(':',''),
            host: a.hostname,
            port: a.port,
            query: a.search,
            params: (function(){
                var ret = {},
                    seg = a.search.replace(/^\?/,'').split('&'),
                    len = seg.length, i = 0, s;
                for (;i<len;i++) {
                    if (!seg[i]) { continue; }
                    s = seg[i].split('=');
                    ret[s[0]] = s[1];
                }
                return ret;
            })(),
            file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
            hash: a.hash.replace('#',''),
            path: a.pathname.replace(/^([^\/])/,'/$1'),
            relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
            segments: a.pathname.replace(/^\//,'').split('/')
        };
    }
    var submitObj = {"type":"","method":"","action":""};
    $(".ios-btn-back").bind("click", function(event) {
        $(this).addClass("ios-load-show");
        if ($(this).hasClass("ios-open-back")) {
            $(this).addClass("ios-close-back");
            $(this).removeClass("ios-open-back");
            $(this).data("action","0");
        } else {
            $(this).addClass("ios-open-back");
            $(this).removeClass("ios-close-back");
            $(this).data("action","1");
        }
        setNoti($(this));
    });
    function setNoti(obj) {
        submitObj["type"] = obj.data("type") + "";
        submitObj["method"] = obj.data("method") + "";
        submitObj["action"] = obj.data("action") + "";
        var token = parseURL(window.location + "")["params"]["user_token"];
        $.post("/setNotification", {submit_args:submitObj,user_token:token},
            function(data) {
                if(data.status == 200 ){
                    obj.removeClass("ios-load-show");
                } else {
                }
            });
    }
}