(function(window, document) {
    var fm = {
        _browser : {
            versions:function(){
                var u = navigator.userAgent, app = navigator.appVersion;
                return {         //移动终端浏览器版本信息
                    trident: u.indexOf('Trident') > -1, //IE内核
                    presto: u.indexOf('Presto') > -1, //opera内核
                    webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                    gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                    mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
                    ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                    android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
                    iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
                    iPad: u.indexOf('iPad') > -1, //是否iPad
                    webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
                };
            }(),
            language:(navigator.browserLanguage || navigator.language).toLowerCase()
        },
        mobile: function() {
            return fm._browser.versions.mobile;
        },
        ios: function() {
            return fm._browser.versions.ios;
        },
        android: function() {
            return fm._browser.versions.ios;
        },
        _addFrame : function() {
            var frame = document.createElement('iframe');
            frame.style.display = "none";
            frame.src = "";
            document.body.appendChild(frame);
            fm._frame = frame;
        },
        _init : function() {
            fm._addFrame();
            console.log("mobile:" + fm.mobile() + "  ios:" + fm.ios() + "  android:" + fm.android());
        },
        /**
         * the interfaces of call native
         * @param catId
         * @param title
         */
        openSearch : function(catId,title) {
            var url = "fivemiles://search-cat?cat=" + catId + "&title=" + encodeURIComponent(title);
            fm._frame.src = url;
        },
        loadShow : function() {

        },
        loadHide : function() {

        }
    }
    fm._init();
    window.fm = fm;
})(this, this.document);
