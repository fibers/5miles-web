
var _thumbScale = function(input,size,face) {
    if(input == null)return input;
    if (!size) {
        size = 0.5;
    }
    if(face){
        return input.replace(/upload/, 'upload/w_' + size + ',f_auto,h_' + size + ',c_thumb,g_face').replace(/http:/, 'https:');
    }else{
        return input.replace(/upload/, 'upload/w_' + size + ",f_auto").replace(/http:/, 'https:');
    }

};
window.onload = function() {
    function parseURL(url) {
        var a =  document.createElement('a');
        a.href = url;
        return {
            source: url,
            protocol: a.protocol.replace(':',''),
            host: a.hostname,
            port: a.port,
            query: a.search,
            params: (function(){
                var ret = {},
                    seg = a.search.replace(/^\?/,'').split('&'),
                    len = seg.length, i = 0, s;
                for (;i<len;i++) {
                    if (!seg[i]) { continue; }
                    s = seg[i].split('=');
                    ret[s[0]] = s[1];
                }
                return ret;
            })(),
            file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
            hash: a.hash.replace('#',''),
            path: a.pathname.replace(/^([^\/])/,'/$1'),
            relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
            segments: a.pathname.replace(/^\//,'').split('/')
        };
    }

    function setNoti(obj) {
        submitObj["type"] = obj.data("type") + "";
        submitObj["method"] = obj.data("method") + "";
        submitObj["action"] = obj.data("action") + "";
        var token = parseURL(window.location + "")["params"]["user_token"];
        $.post("/setNotification", {submit_args:submitObj,user_token:token},
            function(data) {
                if(data.status == 200 ){
                    obj.removeClass("ios-load-show");
                } else {
                }
            });
    }

    var totalheight = 0;
    function loadData(){
        totalheight = parseFloat($(window).height()) + parseFloat($(window).scrollTop());

        if ($(document).height() <= totalheight) {  // 说明滚动条已达底部
            /*这里使用Ajax加载更多内容*/
            var container = $("#container"); // 加载容器
            var data = {}; // 查询参数

            var str = "";
            var container = $("#items");
            $.post("/item/getHomeItems", {},
                function(data) {
                    if(data && data.objects){
                        for (var i = 0; i < data.objects.length;i++) {
                            var item =  data.objects[i];
                            str += '<ul class="item_block"><li>';
                            str += '<img class="item_img" src="';
                            str += _thumbScale(item.images[0].imageLink,200);
                            str += '"/>';
                            str += '</li><li><p class="item_title">';
                            str += item.title;
                            str += '</p></li></ul>';
                        }
                        $(str).appendTo(container);
                    } else {
                    }
                });
        }
    }
    $(window).scroll( function() {
//        loadData();
    });
}