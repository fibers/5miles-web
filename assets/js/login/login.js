angular.module("app")
    .controller('LoginCtrl',
        ['$rootScope', '$scope','$http','$location',
            function($rootScope, $scope, $http) {
                var loc = window.location + "";
                if (loc.indexOf("param=sign") > 0) {
                    $scope.showLoginFlag = false;
                } else {
                    $scope.showLoginFlag = true;
                }
                $scope.changeLoginPage = function(event, pageValue) {
                    if (pageValue == "login") {
                        $scope.showLoginFlag = true;
                    } else {
                        $scope.showLoginFlag = !$scope.showLoginFlag;
                    }
                }
                $scope.showLogin = function(event) {
                    $scope.loginnow();//ga
                    $scope.changeLoginPage(event,'login');
                }
                $scope.showSignUp = function(event) {
                    $scope.signupnow();//ga
                    $scope.changeLoginPage(event,'sign');
                }
                //get the callback url
                function parseURL(url) {
                    var a =  document.createElement('a');
                    a.href = url;
                    return {
                        source: url,
                        protocol: a.protocol.replace(':',''),
                        host: a.hostname,
                        port: a.port,
                        query: a.search,
                        params: (function(){
                            var ret = {},
                                seg = a.search.replace(/^\?/,'').split('&'),
                                len = seg.length, i = 0, s;
                            for (;i<len;i++) {
                                if (!seg[i]) { continue; }
                                s = seg[i].split('=');
                                ret[s[0]] = s[1];
                            }
                            return ret;
                        })(),
                        file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
                        hash: a.hash.replace('#',''),
                        path: a.pathname.replace(/^([^\/])/,'/$1'),
                        relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
                        segments: a.pathname.replace(/^\//,'').split('/')
                    };
                }

                var _loginBackUrl = "/item/uploadView";
                if (loc.indexOf("loginShow/?back=") > 0) {
                    var arr = parseURL(loc)["params"]["back"];
                    _loginBackUrl = arr;
                }
                // do login
                $scope.validateLogin = {};
                $scope._loginValidEmail = function() {//email validate
                    $scope.validateLogin["email"] = false;
                    $scope.validateLogin["emailNot"] = false;
                    var value = $("#emailValue").val();
                    if (value == "") {
                        $scope.validateLogin["email"] = true;
                        return false;
                    } else if (!$scope._validateEmail(value)) {
                        $scope.validateLogin["emailNot"] = true;
                        return false;
                    } else  {
                        return true;
                    }
                }
                $scope._loginValidPwd = function() {//pwd validate
                    $scope.validateLogin["pwd"] = false;
                    $scope.validateLogin["pwdNot"] = false;
                    var value = $("#passValue").val();
                    if (value == "") {
                        $scope.validateLogin["pwd"] = true;
                        return false;
                    } else if (value.length < 6) {
                        $scope.validateLogin["pwdNot"] = true;
                        return false;
                    } else  {
                        return true;
                    }
                }
                function _afterLogin(res,redirect) {
                    if ($.cookie) {
                        var header = {
                            "mail":res.data.email,
                            "nick":res.data.nickname,
                            "portrait":res.data.portrait,
                            "userToken":res.data.token,
                            "userId":res.data.id,
                            "fb_user_id":res.data.fb_user_id,
                            "new_id":res.data.new_id,
                            "verified":res.data.verified
                        }
                        $.cookie("5miles_head",JSON.stringify(header),{"path":"/","expires":730});
                    }
                    if (redirect) {
                        window.location.href = redirect;
                    }
                }
                function _checkLogin() {
                    if ($.cookie) {
                        var appStr = $.cookie("5miles_head");
                        if (appStr) {
                            appStr = JSON.parse(appStr);
                            if (appStr.userId && appStr.userToken) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
                // after login do the item upload
                $scope._doUpload = function() {
                    var appStr = $.cookie("5miles_item");
                    var param = {};
                    if (appStr) {
                        param = JSON.parse(appStr);
                        // loading icon
                        $scope.upload_load_flag = true;
                        // not login status set the data to cookie
                        if (_checkLogin()) {
                            // post the item
                            $http.post("/item/uploadFile", param).then(function(res) {
                                if (res.data && res.data.new_id) {
                                    // delete the temp item
                                    $.removeCookie("5miles_item",{path:"/"});
                                    window.location.href = "/item/" + res.data.new_id;
                                } else {
                                    $scope.login_load_flag = false;
                                    $scope.validateTip = res.data.ERROR;
                                }
                            });
                        }
                    }

                }
                $scope.doLogin = function(event) {
                    //ga
                    $scope.signin();
                    //reset the tip
                    $scope.validateLogin = {};
                    //email
                    if (!$scope._loginValidEmail()) {
                        return false;
                    };
                    //password
                    if (!$scope._loginValidPwd()) {
                        return false;
                    };

                    var param = {
                        "email" : $("#emailValue").val(),
                        "password" : $("#passValue").val()
                    }
                    $scope.login_load_flag = true;
                    $scope.validateTip = "";
                    $http.post("/login", param).then(function(res) {
                        if (res.data && res.data.token) {
                            // ga
                            $scope.signin_ok();
                            // do upload
                            if ($scope.checkTempItem()) {
                                _afterLogin(res);
                                $scope._doUpload();
                            } else {
                                _afterLogin(res,_loginBackUrl);
                            }
                        } else {
                            $scope.login_load_flag = false;
                            $scope.validateTip = res.data.msg;
                        }
                    });
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                // keypress
                $scope.doLoginKeyPress = function(event) {
                    if (event.keyCode == 13) {
                        $scope.doLogin(event);
                    }
                };
                $scope.doSignKeyPress = function(event) {
                    if (event.keyCode == 13) {
                        $scope.doSignUp(event);
                    }
                };
                // register
                $scope.sign_passValueConfirm = "";

                $scope.sign_validateTip = "";
                $scope._validateEmail = function(str) {
                    var reg=/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                    if( reg.test(str) ){
                        return true;
                    }else{
                        return false;
                    }
                }
                $scope._validateNickName = function(str) {
                    var re=/[\\:*;?"'$<>|]/;
                    if(str.match(re)){
                        return false;
                    }
                    return true;
                }
                //
                $scope.validateSign = {};
                $scope._signValidNick = function() {//nick validate
                    $scope.validateSign["nick"] = false;
                    $scope.validateSign["nickNot"] = false;
                    var value = $("#sign_nickName").val();
                    if (value == "") {
                        $scope.validateSign["nick"] = true;
                        return false;
                    } else if (!$scope._validateNickName(value)) {
                        $scope.validateSign["nickNot"] = true;
                        return false;
                    } else  {
                        return true;
                    }
                }
                $scope._signValidEmail = function() {//email validate
                    $scope.validateSign["email"] = false;
                    $scope.validateSign["emailNot"] = false;
                    var value = $("#sign_emailValue").val();
                    if (value == "" || value.length == 0) {
                        $scope.validateSign["email"] = true;
                        return false;
                    } else if (!$scope._validateEmail(value)) {
                        $scope.validateSign["emailNot"] = true;
                        return false;
                    } else  {
                        return true;
                    }
                }
                $scope._signValidPwd = function() {//email validate
                    $scope.validateSign["pwd"] = false;
                    $scope.validateSign["pwdNot"] = false;
                    var value = $("#sign_passValue").val();
                    if (value == "") {
                        $scope.validateSign["pwd"] = true;
                        return false;
                    } else if (value.length < 6) {
                        $scope.validateSign["pwdNot"] = true;
                        return false;
                    } else  {
                        return true;
                    }
                }
                $scope.checkTempItem = function() {
                    if ($.cookie) {
                        var appStr = $.cookie("5miles_item");
                        if (appStr) {
                            appStr = JSON.parse(appStr);
                            if (appStr.title) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
                $scope.doSignUp = function(event) {
                    //ga
                    $scope.signup();
                    //nickName
                    if (!$scope._signValidNick()) {
                        return false;
                    };
                    //email
                    if (!$scope._signValidEmail()) {
                        return false;
                    };
                    //password
                    if (!$scope._signValidPwd()) {
                        return false;
                    };

                    $scope.validateSign = {};
                    var timezone = jstz.determine();
                    var param = {
                        "email" : $("#sign_emailValue").val(),
                        "password" : $("#sign_passValue").val(),
                        "nickname" : $("#sign_nickName").val(),
                        "timezone" : timezone.name()
                    }

                    // judge if have the temp upload item
                    if ($scope.checkTempItem()) {
                        var appStr = $.cookie("5miles_item");
                        if (appStr) {
                            appStr = JSON.parse(appStr);
                            param = $.extend(param,appStr);
                        }
                        $scope.signUp_load_flag = true;
                        $http.post("/@login/signUpAndUpload", param).then(function(res) {
                            if (res.data && res.data.ERROR) {
                                $scope.signUp_load_flag = false;
                                alert(res.data.ERROR);
                            } else if (res.data) {
                                //ga
                                $scope.signup_ok();
                                // set the track page
                                $("#sign_ok_frame").attr("src","/ga_signOK");
                                _afterLogin(res,"/validate/phone");
                            }
                        });
                    } else {
                        $scope.signUp_load_flag = true;
                        $http.post("/signUp", param).then(function(res) {
                            if (res.data && res.data.token) {
                                //ga
                                $scope.signup_ok();
                                // set the track page
                                $("#sign_ok_frame").attr("src","/ga_signOK");
                                _afterLogin(res,_loginBackUrl);
                            } else if (res.data) {
                                $scope.signUp_load_flag = false;
                                alert(res.data.msg);
                            }
                        });
                    }
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                // facebook login
                $scope.loginFacebook = function() {
                    // ga
                    if ($scope.showLoginFlag == true) {
                        $scope.signinwithfacebook();
                    } else {
                        $scope.registerwithfacebook();
                    }
                    if (!window.fInitFlag) {
//                        alert("Facebook can't init!");
                        return false;
                    }
                    FB.login(function(response) {
                        if (response.authResponse) {
                            console.log('Welcome!  Fetching your information.... ' +  response);
                            var token = response.authResponse.accessToken;
                            FB.api('/me', function(response) {
//                                console.log('Good to see you, ' + response + '.');
                                if (response.name) {
                                    // do fivemiles login
                                    var headImg = "https://graph.facebook.com/" + response.id + "/picture?type=large";
                                    var timezone = jstz.determine();
                                    var param = {
                                        "fb_userid" : response.id,
                                        "fb_username" : response.name,
                                        "fb_headimage" : headImg,
                                        "fb_token":token,
                                        "fb_email":response.email,
                                        "timezone" : timezone.name()
                                    }
                                    $.post('/loginFacebook', param, function(res) {
                                        if (res && res.status == 400) {
                                            alert(res.msg);
                                        } else if (res.token) {
                                            // ga
                                            if ($scope.showLoginFlag == true) {
                                                $scope.signinwithfacebook_ok();
                                            } else {
                                                $scope.registerwithfacebook_ok();
                                            }

                                            res.data = {"email":res.email,"nickname":res.nickname,
                                                "portrait":res.portrait,"token":res.token,"id":res.id,"fb_user_id":res.fb_user_id};
                                            // do upload
                                            if ($scope.checkTempItem()) {
                                                _afterLogin(res);
                                                $scope._doUpload();
                                            } else {
                                                _afterLogin(res,_loginBackUrl);
                                            }
                                        }
                                    });
                                }
                            });
                        } else {
                            console.log('User cancelled login or did not fully authorize.');
                        }
                    },{scope: 'email,public_profile'});
                }
                $scope.registerwithfacebook = function(event) {
                    ga('send', 'event', 'register_view', 'registerwithfacebook');
                };
                $scope.registerwithfacebook_ok = function(event) {
                    ga('send', 'event', 'register_view', 'registerwithfacebook_ok');
                };
                $scope.signup = function(event) {
                    ga('send', 'event', 'register_view', 'signup');
                };
                $scope.signup_ok = function(event) {
                    ga('send', 'event', 'register_view', 'signup_ok');
                };
                $scope.loginnow = function(event) {
                    ga('send', 'event', 'register_view', 'loginnow');
                };

                $scope.signinwithfacebook = function(event) {
                    ga('send', 'event', 'signin_view', 'signinwithfacebook');
                };
                $scope.signinwithfacebook_ok = function(event) {
                    ga('send', 'event', 'signin_view', 'signinwithfacebook_ok');
                };
                $scope.signin = function(event) {
                    ga('send', 'event', 'signin_view', 'signin');
                };
                $scope.signin_ok = function(event) {
                    ga('send', 'event', 'signin_view', 'signin_ok');
                };
                $scope.signupnow = function(event) {
                    ga('send', 'event', 'signin_view', 'signupnow');
                };
                $scope.forgotpassword = function(event) {
                    ga('send', 'event', 'signin_view', 'forgotpassword');
                };
            }]
    )
    .controller('ValidatePhoneCtrl',
        ['$rootScope', '$scope','$http','cookie',
            function($rootScope, $scope, $http,cookie) {
                $scope.pre_number = "+1";
                $scope.vb_item_verify_tip = "";
                var tip = "Send Passcode";
                $scope.sendPassValue = tip;
                function filterCountryCode(str) {
                    var arr2 = str.split("(+");
                    if (arr2[1]) {
                        var arr = arr2[1].split(")");
                        return  "+" + arr[0];
                    }
                    return null;
                }
                function checkLogin() {
                    if ($.cookie) {
                        var appStr = $.cookie("5miles_head");
                        if (appStr) {
                            appStr = JSON.parse(appStr);
                            if (appStr.userId && appStr.userToken) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
                function getPhoneNumber() {
                    var countryCodeStr = $("#vb_country option:selected").html();
                    var country = filterCountryCode(countryCodeStr);
                    var phone = $("#vb_phone_number").val();
                    if (phone.length == 0 || country.length == 0) {
                        return null;
                    } else {
                        return country + phone;
                    }
                }
                $scope._validatePhoneNumber = function(str) {
                    var reg=/^1[3|4|5|8][0-9]\d{4,8}$/;
                    if(reg.test(str) ){
                        return true;
                    } else{
                        return false;
                    }
                }
                $scope.vbCountryChange = function(event) {
                    var countryCodeStr = $("#vb_country option:selected").html();
                    var country = filterCountryCode(countryCodeStr);
                    $scope.$apply(function() {
                        $scope.pre_number = country;
                    });
                }
                $("#vb_country").bind("change", function() {
                    $scope.vbCountryChange();
                });
                // get the pass code
                $scope.sendPassCode = function() {
                    var phone = getPhoneNumber();
                    if (phone == null) {
                        return false;
                    }
                    if ($scope.passcode_load_flag) {
                        return false;
                    }
                    if (!checkLogin()) {
                        alert("Please sign in!");
                        return false;
                    }
                    // check the phone number
//                    if (!$scope._validatePhoneNumber($("#vb_phone_number").val())) {
//                        return false;
//                    }
                    var param = {
                        "phone" : phone
                    }
                    $scope.passcode_load_flag = true;
                    var passValue = "Resend in ";
                    var sencondLimit = 60;
                    $scope.sendPassValue = passValue + sencondLimit + "s";
                    var limit = setInterval(function() {
                        sencondLimit--;
                        if (sencondLimit == 0) {
                            $scope.passcode_load_flag = false;
                            $scope.sendPassValue = tip;
                            $scope.$apply(function() {
                                $scope.sendPassValue = "Send Passcode";
                            });
                            clearInterval(limit);
                        } else {
                            $scope.$apply(function() {
                                $scope.sendPassValue = passValue + sencondLimit + "s";
                            });
                        }
                    },1000);
                    $http.post("/@validate/send_passcode", param).then(function(res) {
                        if (res.data && res.data.ERROR) {
                            $scope.signUp_load_flag = false;
                            alert(res.data.ERROR);
                        } else if (res.data) {
                            // set the track page
                            if (res.data.message == "OK") {

                            } else {
                                clearInterval(limit);
                                $scope.passcode_load_flag = false;
                                $scope.sendPassValue = "Send Passcode";
                                alert(res.data.message);
                            }
                        }
                    });
                    $scope.sendpasscode();
                }
                // validate both the phone number and pass code
                $scope.verifyPassCode = function() {
                    var phone = getPhoneNumber();
                    if (phone == null) {
                        return false;
                    }
                    var passCode = $("#vi_item_passCode").val();
                    if (passCode.length == 0) {
                        return false;
                    }
                    if (!checkLogin()) {
                        alert("Please sign in!");
                        return false;
                    }
                    if ($scope.vb_item_verify_load) {
                        return false;
                    }
                    var param = {
                        "phone" : phone,
                        "passCode" : passCode
                    }
                    $scope.vb_item_verify_tip = "";
                    $scope.vb_item_verify_load = true;

                    $http.post("/@validate/verify_passcode", param).then(function(res) {
                        if (res.data && res.data.ERROR) {
                            alert(res.data.ERROR);
                        } else if (res.data) {
                            // set the track page
                            //Thanks for verifying your phone number
                            if (res.data.message.indexOf("verifying") > 0) {//validate ok
                                // set the validate flag
                                cookie.set("5miles_head","verified",true);
                                window.location.href = "/item/" + $scope.goItemId + "/?verified=phone";
                            } else {
                                $scope.vb_item_verify_tip = res.data.message;
                            }
                        }
                        $scope.vb_item_verify_load = false;
                    });
                    $scope.confirmpasscode();
                };
                // get item id
                $scope.initItemId = function() {
                    if ($.cookie) {
                        var appStr = $.cookie("5miles_head");
                        if (appStr) {
                            appStr = JSON.parse(appStr);
                            $scope.goItemId = appStr.new_id;
                        }
                    }
                    // delete the temp item
                    $.removeCookie("5miles_item",{path:"/"});
                }
                $scope.initItemId();
                $scope.sendpasscode = function(event) {
                    ga('send', 'event', 'phone_view', 'sendpasscode');
                }
                $scope.confirmpasscode = function() {
                    ga('send', 'event', 'phone_view', 'confirmpasscode');
                }
                $scope.skiptoitem = function() {
                    ga('send', 'event', 'phone_view', 'skiptoitem');
                }
            }]
    );

