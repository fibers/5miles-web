angular.module("app")
    .controller('HomeController',
        ['$rootScope', '$scope', '$http','$timeout',
            function($rootScope, $scope, $http, $timeout) {
                //gleam plugin
                $scope.judgeGleam = function() {
                    var showFlag = $.cookie("gleam_close_flag");
                    if (!showFlag) {
                        $scope.pcGleamShow = true;
                    }
                };
                $scope.judgeGleam();
                $scope.openGleam = function(event) {
                    window.location.href = "/WIN";
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                $scope.closeGleam = function(event) {
                    var date = new Date();
                    date.setTime(date.getTime()+(7*24*60*60*1000));
                    $.cookie("gleam_close_flag","true",{"expires":date});
                    $scope.pcGleamShow = false;
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }

                $scope.homeAppleStore = function(event) {
                    if ($scope.nowPage && $scope.nowPage == "SELL") {
                        ga('send', 'event', 'sell_view', 'sell_appstore');
                    } else {
                        ga('send', 'event', 'home_view', 'home_appstore');
                    }
                }
                $scope.homeGoogleStore = function(event) {
                    if ($scope.nowPage && $scope.nowPage == "SELL") {
                        ga('send', 'event', 'home_view', 'sell_googleplay');
                    } else {
                        ga('send', 'event', 'home_view', 'home_googleplay');
                    }
                }
                $scope.nowPage = "HOME";
            }])
    .controller('SearchController',
        ['$rootScope', '$scope', '$http','$timeout','parseURL','categoryData',
            function($rootScope, $scope, $http, $timeout, parseURL,categoryData) {
                var obj = categoryData;
                var keyword = parseURL.getParam(window.location,"keyword");
                var category = parseURL.getParam(window.location,"category");
                if (keyword && keyword.length > 0) {
                    $scope.nav_keyword = "\"" + keyword + "\"";
                }
                if (category && category.length > 0) {
                    $scope.nav_keyword = "\"" + obj[category].title + "\"";
                }
                $scope.nowPage = "SEARCH";
            }])
    .controller('WaterfallController',
        ['$rootScope', '$scope', '$http','$timeout','parseURL','parseTitle','categoryData','$q','cookie',
            function($rootScope, $scope, $http, $timeout,parseURL,parseTitle,categoryData,$q,cookie) {
                var cateData = categoryData;
                var keyword = parseURL.getParam(window.location,"keyword");
                if (keyword && keyword.length > 0) {
                    $scope.searchKeyword = keyword;
                }
                var category = parseURL.getParam(window.location,"category");
                if (category && category.length > 0) {
                    $scope.category = category;
                }
                //filter
                $scope.distanceShow =  false;
                $scope.orderShow =  false;
                $scope.distanceSelect = "Distance";
                $scope.sortSelect = "Sort Order";
                //get the screen size
                var screenSize = jQuery(window).width();
                var wid = 256;
                if ((310 < screenSize) && (screenSize < 768)) {
                    wid = Math.floor((screenSize)/2 - 9);
                } else if ((769 < screenSize) && (screenSize < 1079)) {
                    wid = Math.floor(screenSize/3) - 9;
                }
                function filter(data) {
                    var res = [];
                    angular.forEach(data, function(item,key) {
                        var image = item.images[0];
                        var imageWidth = image.width;
                        var imageHeight = image.height;
                        var hei = Math.floor(imageHeight*(wid/imageWidth));
                        var place = "";
                        if (item.city) {
                            place += item.city;
                        }
                        if (item.region) {
                            place += ", ";
                            place += item.region;
                        }
                        if (place.length == 0) {
                            place = item.distance;
                        }
                        var preTitle = cateData[item.cat_id] ? cateData[item.cat_id].title : "";
                        var idParse = parseTitle.getParse(preTitle + " " + item.title);
                        var temp = {
                            "id":item.id,
                            "title":item.title,
                            "idParse":idParse,
                            "local_price":item.local_price,
                            "original_price":item.original_price || "",
                            "distance":item.distance,
                            "state":item.state,
                            "place":place,
                            "is_new":item.is_new,
                            "owner":{
                                "portrait":item.owner.portrait,
                                "nickname":item.owner.nickname
                            },
                            "images":{
                                "imageLink":image.imageLink,
                                "width":wid,
                                "height":hei
                            },
                            "hide":true
                        };
                        res.push(temp);
                    });
                    return res;
                }
                var saveItemsToLoc = function(key,items) {
                    localStorage.setItem(key, items);
                }
                var homeItemCallback = function(res) {// the callback for show city and initialize the waterfall
                    var deferred = $q.defer();
                    if (res.data) {
                        if (res.data.STATE == "401") {
                            window.location.href = window.location.href;
                        } else {
                            if ($scope.list_more_load_flag) {
                                var fData = filter(res.data.objects);
                                $scope.homeItems = $scope.homeItems.concat(fData);
                                // set hash && save the items to local
                                if ($scope.nowPage == "HOME") {
                                    var hash = window.location.hash;
                                    if (hash == "") {
                                        history.replaceState(undefined, undefined, window.location + "#items");//set the back to the #items
                                    }
                                    var obj = {"url":"#items","meta": res.data.meta,"items":$scope.homeItems,
                                        "parCity":$scope.$parent.home_city, "itemCity":$scope.waterCon_city};
                                    saveItemsToLoc("recent-items",JSON.stringify(obj));
                                } else if ($scope.nowPage == "SEARCH") {
                                    var hash = window.location.hash;
                                    if (hash == "") {
                                        history.replaceState(undefined, undefined, window.location + "#search_items");
                                    }
                                    var obj = {"url":"#search_items","meta": res.data.meta,"items":$scope.homeItems,
                                        "parCity":$scope.$parent.home_city, "itemCity":$scope.waterCon_city};
                                    saveItemsToLoc("recent-items-search",JSON.stringify(obj));
                                }
                            } else  {
                                $scope.homeItems = filter(res.data.objects);
                                $scope.homeItemsLoc = res.data.loc;

                                $scope.waterCon_city = "Recent items near ";
                                if (res.data.loc && res.data.loc.city) {
                                    $scope.$parent.home_city = "in " + res.data.loc.city;
                                    var inCity = res.data.loc.city + ", " + res.data.loc.country;
                                    $("#pac-input").val(inCity);
                                } else if (res.address) {
                                    $scope.$parent.home_city = "in " + res.address.split(",")[0];
                                } else {
                                    $scope.waterCon_city = "New items near you.";
                                }
                            }

                            $scope.myMeta = res.data.meta;
                            if ($scope.myMeta && $scope.myMeta.next == null) {
                                $scope.more_block_flag = false;
                            }
                            loaded();
                            deferred.resolve();
                        }
                    } else {
                        deferred.reject();
                        alert("error");
                    }
                    return deferred.promise;

                };
                // get the address for display on page
                $scope._getAddressFromCookie = function() {
                    var city = cookie.get("5__location","locality");
                    var state = cookie.get("5__location","administrative_area_level_1");
                    var country = cookie.get("5__location","country");
                    var address = "";
                    if ((city == undefined) || (city && (city == state))) {
                        if (state == undefined) {
                            address = country;
                        } else {
                            address = state + ", " + country;
                        }
                    } else if (city != undefined && state != undefined){
                        address = city + ", " + state;
                    }
                    return address;
                }
                // get the city and set to cookie、show city on page
                $scope._getAPICity = function(position,param) {
                    var GOOGLE_GEOCODE_URL = "http://maps.googleapis.com/maps/api/geocode/json?latlng=#lat#,#lng#&sensor=true";
                    var url = GOOGLE_GEOCODE_URL.replace(/#lat#/,position.coords.latitude).replace(/#lng#/,position.coords.longitude);

                    var googleGeo = $http.get(url,{timeout:5000});
                    $q.all([googleGeo]).then(function(results){
                        var position = results[0].data;
                        var address = position.results[0].address_components;
                        var adArray = {"lat":param['clientLat'],"lng":param['clientLon']};
                        for(var i in address){
                            if(address[i].types.indexOf("country")!=-1){
                                adArray["country"] = address[i].short_name;
                            } else if (address[i].types.indexOf("administrative_area_level_1")!=-1) {
                                adArray["administrative_area_level_1"] = address[i].short_name;
                            } else if(address[i].types.indexOf("locality")!=-1){
                                adArray["locality"] = address[i].short_name;
                            }
                        }
                        if ($.cookie) {
                            $.cookie("5__location",JSON.stringify(adArray),{"path":"/","expires":730});
                        }
                        if ($scope.navigator_flag) {// click the position icon
                            var href = window.location.href;
                            href = href.replace(/#items/,"").replace(/#search_items/,"");
                            window.location.href = href;
                        } else {// the first time to position select
                            var address = $scope._getAddressFromCookie();
                            $scope.$parent.home_city = "in " + address.split(",")[0];
                            if ($scope._cityShowDelay) {
                                $scope.waterCon_city = $scope.waterCon_city + "near " + address;
                            }
                        }
                    });
                }
                // package the navigator geo function
                $scope.__geoPosition = function(opts) {
                    var param = {};
                    param = $.extend(param, opts);
                    navigator.geolocation.getCurrentPosition(function(position){
                        // init the client position
                        param['clientLat'] = position.coords.latitude;
                        param['clientLon'] = position.coords.longitude;
                        $scope.positionInfo = position.coords;

                        if ($scope.navigator_flag) {// click the marker
                            $scope._getAPICity(position,param);
                        }
                    },function(err){
                        $http.post("/item/getHomeItems", param).then(homeItemCallback);
                    },{timeout:5000,maximumAge: 1000*60*60});
                }
                // according the position to get the items
                $scope.getItems = function() {
                    $scope.upload_list_more_load_flag = true;
                    var limit = $scope.itemNumber == undefined?40:$scope.itemNumber;
                    if (screenSize < 420) {
                        limit = $scope.itemNumber == undefined?15:$scope.itemNumber;
                        if ($scope.list_more_load_flag) {
                            var next = $scope.myMeta.next;
                            $scope.myMeta.next = next.replace(/limit=6/g, "limit=" + limit);
                        }
                    }
                    var param = {"limit":limit};
                    if ($scope.list_more_load_flag) {
                        param["next"] = $scope.myMeta.next;
                    }
                    //keyword
                    if ($scope.searchKeyword) {
                        param["keyword"] = $scope.searchKeyword;
                    }
                    if ($scope.category) {
                        param["category"] = $scope.category;
                    }
                    if ($scope.distance) {
                        param["distance"] = $scope.distance;
                    }
                    if ($scope.orderby) {
                        param["orderby"] = $scope.orderby;
                    }
                    if (cookie.get("5__location","lat") && !$scope.navigator_flag) {// have cookie location
                        param['clientLat'] = cookie.get("5__location","lat");
                        param['clientLon'] = cookie.get("5__location","lng");
                        $http.post("/item/getHomeItems", param).then(function(data) {
                            data.address = $scope._getAddressFromCookie();
                            return data;
                        }).then(homeItemCallback);
                    } else if ($scope.navigator_flag){
                        $scope.__geoPosition(param);
                    } else {
                        $http.post("/item/getHomeItems", param).then(homeItemCallback);
                    }
                }
                $scope._init = function () {
                    function locInit(key) {
                        var locObj = localStorage.getItem(key);
                        if (locObj) {
                            locObj = JSON.parse(locObj);
                            $scope.homeItems =  locObj.items;
                            $scope.myMeta = locObj.meta;
                            $scope.$parent.home_city = locObj.parCity;
                            $scope.waterCon_city = locObj.itemCity;
                            loaded();
                        } else {
                            $scope.getItems();// init
                            localStorage.removeItem(key);
                            window.location.hash = "";
                        }
                    }
                    function filterInit() {
                        var locObj = null;
                        if ($scope.nowPage == "HOME") {
                            locObj = $.cookie("5__home_filter");
                        } else if ($scope.nowPage == "SEARCH") {
                            locObj = $.cookie("5__search_filter");
                        }
                        if (locObj) {
                            locObj = JSON.parse(locObj);
                            $scope.distance =  locObj.distance;
                            if ($scope.distance && $scope.distance != undefined) {
                                $scope.distanceSelect = $("#waterCon_distance_list").find(".order_item[value='" + locObj.distance + "']").text();
                            }
                            $scope.orderby = locObj.order;
                            if ($scope.orderby && $scope.orderby != undefined) {
                                    $scope.sortSelect = $("#waterCon_order_list").find(".order_item[value='" + locObj.order + "']").text();
                            }
                        }
                    }
                    var hash = window.location.hash;
                    $scope.upload_list_more_load_flag = true;

                    if ($scope.nowPage == "HOME") {
                        if (hash == "#items") {
                            filterInit();
                            locInit("recent-items");
                        } else {
                            $scope.getItems();// init
                            localStorage.removeItem("recent-items");
                        }
                    } else if ($scope.nowPage == "SEARCH") {
                        if (hash == "#search_items") {
                            filterInit();
                            locInit("recent-items-search");
                        } else {
                            $scope.getItems();// init
                            localStorage.removeItem("recent-items-search");
                        }
                    } else {
                        $scope.getItems();// init
                    }
                }
                $scope._init();
                $scope.more_block_flag = true;// show or hide the load block
                function loaded() {// after loaded to initalize the waterfall
                    var container = document.querySelector('#container');
                    var msnry;
                    // initialize Masonry after all images have loaded
                    var fixed = 6;
                    var imgLoad = imagesLoaded( container, function() {
                        msnry = new Masonry( container ,{
                            // options
                            columnWidth: wid + fixed,
                            "isFitWidth": true,
                            itemSelector: '.waterItem'
                        });

                        $("#waterCon").css("height","auto");
                        $scope.$apply(function() {
                            $scope.upload_list_more_load_flag = false;
                            // footer set
                            footerFixed();
                        });
                    });
                }
                // click the position marker to get the navigator to load items
                $scope.navPosition = function() {
                    //ga
                    $scope.getlocation();
                    if ($scope.navigator_flag) {
                        return false;
                    }
                    $scope.navigator_flag = true;// flag the more load status
                    $scope.getItems();
                }

                // list more
                $scope.listMoreFlag = 0;
                $scope.listMore = function(event) {
                    if ($scope.upload_list_more_load_flag) {
                        return false;
                    }
                    $scope.upload_list_more_load_flag = true;
                    $scope.list_more_load_flag = true;// flag the more load status

                    $timeout(function() {
                        $scope.getItems();
                        if ($scope.listMoreFlag == 0) { // click
                            $scope._clickLoad();
                        } else {// auto
                            $scope._autoLoad();
                        }
                        $scope.listMoreFlag ++;
                    },200);
                }

                $scope.homeProduct = function(event) {
                    if ($scope.nowPage && $scope.nowPage == "SEARCH") {
                        if ($scope.category) {
                            ga('send', 'event', 'category_view', 'categoryproduct');
                        } else {
                            ga('send', 'event', 'search_view', 'search_product');
                        }
                    } else {
                        ga('send', 'event', 'home_view', 'home_product');
                    }
                }
                $scope.getlocation = function() {
                    if ($scope.nowPage && $scope.nowPage == "SEARCH") {
                        ga('send', 'event', 'search_view', 'getlocation');
                    } else {
                        ga('send', 'event', 'home_view', 'getlocation');
                    }
                }
                $scope.queryDistance = function(event) {
                    $scope.more_block_flag = true;
                    $scope.upload_list_more_load_flag = true;
                    $scope.list_more_load_flag = false;

                    $scope.homeItems = [];
                    $scope.myMeta = null;
                    $("#container").css("height","0px");
                    setTimeout(function() {
                        $scope.getItems();
                    },2000);
                }
                $scope.queryOrder = function(event) {
                    $scope.more_block_flag = true;
                    $scope.upload_list_more_load_flag = true;
                    $scope.list_more_load_flag = false;

                    $scope.homeItems = [];
                    $scope.myMeta = null;
                    $("#container").css("height","0px");
                    setTimeout(function() {
                        $scope.getItems();
                    },1000);

                }
                // filter
                $scope._filterSave = function() {
                    var obj = {"distance":$scope.distance,"order":$scope.orderby};
                    if ($scope.nowPage == "HOME") {
                        $.cookie("5__home_filter",JSON.stringify(obj),{"path":"/","expires":730});
                    } else if ($scope.nowPage == "SEARCH") {
                        $.cookie("5__search_filter",JSON.stringify(obj),{"path":"/","expires":730});
                    }
                }
                $scope.distanceFilter = function() {
                    $scope.orderShow = false;
                    var nowClass = event.target.className;
                    if (nowClass && nowClass.indexOf("order_item") >= 0) {
                        $scope.distance = $(event.target).attr("value");
                        var select = $(event.target).text();
                        $scope.distanceSelect = select;
                        $scope.distanceShow = false;
                        $scope._filterSave();
                        $scope.queryDistance();
                        $scope._homedistancerefine();
                    } else {
                        if ($scope.distanceShow == false) {
                            $scope._homedistanceon();
                        } else {
                            $scope._homedistanceoff();
                        }
                        $scope.distanceShow = !$scope.distanceShow;
                    }
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
                $scope.sortFilter = function(event) {
                    $scope.distanceShow =  false;
                    var nowClass = event.target.className;
                    if (nowClass && nowClass.indexOf("order_item") >= 0) {
                        $scope.orderby = $(event.target).attr("value");
                        if ($scope.orderby == 0) {//reset to default
                            $scope.orderby = null;
                        }
                        var select = $(event.target).text();
                        $scope.sortSelect = select;
                        $scope.orderShow = false;
                        var hash = window.location.hash;
                        if (hash == "#items") {
                            var loc = window.location.href;
                            loc = loc.replace(/#items/, '');
                            history.replaceState(undefined, undefined, loc);
                        } else if (hash == "#search_items") {
                            var loc = window.location.href;
                            loc = loc.replace(/#search_items/, '');
                            history.replaceState(undefined, undefined, loc);
                        }
                        $scope._filterSave();
                        $scope.queryOrder();

                        $scope._homesortorderrefine();
                    } else {
                        if ($scope.orderShow == false) {
                            $scope._homesortorderon();
                        } else {
                            $scope._homesortorderoff();
                        }
                        $scope.orderShow = !$scope.orderShow;
                    }
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
                $("body").bind("click", function(event) {
                    $scope.$apply(function() {
                        if ($scope.orderShow == true) {
                            $scope._homesortorderoff();
                        }
                        $scope.orderShow = false;
                        if ($scope.distanceShow == true) {
                            $scope._homedistanceoff();
                        }
                        $scope.distanceShow = false;
                    });
                });
                //window scroll
                $scope.waterItemTopFlag = false;
                var clientHei = jQuery(window).height() + 100;
                var compareHei = 150;
                var screenSize = jQuery(window).width();
                if (screenSize < 768) {
                    compareHei = 200;
                }
                $(window).on("scroll",function() {
                    // to the top
                    var scrollTop = $("body").scrollTop();
                    if (clientHei <  scrollTop) {
                        $scope.$apply(function() {
                            $scope.waterItemTopFlag = true;
                        });
                    } else {
                        $scope.$apply(function() {
                            $scope.waterItemTopFlag = false;
                        });
                    }
                    // auto loading
                    if($(document).height() - ($(window).height() + $(document).scrollTop()) < compareHei ){
                        if ($scope.listMoreFlag > 0) {
                            $(".waterItem_more_span").html("")
                            $scope.listMore();
                        }
                    }
                });
                $scope.top = function() {
                    $(window).scrollTop(0);
                    $scope._backToTop();
                }
                //GA
                $scope._nowGAPage = "home_view";
                if ($scope.nowPage && $scope.nowPage == "SEARCH") {
                    $scope._nowGAPage = "search_view";
                }
                $scope._homedistanceon = function(event) {
                    ga('send', 'event', $scope._nowGAPage, 'homedistanceon');
                }
                $scope._homedistancerefine = function(event) {
                    ga('send', 'event', $scope._nowGAPage, 'homedistancerefine');
                }
                $scope._homedistanceoff = function(event) {
                    ga('send', 'event', $scope._nowGAPage, 'homedistanceoff');
                }
                $scope._homesortorderon = function(event) {
                    ga('send', 'event', $scope._nowGAPage, 'homesortorderon');
                }
                $scope._homesortorderrefine = function(event) {
                    ga('send', 'event', $scope._nowGAPage, 'homesortorderrefine');
                }
                $scope._homesortorderoff = function(event) {
                    ga('send', 'event', $scope._nowGAPage, 'homesortorderoff');
                }

                $scope._clickLoad = function() {
                    var str = "bottom_load";
                    if ($scope._nowGAPage == "search_view") {
                        str = "searchloadmore";
                    }
                    ga('send', 'event', $scope._nowGAPage, str);
                }
                $scope._autoLoad = function() {
                    var str = "auto_load";
                    if ($scope._nowGAPage == "search_view") {
                        str = "searchloadmore_auto";
                    }
                    ga('send', 'event', $scope._nowGAPage, str);
                }
                $scope._backToTop = function() {
                    ga('send', 'event', $scope._nowGAPage, 'backtotop');
                }
            }])
    .controller('recommendCtl',
        ['$rootScope', '$scope', '$http','$timeout','parseURL',
            function($rootScope, $scope, $http, $timeout,parseURL) {
                //get the screen size

                var screenSize = jQuery(window).width();
                var wid = 256;
                if ((310 < screenSize) && (screenSize < 768)) {
                    wid = Math.floor((screenSize)/2 - 9);
                } else if ((769 < screenSize) && (screenSize < 1079)) {
                    wid = Math.floor(screenSize/3) - 9;
                }
                function loaded() {
                    //set image and waterItem_info width and height;
                    $(".waterItem").each(function(){
                        var img = $(this).find(".waterItemImg");
                        var info = $(this).find(".waterItem_info");
                        var width = img.data("width");
                        var height = img.data("height");
                        var hei = height*(wid/width);
                        img.css("height",hei).css("width",wid);
                        info.css("width",wid);
                    })
                    var container = document.querySelector('#container');
                    var msnry;
                    // initialize Masonry after all images have loaded
                    var fixed = 6;
                    imagesLoaded( container, function() {
                        msnry = new Masonry( container ,{
                            // options
                            columnWidth: wid + fixed,
                            "isFitWidth": true,
                            itemSelector: '.waterItem'
                        });
                        $(".waterItemInvisible").removeClass("waterItemInvisible");
                        $("#waterCon").css("height","auto");
                    });
                }
                loaded();

                $scope.recommendProduct = function(event) {
                    ga('send', 'event', 'recommend_view', 'recommendproduct');
                }
            }])
    .filter("_thumbFilter",function() {
        function _thumbFilter(input, size, mode) {
            var REGEXP_IMG = /(\/fivemiles\/image\/upload\/)(.*)$/i;
            var w = 50, h = 50;

            // default & max size is 100
            if (size > 0) {
                w = size;
                h = size;
            }

            // to get the @2x image
            w *= 2;
            h *= 2;
            mode = mode ? ',' + mode : '';

            return input.replace(/\/w_[^\/]*\//, '/').replace(REGEXP_IMG, '$1w_' + w + ',h_' + h + ',c_thumb' + mode + '/$2');
        };
        return function(input, size, mode) {

            return _thumbFilter(input, size, mode);
        }
    })


$(document).ready(function() {
    // the sellLayout use the other ga
    var nowPage = null;
    if ($("#sellLayout").length == 1) {
        nowPage = "SELL";
    }
    function gaSendLink() {
        if (nowPage && nowPage == "SELL") {
            ga('send', 'event', 'sell_view', 'sell_sendlink');
        } else {
            ga('send', 'event', 'home_view', 'home_sendlink');
        }
    }
    function smsSend() {
        if (nowPage && nowPage == "SELL") {
            ga('send', 'event', 'sell_view', 'sell_smssend');
        } else {
            ga('send', 'event', 'home_view', 'home_smssend');
        }
    }
    function emailSend() {
        if (nowPage && nowPage == "SELL") {
            ga('send', 'event', 'sell_view', 'sell_emailesend');
        } else {
            ga('send', 'event', 'home_view', 'home_emailesend');
        }
    }

    //send email and phone
    $('.btn-send').on('click', function(e) {
        function filterCountryCode(str) {
            var arr2 = str.split("(+");
            if (arr2[1]) {
                var arr = arr2[1].split(")");
                return ("+"+arr[0]);
            }
            return null;
        }
        var data, dest, input, type;
        input = $(this).parents('.form-group').find('input');
        dest = input.val();
        type = input.attr('id');
        data = {
            dest: dest,
            type: type
        };
        if (dest !== '' && type !== '') {
            //before process
            if (type == "phone") {
                $("#phoneTip").removeClass("send-tip-ok").html("");
                //validate
                var countryCodeStr = $("#sms_link_country option:selected").html();
                var countryCode = filterCountryCode(countryCodeStr);
                if (!countryCode) {
                    $("#phoneTip").html("Please select country code !");
                    return false;
                }
                var emailExp = /^[0-9]*$/;
                if(!emailExp.exec(dest)){//validate for phone
                    $("#phoneTip").html("Please check your number!");
                    return false;
                } else  {
                    $("#phoneTip").html("");
                }
                //prevent too much times click
                if ($("#phoneTip").data("phoneSendFlag") == "true") {
                    $("#phoneTip").html("Please try again after 1 minute !");
                    return true;
                } else {
                    $("#phoneTip").html("");
                    $("#phoneTip").data("phoneSendFlag","true");
                    setTimeout(function() {
                        $("#phoneTip").data("phoneSendFlag","false");
                        $("#phoneTip").html("");
                    }, 60000);
                }
                smsSend();
                data.dest = countryCode + data.dest;
            } else if (type == "email") {
                var mailpatrn = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!mailpatrn.exec(dest)){//validate for email
                    $("#emailTip").html("Please check your number!");
                    return false;
                } else  {
                    $("#emailTip").html("");
                }
                emailSend();
            }
            //phone || email ,send message and params
            $.post('/message', data, function(result) {
                if (result.success) {
                    input.val('');
                    $("#phoneTip").addClass("send-tip-ok").html("Link sent successfully!");
                } else {
                    alert(result.data)
                }
            });
        } else if (dest === '') {
            if (type === "phone") {
                return $("#phoneTip").html("Please input your phone number!");
            } else if (type === "email") {
                return $("#emailTip").html("Please input your email !");
            }
        }
    });

});

