/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

    /***************************************************************************
     *                                                                          *
     * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
     * etc. depending on your default view engine) your home page.              *
     *                                                                          *
     * (Alternatively, remove this and add an `index.html` file in your         *
     * `assets` directory)                                                      *
     *                                                                          *
     ***************************************************************************/

    '/': 'HomeController.home',
    '/recommend' : 'HomeController.recommend',
    '/recommend/:offset' : 'HomeController.recommend',
    '/card': 'HomeController.home',

    '/item/map/:lat/:lon': 'ItemController.showItemMap',
    '/item/uploadView': 'ItemController.showManageItem',
    'post /item/uploadFile': 'ItemController.upload',
    'post /item/getHomeItems': 'ItemController.getHomeItems',
    'post /item/getSearchItems': 'ItemController.getSearchItems',
    'get /item/:code/*': 'ItemController.showItem',
    'get /item/:code': 'ItemController.showItemRedirect',
    'post /like/:item_id' : 'ItemController.like',
    'post /unlike/:item_id' : 'ItemController.unlike',


    'post /@person/myItems': 'PersonController.getMyItems',
    'post /@person/distance': 'PersonController.personDis',
    'get /person/:user_id/*': 'PersonController.person',
    'get /person/:user_id': 'PersonController.personRedirect',
    'get /profile/:user_id': 'PersonController.profile',
    'post /@person/following': 'PersonController.following',
    'post /@person/follower': 'PersonController.follower',
    'post /@person/follow': 'PersonController.follow',
    'post /@person/unfollow': 'PersonController.unfollow',
    'post /@person/userLikes': 'PersonController.getMyLikes',
    'post /@person/myPurchases': 'PersonController.getMyPurchases',
    'get /personinfo/:user_id': 'PersonController.getPersonInfo',
    'get /likerlist/:prod_id': 'PersonController.getLikerList',
    'get /sellerItems/:user_id': 'PersonController.getSellerItems',
    'post /sellerItems': 'PersonController.getSellerItems',

    'get /sell' : 'SellController.sell',
    'get /sell-mb' : 'SellController.sellMb',

    '/loginShow': 'LoginController.loginShow',
    '/loginShow/*': 'LoginController.loginShow',
    '/validate/phone': 'LoginController.validatePhoneShow',
    'post /@validate/send_passcode': 'LoginController.sendPassCode',
    'post /@validate/verify_passcode': 'LoginController.verifyPassCode',
    'post /signUp': 'LoginController.signUp',
    'get /ga_signOK': 'LoginController.signOK',
    'get /ga_offerOK': 'LoginController.offerOK',
    'post /login': 'LoginController.login',
    'post /@login/signUpAndUpload': 'LoginController.signUpAndUpload',
    'post /loginFacebook': 'LoginController.loginFacebook',
    'post /loginOut': 'LoginController.loginOut',
    'get /zendesk_login': 'LoginController.zendesk_login',

    'get /new_messages': 'HomeController.newMessages',
    'get /my_message' : 'HomeController.myMessage',

    '/reset/:email': 'ResetPasswordController.resetPasswordView',
    '/reset': 'ResetPasswordController.resetEmailView',
    'post /resetpassword': 'ResetPasswordController.resetPassword',
    'post /resetemail': 'ResetPasswordController.resetEmail',
    'post /message': 'MessageController.message',
    'get /download': 'MessageController.download',
    'get /update': 'MessageController.download',
    'get /_message': 'MessageController.sendInnerMsg',
    'post /@messageserv': 'MessageController.messageServ',

    'get /userSubscriptions/:user_token': 'MailController.userSubscriptions',
    'post /unsubscribe': 'MailController.unsubscribe',
    'post /setNotification': 'AppController.setNotification',

    '/video/home': 'HomeController.video',
    '/WIN': 'HomeController.win',
    '/search': 'HomeController.search',
    '/search/*': 'HomeController.search',
    '/category-mb': 'CategoryController.categoryMb',

    'get /winning-grand-prize': 'HomeController.home',
    'get /grand-prize-info': 'PromotionController.getPromotionInfo',
    'get /grand-prize-tracking' : {
        view : 'promotion/tracking'
    },

    'get /download-tracking' : {
        view : 'downloadTracking'
    },
    'post /@mail/ask': 'mailController.sendAsk',
    'post /@mail/offer': 'mailController.sendOffer',

    'post /@feedback': 'FeedbackController.feedback',
    'post /reportItem/' : 'FeedbackController.reportItem',
    'post /reportSeller/' : 'FeedbackController.reportSeller',

    'get /verify_email/:emailCode' : 'RegistrationController.verifyEmail',

    'post /chrome/': "ChromeController.sendNotification",

    'get /sitemap.xml' : 'HomeController.siteMapXml',
    'get /sitemap' : 'HomeController.siteMapHTML',
    'get /value' : 'HomeController.home',
    'get /sale' : 'HomeController.home',
    'get /bargain' : 'HomeController.home',
    'get /DMN' : 'HomeController.home',
    'get /promo/:code' : 'HomeController.promo',
    'get /promo/' : 'HomeController.redirectHome',

    'get /zendesk/user' : 'ZendeskController.userList',
    'get /zendesk/create' : 'ZendeskController.createTicket',

    /**
     * Active
     */
    'get /cashcard' : 'ActiveController.cashcard',
    'get /bus' : 'ActiveController.bus',
    'get /metro' : 'ActiveController.metro'


    /***************************************************************************
     *                                                                          *
     * Custom routes here...                                                    *
     *                                                                          *
     *  If a request to a URL doesn't match any of the custom routes above, it  *
     * is matched against Sails route blueprints. See `config/blueprints.js`    *
     * for configuration options and examples.                                  *
     *                                                                          *
     ***************************************************************************/



};
