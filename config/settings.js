/**
 * Copywriters
 * (sails.config.copywriters)
 *
 * Some copywriters on the landing page.
 */
var protocol = "http://";
var testHostName = "54.64.120.187";
var officialHostName = "api.5milesapp.com";
var switchBtn = "inner";//api,test,inner
var officialHostNameInner = "internal-lb-api-server-internal-1271924753.ap-southeast-2.elb.amazonaws.com";
var testMailHost = "54.65.50.181";
var officialMailHost = "internal-lb-fmmc-internal-new-1882292952.ap-southeast-2.elb.amazonaws.com";

var hostName = officialHostName;//change the hostName for testing or developing
if (switchBtn == "test") {
    hostName = testHostName;
} else if (switchBtn == "inner") {
    hostName = officialHostNameInner;
}
var mailHostName = protocol + officialMailHost;
var previousAPI = protocol + hostName;
var apiToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjotMTI1MDZ9.yKUQNs1da_Z5fKAOly5faynkps8bU-kW1r1dorxDltc";
var testToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjotMTI1MDZ9.TWtuj7XLqWigIZOooqOzTwfuU5xrIUQSyosalSrcDic";
var defaultUsertoken = switchBtn == "test" ? testToken :apiToken;
var defaultUserid = "-12506";


module.exports.settings = {
    androidDownloadLink: 'http://m.onel.ink/d4d5c3f1',
    iosDownloadLink: 'http://m.onel.ink/87c352d4',
    secretKey: "358e5ac8-4a2d-4b3b-9f46-2743d0fd935c3bd6fae8-3700-4063-87df-9402bcb14aee952bc072-a624-4a4c-b31c-1d27ca806afc",    
    defaultUsertoken:defaultUsertoken,
    defaultUserid:defaultUserid,
    picCloudPrefix:"http://res.cloudinary.com/fivemiles/image/upload/",
    message: {
        title: '5miles app download link',
        content: 'Download the 5miles app here: https://5milesapp.com/download to open your pocket shop!',
        path:mailHostName + "/api/send_sms/"
    },
    mailgun: {
        apiKey: 'key-0581e4323e67d34e36d5f1441d2113bd',
        domain: 'mail.5milesapp.com'
    },
    nexmo: {
        hostname: 'rest.nexmo.com',
        path: '/sms/json',
        apiKey: '8cdf3d4b',
        apiSecret: 'fa62e0c3',
        from: '18324127075'
    },
    ses: {
        region: 'us-east-1',
        endpoint: 'email.us-east-1.amazonaws.com',
        accessKey: 'AKIAJ2SWT346HW7ZSW5A',
        secretKey: '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'
    },
    resetpwd: {
        hostname:hostName,
        path:'/api/v2/reset_password/',
        pathuri:previousAPI + '/api/v2/reset_password/',
        usertoken:defaultUsertoken,
        userid:defaultUserid,
        forgeturi:previousAPI + '/api/v2/forget_password/'
    },
    item: {
        hostname:hostName,
        uri: previousAPI + '/api/v2/item_detail/?item_id=',
        usertoken: defaultUsertoken,
        userid:defaultUserid,
        opts: {
            json: true
        },
        suggesturi: previousAPI + '/api/v2/suggest/',
        posturi: previousAPI + '/api/v2/post_item/',
        postpath: '/api/v2/post_item/',
        signuri: previousAPI + '/api/v2/sign_image_upload/',
        viewuri: previousAPI + '/api/v2/home/?',
        searchuri: previousAPI + '/api/v2/search/?',
        searchcategoryuri: previousAPI + '/api/v2/search/category/?',
        likeuri: previousAPI + '/api/v2/like/',
        unlikeuri: previousAPI + '/api/v2/unlike/',
        followuri: previousAPI + '/api/v2/follow/',
        unfollowuri: previousAPI + '/api/v2/unfollow/'
    },
    recommend:{
        mockIP : "174.36.122.91"
    },
    person: {
        userdetail: previousAPI + "/api/v2/user_detail/?user_id=",
        infouri : previousAPI + "/api/v2/me/",
        followinguri : previousAPI + "/api/v2/user_following/?user_id=",
        followeruri : previousAPI + "/api/v2/user_followers/?user_id=",
        followuri : previousAPI + "/api/v2/follow/",
        unfollowuri : previousAPI + "/api/v2/unfollow/",
        usertoken: defaultUsertoken,
        userid:defaultUserid,
        selluri: previousAPI + '/api/v2/user_sellings/',
        myselluri: previousAPI + '/api/v2/my_sellings/',
        likeruri: previousAPI + '/api/v2/item_likers/?item_id=',
        userlikesuri: previousAPI + '/api/v2/user_likes/',
        mypurchasesuri: previousAPI + '/api/v2/my_purchases/',
        offset:"0",
        limit:"20"
    },
    login: {
        uri: previousAPI + '/api/v2/login_email/',
        facebookuri: previousAPI + '/api/v2/login_facebook/',
        signuri: previousAPI + '/api/v2/signup_email/',
        signweburi: previousAPI + '/api/v2/web_signup_email/',
        sendpasscodeuri : previousAPI + '/api/v2/send_passcode/',
        verifypasscodeuri : previousAPI + '/api/v2/verify_passcode/'
    },
    category: {
        uri: previousAPI + '/api/v2/categories/',
        uri_new: previousAPI + '/api/v2/new_categories/',
        usertoken: defaultUsertoken,
        userid:defaultUserid,
    },
    feedback: {
        uri: previousAPI + '/api/v2/feedback/',
        reportItemUri : previousAPI + "/api/v2/report_bad_item/",
        reportSellerUri : previousAPI + "/api/v2/report_bad_user/",
        reportReasons: previousAPI + "/api/v2/report_reasons/"
    },
    mail: {
        subscribeuri: previousAPI + '/api/v2/subscribe/',
        uri: previousAPI + '/api/v2/bulk_subscribe/',
        userSubscriptions: previousAPI + '/api/v2/user_subscriptions/',
        sendmailuri : previousAPI + '/api/v2/send_mail/',// email chat
        sendDirectUri: mailHostName + '/api/send_direct_mail/',
        tmplName : "TMPL_DOWNLOAD_APP"
    },
    msg: {
        uri: previousAPI + '/api/v2/make_offer/',
        uri_test: previousAPI + '/api/v2/test_api/'
    },
    account: {
        uri: previousAPI + '/api/v2/deactive_account/'
    },
    promotion: {
        startTime: "2014-11-21 00:00",
        endTime: "2014-11-24 00:00",
        timeZone: "America/Los_Angeles",
        warmupView: "promotion/warmUp/",
        officialView: "promotion/official/",
        promotionItem: previousAPI + "/api/v2/promotion_items/",
        promotionRanking: previousAPI + "/api/v2/promotion_ranking/",
        giftCard: previousAPI + "/api/v2/promotion_red_package/"

    },
    //array[0]->android app store link; array[1]->apple app store link
    urlDownloadMap : {
        baseAndroid:"http://app.appsflyer.com/com.thirdrock.fivemiles?",
        baseIos:"http://app.appsflyer.com/id917554930?",
        "web":{
            "google":{
                "google_2cents": ["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=web&c=gg_2cents","http://app.appsflyer.com/id917554930?pid=web&c=gg_2cents"],
                "google_normal":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=web&c=gg","http://app.appsflyer.com/id917554930?pid=web&c=gg"]
            },
            "fb":{
                "fb":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=web&c=fb","http://app.appsflyer.com/id917554930?pid=web&c=fb"]
            }
        },
        "social":{
            "instagram":{
                "default":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=instagram","http://app.appsflyer.com/id917554930?pid=instagram"]
            }
        },
        "mv":{
            "cheap":{
                "seller nov": ["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=http://5milesapp.com/winning-grand-prize?utm_source=mv&utm_medium=cheap&utm_campaign=seller%20nov",
                    "http://app.appsflyer.com/id917554930?pid=http://5milesapp.com/winning-grand-prize?utm_source=mv&utm_medium=cheap&utm_campaign=seller%20nov"]
            }
        },
        "bf":{
            "adn":{
                "seller nov": ["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=http://5milesapp.com/winning-grand-prize?utm_source=bf&utm_medium=adn&utm_campaign=seller%20nov",
                    "http://app.appsflyer.com/id917554930?pid=http://5milesapp.com/winning-grand-prize?utm_source=bf&utm_medium=adn&utm_campaign=seller%20nov"]
            }
        },
        "tm":{
            "adn":{
                "seller nov": ["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=http://5milesapp.com/winning-grand-prize?utm_source=tm&utm_medium=adn&utm_campaign=seller%20nov",
                    "http://app.appsflyer.com/id917554930?pid=http://5milesapp.com/winning-grand-prize?utm_source=tm&utm_medium=adn&utm_campaign=seller%20nov"]
            }
        },
        "SMS":{
            "FreeListing":{
                "default":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=SMS&c=FreeListing","http://app.appsflyer.com/id917554930?pid=SMS&c=FreeListing"]
            },
            "THOUSANDS":{
                "default":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=SMS&c=THOUSANDS","http://app.appsflyer.com/id917554930?pid=SMS&c=THOUSANDS"]
            },
            "Craigs":{
                "default":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=SMS&c=Craigs","http://app.appsflyer.com/id917554930?pid=SMS&c=Craigs"]
            }
        },
        "Email":{
            "Garage":{
                "default":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=Email&c=Garage","http://app.appsflyer.com/id917554930?pid=Email&c=Garage"]
            }
        },
        "ggdis":{
            "cpc":{
                "Branding":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=google&c=dis","http://app.appsflyer.com/id917554930?pid=google&c=dis"]
            }
        },
        "ggsrc":{
            "cpc":{
                "Branding":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=google&c=src","http://app.appsflyer.com/id917554930?pid=google&c=src"]
            }
        },
        "dallasnews":{
            "CPM":{
                "Branding":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=Branding&c=dallasnews","http://app.appsflyer.com/id917554930?pid=branding&c=dallasnews"]
            }
        },
        "chron":{
            "CPM":{
                "Branding":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=Branding&c=chron","http://app.appsflyer.com/id917554930?pid=branding&c=chron"]
            }
        },
        "postcard":{
            "postcard":{
                "postcard":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=PostCard","http://app.appsflyer.com/id917554930?pid=PostCard"]
            }
        },
        "default":["http://app.appsflyer.com/com.thirdrock.fivemiles?pid=web&c=default","http://app.appsflyer.com/id917554930?pid=web&c=default"]
    },
    verifyEmail : previousAPI + '/api/v2/verify_email/',
    siteMap : [{
        setname:"Navigate",
        urls:[
            {name:"Register",url:"https://5milesapp.com/loginShow/?back=/loginShow/&param=sign"},
            {name:"Sign in",url:"https://5milesapp.com/loginShow/?back=/loginShow/"},
            {name:"Sell",url:"https://5milesapp.com/sell"},
            {name:"About",url:"https://5milesapp.com/info/about"},
            {name:"Support",url:"https://5milesapp.com/info/support"},
            {name:"Privacy Policy",url:"https://5milesapp.com/info/privacy"},
            {name:"Terms of use",url:"https://5milesapp.com/info/terms"},
            {name:"Blog",url:"http://blog.5milesapp.com/"},
            {name:"Recommendation",url:"https://5milesapp.com/recommend"}

        ]
    }],
    newMessageSettings : {
        url : previousAPI + '/api/v2/new_message_count_details/'
    }
};
