# webapp

a [Sails](http://sailsjs.org) application

## 1. 请确认本地安装[nodejs](http://nodejs.org/)
## 2. 本地安装node依赖,到webapp目录下，执行
>**npm install**

## 3. 本地先安装bower
>**npm install bower -g**

## 4. 本地安装bower依赖
>**bower install**

## 5. 启动应用
>**sails lift**

## 6. 浏览器访问
>**http://localhost:1337**
